package com.bursadesain.android.firebase

import android.util.Log
import com.bursadesain.android.app.Constants
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.util.AppUtil.getProfile
import com.bursadesain.android.util.AppUtil.isLogin
import com.bursadesain.android.util.NotificationUtil
import com.bursadesain.android.util.PrefUtil.write
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class MessagingService : FirebaseMessagingService() {

    private val TAG = "FirebaseService"

    private val compositeDisposable = CompositeDisposable()

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.from)

        if (remoteMessage.notification != null)
            Log.d(TAG, "onMessageReceived: " + remoteMessage.notification!!.body)

        // Check if message contains a data payload.
        if (remoteMessage.data.size > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.data)

            if (!isLogin(this)) return

            // show notification
            NotificationUtil.getInstance().showNotification(
                this, remoteMessage.data, remoteMessage.from
            )
        }
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)

        // save token
        write(applicationContext, Constants.PREF_FCM_TOKEN, token)

        // send to server if already login
        if (isLogin(this)) {
            val profile = getProfile(this)!!

            val disposable = ServerManager.getInstance()
                .service.updateFcmToken(profile.id, token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .debounce(400, TimeUnit.MILLISECONDS)
                .subscribe({}, {})

            compositeDisposable.add(disposable)
        }
    }
}