package com.bursadesain.android.app

object Constants {

    const val FIREBASE_GLOBAL_TOPIC = "globalPush"

    const val PREF_PROFILE = "pref_profile"
    const val PREF_FCM_TOKEN = "pref_fcm_token"

    const val ORDER_STATUS_PENDING = 0
    const val ORDER_STATUS_PAID = 1
    const val ORDER_STATUS_ONPROGRESS = 2
    const val ORDER_STATUS_COMPLETED = 3
    const val ORDER_STATUS_CANCELLED = 4
    const val ORDER_STATUS_PAYMENT_INVALID = 5
}