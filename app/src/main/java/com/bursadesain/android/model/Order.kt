package com.bursadesain.android.model

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentReference

data class Order (
    var id: String,
    var user_id: String,
    var package_id: String,
    var date_start: String,
    var date_end: String,
    var notes: String,
    var payment_type: String,
    var status: Int,
    var payment_proof_count: Int,
    var package_name: String,
    var description: String,
    var price: Int,
    var email: String,
    var fullname: String,
    var phone: String,
    var address: String,
    var type: Int,
    var profile_pic: String,
    var fcm_token: String,
    var files: List<OrderFile>,
    var created_date: String,
    var designer_id: String,
    var designer_name: String,
    var designer_profile_pic: String,
    var designer_fcm_token: String,
    var rating: Float
)