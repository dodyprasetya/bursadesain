package com.bursadesain.android.model

data class OrderSummary (
    val pending: Int,
    val on_progress: Int,
    val completed: Int
)
