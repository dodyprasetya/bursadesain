package com.bursadesain.android.model

data class OrderFile (
    val id: String,
    val file: String
)