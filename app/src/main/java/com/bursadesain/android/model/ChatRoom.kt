package com.bursadesain.android.model

class ChatRoom {
    var id: String = ""
    var name: String = ""
    var pic: String = ""
    var status: String = ""
    var timestamp: String = ""
    var latest_message: String = ""
    var designer_id: String = ""
    var member_id: String = ""
    var typing: String = ""
}