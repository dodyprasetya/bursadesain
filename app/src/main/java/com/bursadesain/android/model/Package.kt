package com.bursadesain.android.model

data class Package (
    val id: String,
    val name: String,
    val description: String,
    val color: String,
    val price: Int,
    val portfolios: List<Portfolio>,
    val created_date: String,
    val is_icon: String,
    val user_id: String
)