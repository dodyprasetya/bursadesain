package com.bursadesain.android.model

class Profile {
    var id: String = "0"
    var fullname: String = ""
    val email: String = ""
    var address: String = ""
    var phone: String = ""
    var profile_pic: String = ""
    val fcm_token: String = ""
    val created_date: String = ""
    val type: Int = 0
    val can_post_package: String = "0"
}