package com.bursadesain.android.model

data class UrlData (
    val url: String
)