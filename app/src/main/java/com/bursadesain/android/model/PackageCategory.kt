package com.bursadesain.android.model

data class PackageCategory (
    val id: String,
    val name: String,
    val price: Int,
    val created_date: String,
    val user_id: String,
    val fullname: String
)
