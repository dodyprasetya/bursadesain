package com.bursadesain.android.model

class ChatMessage {
    var id: String = ""
    var message: String = ""
    var user_id: String = ""
    var file_url: String = ""
    var status: String = ""
    var timestamp: String = ""
    var type: String = ""
}