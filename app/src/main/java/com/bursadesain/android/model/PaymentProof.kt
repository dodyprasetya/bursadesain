package com.bursadesain.android.model

data class PaymentProof (
    val id: String,
    val order_id: String,
    val account_name: String,
    val bank_name: String,
    val account_number: String,
    val payment: Int,
    val status: Int,
    val payment_date: String,
    val file: String,
    val created_date: String
)
