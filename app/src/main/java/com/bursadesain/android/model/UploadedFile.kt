package com.bursadesain.android.model

data class UploadedFile (
    val imagePath: String,
    val fileName: String,
    val type: String,
    val fileSize: Int
)