package com.bursadesain.android.model

class Notification {
    var type: String = ""
    var id: String = "0"
    var message: String = ""
    var date: String = ""
    var room_name: String = ""
    var room_pic: String = ""
}