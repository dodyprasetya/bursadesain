package com.bursadesain.android.ui.packages

import android.app.Activity
import android.app.ProgressDialog
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.bursadesain.android.R
import com.bursadesain.android.server.ServerManager
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_input_category.*
import java.util.concurrent.TimeUnit

class InputCategoryActivity : AppCompatActivity() {

    lateinit var dialog: ProgressDialog

    private val compositeDisposable = CompositeDisposable()

    private var isEditing: Boolean = false
    private var categoryId: String = "0"
    private var designerId: String = "0"
    private var name: String = ""
    private var price: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_input_category)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initProgressDialog()

        intent.extras?.let {
            isEditing = it.getBoolean("is_editing", false)
            categoryId = it.getString("category_id", "0")
            designerId = it.getString("designer_id", "0")
            name = it.getString("name", "")
            price = it.getInt("price", 0)

            if (isEditing) toolbar.title = "Edit Kategori"

            etName.setText(name)
            etPrice.setText(price.toString())
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_submit, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
            R.id.menu_submit -> {
                submit()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun initProgressDialog() {
        dialog = ProgressDialog(this);
        dialog.setMessage("Please wait..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
    }

    private fun submit() {
        val name = etName.text.toString();
        val price = etPrice.text.toString();

        if (
            !TextUtils.isEmpty(name) &&
            !TextUtils.isEmpty(price)
        ) {
            submitData()
        } else {
            Toasty.info(this, "Isi semua data", Toasty.LENGTH_LONG).show()
        }
    }

    private fun submitData() {
        val name = etName.text.toString();
        val price = etPrice.text.toString();

        val observable =
            if (!isEditing) ServerManager.getInstance().service.addPackageCategory(name, price, designerId)
            else ServerManager.getInstance().service.editPackageCategory(name, price, categoryId)

        val disposable = observable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { dialog.show() }
            .doOnTerminate { dialog.dismiss() }
            .subscribe(
                {
                    if (it.equals("Berhasil")) {
                        if (!isEditing)
                            Toasty.success(this, "Tambah kategori berhasil", Toasty.LENGTH_LONG).show()
                        else
                            Toasty.success(this, "Edit kategori berhasil", Toasty.LENGTH_LONG).show()

                        setResult(Activity.RESULT_OK)
                        finish()

                    } else {
                        if (!isEditing)
                            Toasty.error(this, "Tambah kategori gagal", Toasty.LENGTH_LONG).show()
                        else
                            Toasty.error(this, "Edit kategori gagal", Toasty.LENGTH_LONG).show()

                    }
                },
                {
                    if (!isEditing)
                        Toasty.error(this, "Tambah kategori gagal", Toasty.LENGTH_LONG).show()
                    else
                        Toasty.error(this, "Edit kategori gagal", Toasty.LENGTH_LONG).show()
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}