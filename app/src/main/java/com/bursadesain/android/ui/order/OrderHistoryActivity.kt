package com.bursadesain.android.ui.order

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import com.bursadesain.android.R
import com.bursadesain.android.app.Constants
import com.bursadesain.android.model.Order
import com.bursadesain.android.model.Profile
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.ui.adapter.OrderAdapter
import com.bursadesain.android.util.PrefUtil
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_order_history.*
import java.util.concurrent.TimeUnit

class OrderHistoryActivity : AppCompatActivity(), OrderAdapter.Listener {

    val TAG = OrderHistoryActivity::class.java.name

    private val compositeDisposable = CompositeDisposable()

    val itemList = ArrayList<Order>()
    lateinit var adapter: OrderAdapter

    lateinit var profile: Profile

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_history)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val profileJson = PrefUtil.read(this, Constants.PREF_PROFILE, "");
        profile = Gson().fromJson(profileJson, Profile::class.java)

        adapter = OrderAdapter()
        adapter.listener = this

        rvOrders.adapter = adapter

        refreshLayout.setOnRefreshListener {
            loadData()
        }

        loadData()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun loadData() {
        val disposable = ServerManager.getInstance()
            .service.loadUserOrders(profile.id, "history")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { refreshLayout.isRefreshing = true }
            .doOnTerminate { refreshLayout.isRefreshing = false }
            .subscribe(
                {
                    itemList.clear()
                    itemList.addAll(it)
                    adapter.updateData(itemList)
                },
                {
                    Log.d(TAG, "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onItemSelected(item: Order) {
        val intent = Intent(this, OrderDetailActivity::class.java)
        intent.putExtra("order_id", item.id)
        startActivity(intent)
    }

    override fun onConfirmPayment(item: Order) {
    }
}