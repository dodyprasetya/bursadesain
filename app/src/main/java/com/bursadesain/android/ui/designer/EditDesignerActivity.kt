package com.bursadesain.android.ui.designer

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bursadesain.android.R
import com.bursadesain.android.app.GlideApp
import com.bursadesain.android.model.Profile
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.util.AppUtil
import com.bursadesain.android.util.PermissionUtil
import com.bursadesain.android.util.RequestUtil
import com.yalantis.ucrop.UCrop
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_edit_designer.*
import java.io.File
import java.util.concurrent.TimeUnit

class EditDesignerActivity : AppCompatActivity() {

    private val PICK_IMAGE = 100

    lateinit var dialog: ProgressDialog
    private var imageUri: Uri? = null

    private val compositeDisposable = CompositeDisposable()

    private var designerId: String = "0"
    private var profilePic: String = ""
    private var email: String = ""
    private var fullname: String = ""
    private var address: String = ""
    private var phone: String = ""
    private var canPostPackage: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_designer)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initProgressDialog()

        intent.extras?.let {
            designerId = it.getString("designer_id", "0")
            profilePic = it.getString("profile_pic", "")
            email = it.getString("email", "")
            fullname = it.getString("fullname", "")
            address = it.getString("address", "")
            phone = it.getString("phone", "")
            canPostPackage = it.getString("can_post_package", "0")

            initData()
        }

        btSave.setOnClickListener {
            saveData()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onStart() {
        super.onStart()
        PermissionUtil.askStoragePermissions(this)
    }

    private fun initProgressDialog() {
        dialog = ProgressDialog(this);
        dialog.setMessage("Please wait..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
    }

    private fun initData() {
        etFullname.setText(fullname)
        etAddress.setText(address)
        etPhone.setText(phone)
        etEmail.setText(email)
        switchPostPackage.isChecked = canPostPackage.equals("1")

        GlideApp.with(this)
            .load(profilePic)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .circleCrop()
            .into(ivProfile)
    }

    private fun saveData() {
        val fullname = etFullname.text.toString();
        val address = etAddress.text.toString();
        val phone = etPhone.text.toString();
        val email = etEmail.text.toString();

        if (
            !TextUtils.isEmpty(fullname) &&
            !TextUtils.isEmpty(address) &&
            !TextUtils.isEmpty(email) &&
            !TextUtils.isEmpty(phone)
        ) {
            postUser()
        } else {
            Toasty.info(this, "Isi semua data", Toasty.LENGTH_LONG).show()
        }
    }

    private fun postUser() {
        val fullname = etFullname.text.toString();
        val address = etAddress.text.toString();
        val phone = etPhone.text.toString();
        val canPostPackage = if (switchPostPackage.isChecked) "1" else "0"

        val user = hashMapOf(
            "user_id" to designerId,
            "fullname" to fullname,
            "address" to address,
            "phone" to phone,
            "email" to email,
            "can_post_package" to canPostPackage
        )

        val disposable = ServerManager.getInstance()
            .service.editDesigner(user)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { dialog.show() }
            .doOnTerminate { dialog.dismiss() }
            .subscribe(
                {
                    if (it.equals("Berhasil")) {
                        Toasty.success(this, "Update designer berhasil", Toasty.LENGTH_LONG).show()

                        setResult(Activity.RESULT_OK)
                        finish()

                    } else if (it.equals("Gagal Upload") || it.equals("No Image")) {
                        Toasty.error(this, "Upload gambar gagal", Toasty.LENGTH_LONG).show()
                    } else {
                        Toasty.error(this, "Update gagal", Toasty.LENGTH_LONG).show()
                    }
                },
                {
                    Toasty.error(this, "Update gagal", Toasty.LENGTH_LONG).show()
                }
            )

        compositeDisposable.add(disposable)
    }

    private fun updateProfilePic() {
        if (imageUri != null) {
            val user = hashMapOf(
                "user_id" to RequestUtil.getBody(designerId)
            )

            val fileRequestBody = RequestUtil.getFileBody(imageUri!!)

            val disposable = ServerManager.getInstance()
                .service.updateProfilePic(user, fileRequestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .debounce(400, TimeUnit.MILLISECONDS)
                .doOnSubscribe { dialog.show() }
                .doOnTerminate { dialog.dismiss() }
                .subscribe(
                    {
                        if (it.url.equals("Gagal Upload") || it.url.equals("No Image")) {
                            Toasty.error(this, "Upload gambar gagal", Toasty.LENGTH_LONG).show()
                        } else if (it.url.equals("Gagal")) {
                            Toasty.error(this, "Update gagal", Toasty.LENGTH_LONG).show()
                        } else {
                            Toasty.success(this, "Update profile picture berhasil", Toasty.LENGTH_LONG).show()

                            profilePic = it.url

                            GlideApp.with(this)
                                .load(profilePic)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .circleCrop()
                                .into(ivProfile)
                        }
                    },
                    {
                        Toasty.error(this, "Update gagal", Toasty.LENGTH_LONG).show()
                    }
                )

            compositeDisposable.add(disposable)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            if (data == null) {
                Toasty.error(this, "Error picking image", Toasty.LENGTH_LONG).show()
                return;
            }

            imageUri = data.getData()

            val options = UCrop.Options()
            options.setCompressionFormat(Bitmap.CompressFormat.JPEG)
            options.setCompressionQuality(80)
            options.setShowCropGrid(true)

            UCrop.of(imageUri!!, Uri.fromFile(File(cacheDir, "user_profile_pic.jpg")))
                .withAspectRatio(1f, 1f)
                .withOptions(options)
                .withMaxResultSize(1024, 1024)
                .start(this)
        }
        else if (requestCode == UCrop.REQUEST_CROP) {
            if (data == null) {
                Toasty.error(this, "Error cropping image", Toasty.LENGTH_LONG).show()
                return;
            }

            if (resultCode == RESULT_OK) {
                imageUri = UCrop.getOutput(data);
                ivProfile.setImageURI(imageUri);

                updateProfilePic()

            } else if (resultCode == UCrop.RESULT_ERROR) {
                Toasty.error(this, "Error cropping image", Toasty.LENGTH_LONG).show()
                Log.e("Error", "Crop error:" + UCrop.getError(data)!!.message);
            }
        }
    }

    fun selectProfilePic(view: View) {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Pilih gambar profil"), PICK_IMAGE)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}