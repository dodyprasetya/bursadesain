package com.bursadesain.android.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bursadesain.android.R
import com.bursadesain.android.model.PackageCategory
import com.bursadesain.android.util.AppUtil
import kotlinx.android.synthetic.main.item_manage_category.view.*

class ManageCategoryAdapter(val isAdmin: Boolean) :
    RecyclerView.Adapter<ManageCategoryAdapter.ViewHolder>(){

    private lateinit var itemList: List<PackageCategory>
    private var context: Context? = null
    var listener: Listener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_manage_category, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if (::itemList.isInitialized) itemList.size else 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    fun updateData(list: ArrayList<PackageCategory>) {
        itemList = list;
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind() {
            val item = itemList.get(adapterPosition)

            val price = AppUtil.convertToMoney(item.price)
            itemView.tvName.text = item.name
            itemView.tvPrice.text = "Rp. ${price}"

            if (isAdmin) {
                itemView.tvOwner.visibility = View.VISIBLE

                val name = if (item.user_id.equals("0")) "Admin" else item.fullname
                itemView.tvOwner.text = "Oleh ${name}"

                val color = if (item.user_id.equals("0")) R.color.colorPrimary
                    else R.color.package10

                itemView.tvOwner.setTextColor(ContextCompat.getColor(context!!, color))

            } else {
                itemView.tvOwner.visibility = View.GONE
            }

            itemView.container.setOnClickListener {
                listener?.onItemSelected(item)
            }

            itemView.container.setOnLongClickListener {
                listener?.onItemLongClicked(item)
                true
            }
        }
    }

    interface Listener {
        fun onItemSelected(item: PackageCategory)
        fun onItemLongClicked(item: PackageCategory)
    }
}