package com.bursadesain.android.ui.payment

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bursadesain.android.R
import com.bursadesain.android.helper.DatePickerHelper
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.ui.main.MainActivity
import com.bursadesain.android.util.DateUtil
import com.bursadesain.android.util.PermissionUtil
import com.bursadesain.android.util.RequestUtil
import com.yalantis.ucrop.UCrop
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_payment_confirmation.*
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit

class ConfirmPaymentActivity : AppCompatActivity() {

    private val PICK_IMAGE = 100

    lateinit var dialog: ProgressDialog
    private var imageUri: Uri? = null

    private val compositeDisposable = CompositeDisposable()

    private var orderId: String = "0"
    private var packageTitle: String = ""

    lateinit var datePicker: DatePickerHelper
    private var date: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_confirmation)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initProgressDialog()
        datePicker = DatePickerHelper(this)

        intent.extras?.let {
            orderId = it.getString("order_id", "0")
            packageTitle = it.getString("package_name", "")

            tvOrderId.text = "ORDER-${orderId}"
            tvTitle.text = packageTitle
        }

        tvDate.setOnClickListener {
            val cal = Calendar.getInstance()

            val d = cal.get(Calendar.DAY_OF_MONTH)
            val m = cal.get(Calendar.MONTH)
            val y = cal.get(Calendar.YEAR)

            datePicker.showDialog(d, m, y, object : DatePickerHelper.Callback {

                override fun onDateSelected(dayofMonth: Int, month: Int, year: Int) {
                    cal.set(Calendar.DAY_OF_MONTH, dayofMonth)
                    cal.set(Calendar.MONTH, month)
                    cal.set(Calendar.YEAR, year)

                    date = DateUtil.getFormattedCalendar(cal, DateUtil.MYSQL_DATE_FORMAT)
                    tvDate.text = DateUtil.getFormattedCalendar(cal, DateUtil.LOCAL_DATE_FORMAT)
                }
            })
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_submit, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
            R.id.menu_submit -> {
                submit()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onStart() {
        super.onStart()
        PermissionUtil.askStoragePermissions(this)
    }

    private fun initProgressDialog() {
        dialog = ProgressDialog(this);
        dialog.setMessage("Please wait..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
    }

    private fun submit() {
        if (imageUri != null) {

            val fullname = etFullname.text.toString();
            val bankName = etBankName.text.toString();
            val accountNumber = etBankAccNumber.text.toString();
            val totalPaid = etTotalPaid.text.toString();

            if (
                !TextUtils.isEmpty(fullname) &&
                !TextUtils.isEmpty(bankName) &&
                !TextUtils.isEmpty(accountNumber) &&
                !TextUtils.isEmpty(totalPaid) &&
                !TextUtils.isEmpty(date)
            ) {
                submitData()
            } else {
                Toasty.info(this, "Isi semua data", Toasty.LENGTH_LONG).show()
            }
        } else {
            Toasty.info(this, "Pilih screenshot / foto bukti pembayaran", Toasty.LENGTH_LONG).show()
        }
    }

    private fun submitData() {
        val fullname = etFullname.text.toString();
        val bankName = etBankName.text.toString();
        val accountNumber = etBankAccNumber.text.toString();
        val totalPaid = etTotalPaid.text.toString();

        val payment = hashMapOf(
            "order_id" to RequestUtil.getBody(orderId),
            "account_name" to RequestUtil.getBody(fullname),
            "account_number" to RequestUtil.getBody(accountNumber),
            "bank_name" to RequestUtil.getBody(bankName),
            "payment" to RequestUtil.getBody(totalPaid),
            "payment_date" to RequestUtil.getBody(date)
        )

        val fileRequestBody = RequestUtil.getFileBody(imageUri!!)

        val disposable = ServerManager.getInstance()
            .service.confirmPayment(payment, fileRequestBody)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { dialog.show() }
            .doOnTerminate { dialog.dismiss() }
            .subscribe(
                {
                    if (it.equals("Berhasil")) {
                        Toasty.success(this, "Konfirmasi berhasil", Toasty.LENGTH_LONG).show()

                        val intent = Intent(this, MainActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                        intent.putExtra("selected_tab", 1)
                        startActivity(intent)

                    } else if (it.equals("Gagal Upload") || it.equals("No Image")) {
                        Toasty.error(this, "Upload gagal", Toasty.LENGTH_LONG).show()
                    } else {
                        Toasty.error(this, "Konfirmasi gagal", Toasty.LENGTH_LONG).show()
                    }
                },
                {
                    Toasty.error(this, "Konfirmasi gagal", Toasty.LENGTH_LONG).show()
                }
            )

        compositeDisposable.add(disposable)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            if (data == null) {
                Toasty.error(this, "Error picking image", Toasty.LENGTH_LONG).show()
                return;
            }

            imageUri = data.getData()

            val options = UCrop.Options()
            options.setCompressionFormat(Bitmap.CompressFormat.JPEG)
            options.setCompressionQuality(80)
            options.setShowCropGrid(true)
            options.setFreeStyleCropEnabled(true)

            UCrop.of(imageUri!!, Uri.fromFile(File(cacheDir, "user_payment_proof.jpg")))
                .withOptions(options)
                .withMaxResultSize(2048, 2048)
                .start(this)
        }
        else if (requestCode == UCrop.REQUEST_CROP) {
            if (data == null) {
                Toasty.error(this, "Error cropping image", Toasty.LENGTH_LONG).show()
                return;
            }

            if (resultCode == RESULT_OK) {
                imageUri = UCrop.getOutput(data);
                ivPaymentProof.setImageURI(imageUri);

            } else if (resultCode == UCrop.RESULT_ERROR) {
                Toasty.error(this, "Error cropping image", Toasty.LENGTH_LONG).show()
                Log.e("Error", "Crop error:" + UCrop.getError(data)!!.message);
            }
        }
    }

    fun uploadPaymentProof(view: View) {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Pilih bukti pembayaran"), PICK_IMAGE)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}