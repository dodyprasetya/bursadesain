package com.bursadesain.android.ui.packages

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.bursadesain.android.R
import com.bursadesain.android.model.Package
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.ui.adapter.PackageAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_search_packages.*
import java.util.concurrent.TimeUnit

class SearchPackagesActivity : AppCompatActivity(), PackageAdapter.Listener {

    val TAG = SearchPackagesActivity::class.java.name

    private val compositeDisposable = CompositeDisposable()

    val packageList = ArrayList<Package>()
    lateinit var adapter: PackageAdapter

    private var keyword: String = ""
    lateinit var handler: Handler
    lateinit var runnable: Runnable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_packages)

        setSupportActionBar(toolbar)

        adapter = PackageAdapter()
        adapter.listener = this
        rvPackages.adapter = adapter

        handler = Handler()
        runnable = object : Runnable {
            override fun run() {
                searchData()
            }
        }

        refreshLayout.setOnRefreshListener {
            searchData()
        }

        ivBack.setOnClickListener {
            onBackPressed()
        }

        etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                handler.removeCallbacks(runnable)
            }

            override fun afterTextChanged(s: Editable?) {
                keyword = s.toString()

                if (!TextUtils.isEmpty(keyword)) {
                    handler.postDelayed(runnable, 500)
                } else {
                    packageList.clear()
                    adapter.updateData(packageList)
                }
            }
        })
    }

    private fun searchData() {
        val disposable = ServerManager.getInstance()
            .service.searchPackages(keyword)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { refreshLayout.isRefreshing = true }
            .doOnTerminate { refreshLayout.isRefreshing = false }
            .subscribe(
                {
                    packageList.clear()
                    packageList.addAll(it)
                    adapter.updateData(packageList)
                },
                {
                    Log.d(TAG, "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onItemSelected(item: Package) {
        val intent = Intent(this, PackageDetailActivity::class.java)
        intent.putExtra("package_id", item.id)
        intent.putExtra("package_name", item.name)
        intent.putExtra("package_desc", item.description)
        intent.putExtra("package_price", item.price)
        startActivity(intent)
    }
}