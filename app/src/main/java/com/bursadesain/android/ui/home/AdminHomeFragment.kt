package com.bursadesain.android.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bursadesain.android.R
import com.bursadesain.android.model.ChatRoom
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.ui.adapter.ChatRoomAdapter
import com.bursadesain.android.ui.chat.ChatMessageActivity
import com.bursadesain.android.ui.designer.DesignerListActivity
import com.bursadesain.android.ui.member.MemberActivity
import com.bursadesain.android.ui.notification.NotificationActivity
import com.bursadesain.android.ui.packages.ManageCategoryActivity
import com.google.firebase.database.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_admin_home.*
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class AdminHomeFragment : Fragment(), ChatRoomAdapter.Listener {

    val TAG = AdminHomeFragment::class.java.name

    private val compositeDisposable = CompositeDisposable()

    val db = FirebaseDatabase.getInstance()
    lateinit var chatRoomQuery: Query
    lateinit var chatRoomListener: ValueEventListener

    val itemList = ArrayList<ChatRoom>()
    lateinit var adapter: ChatRoomAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_admin_home, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.inflateMenu(R.menu.menu_notification)
        toolbar.menu.findItem(R.id.menu_notification).setOnMenuItemClickListener {
            startActivity(Intent(context, NotificationActivity::class.java))
            true
        }

        adapter = ChatRoomAdapter()
        adapter.listener = this

        rvNewChats.adapter = adapter

        llMember.setOnClickListener {
            startActivity(Intent(context!!, MemberActivity::class.java))
        }

        llDesigners.setOnClickListener {
            startActivity(Intent(context!!, DesignerListActivity::class.java))
        }

        llPackage.setOnClickListener {
            startActivity(Intent(context!!, ManageCategoryActivity::class.java))
        }

        loadNewChats()
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    private fun loadData() {
        val disposable = ServerManager.getInstance()
            .service.loadSummaryOrderAdmin()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .subscribe(
                {
                    tvSummaryOnProgress.text = "${it.on_progress}"
                    tvSummaryPending.text = "${it.pending}"
                    tvSummaryActive.text = "${it.on_progress + it.pending}"
                },
                {
                    Log.d(TAG, "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    private fun loadNewChats() {
        chatRoomQuery = db.reference.child("chat_rooms").orderByChild("timestamp")
            .limitToLast(5)

        chatRoomListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                itemList.clear()

                for (ds in dataSnapshot.children) {
                    val item = ds.getValue(ChatRoom::class.java)
                    item?.id = ds.key ?: ""

                    item?.let {
                        itemList.add(it)
                    }
                }

                Collections.reverse(itemList)
                adapter.updateData(itemList)

                if (itemList.size > 0) {
                    llNoChat.visibility = View.GONE
                } else {
                    llNoChat.visibility = View.VISIBLE
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Log.e(TAG, databaseError.message)
            }
        }

        chatRoomQuery.addValueEventListener(chatRoomListener)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        chatRoomQuery.removeEventListener(chatRoomListener)
        super.onDestroy()
    }

    override fun onItemSelected(item: ChatRoom) {
        val intent = Intent(context, ChatMessageActivity::class.java)
        intent.putExtra("room_id", item.id)
        intent.putExtra("room_name", item.name)
        intent.putExtra("room_pic", item.pic)
        startActivity(intent)
    }
}