package com.bursadesain.android.ui.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bursadesain.android.R
import com.bursadesain.android.model.Package
import com.bursadesain.android.model.PackageCategory
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.ui.packages.PackageDetailActivity
import com.bursadesain.android.util.AppUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.item_package_category1.view.*
import java.util.concurrent.TimeUnit

class PackageCategoryAdapter : RecyclerView.Adapter<PackageCategoryAdapter.ViewHolder>(){

    val TAG = PackageCategoryAdapter::class.java.name

    private val compositeDisposable = CompositeDisposable()

    private lateinit var itemList: List<PackageCategory>
    private var context: Context? = null
    var listener: Listener? = null

    private val adapterList = ArrayList<PackageAdapter>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_package_category1, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if (::itemList.isInitialized) itemList.size else 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    fun updateData(list: ArrayList<PackageCategory>) {
        itemList = list;

        adapterList.clear()
        for (i in list.indices) {
            adapterList.add(PackageAdapter())
        }

        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind() {
            val item = itemList.get(adapterPosition)

            val price = AppUtil.convertToMoney(item.price)
            itemView.tvTitle.text = item.name
            itemView.tvPrice.text = "Rp. ${price}"

            val adapter = adapterList.get(adapterPosition)
            adapter.listener = object : PackageAdapter.Listener {
                override fun onItemSelected(item: Package) {
                    val intent = Intent(context, PackageDetailActivity::class.java)
                    intent.putExtra("package_id", item.id)
                    intent.putExtra("package_name", item.name)
                    intent.putExtra("package_desc", item.description)
                    intent.putExtra("package_price", item.price)
                    context?.startActivity(intent)
                }
            }

            itemView.rvPackages.adapter = adapter

            // load data
            loadData(item.id, adapterPosition, itemView.progressBar, itemView.tvNotFound)

            itemView.container.setOnClickListener {
                listener?.onItemSelected(item)
            }
        }
    }

    interface Listener {
        fun onItemSelected(item: PackageCategory)
    }

    private fun loadData(categoryId: String, position: Int, pb: ProgressBar, tvNotFound: TextView) {
        val disposable = ServerManager.getInstance()
            .service.loadPackages(categoryId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { pb.visibility = View.VISIBLE }
            .doOnTerminate { pb.visibility = View.GONE }
            .subscribe(
                {
                    val packageList = ArrayList<Package>(it)

                    adapterList.get(position).updateData(packageList)

                    if (packageList.size == 0) {
                        tvNotFound.visibility = View.VISIBLE
                    } else {
                        tvNotFound.visibility = View.GONE
                    }
                },
                {
                    Log.d(TAG, "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }
}