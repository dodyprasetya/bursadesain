package com.bursadesain.android.ui.chat

import android.app.ProgressDialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.AbsListView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bursadesain.android.BuildConfig
import com.bursadesain.android.R
import com.bursadesain.android.app.GlideApp
import com.bursadesain.android.model.ChatMessage
import com.bursadesain.android.model.ChatRoom
import com.bursadesain.android.model.Profile
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.ui.adapter.ChatMessageAdapter
import com.bursadesain.android.util.AppUtil
import com.bursadesain.android.util.DateUtil
import com.bursadesain.android.util.FileUtil
import com.bursadesain.android.util.PermissionUtil
import com.downloader.Error
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.yalantis.ucrop.UCrop
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_chat_message.*
import kotlinx.android.synthetic.main.dialog_select_image.view.*
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import java.io.File
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class ChatMessageActivity : AppCompatActivity(), ChatMessageAdapter.Listener {

    val TAG = ChatMessageActivity::class.java.name

    private val compositeDisposable = CompositeDisposable()

    val db = FirebaseDatabase.getInstance()
    val storage = FirebaseStorage.getInstance()

    lateinit var dialog: ProgressDialog
    lateinit var dialogSelectImage: BottomSheetDialog

    lateinit var chatRoomQuery: Query
    lateinit var chatRoomListener: ValueEventListener

    lateinit var chatQuery: Query
    lateinit var chatListener: ChildEventListener

    val memberList = ArrayList<Profile>()

    val itemList = ArrayList<ChatMessage>()
    lateinit var adapter: ChatMessageAdapter

    private var roomId: String = "0"
    private var roomName: String = ""
    private var roomPic: String = ""

    lateinit var profile: Profile

    private var recordingTime: Long = 0
    private var recorder: MediaRecorder? = null
    private var audioFile: String? = null

    lateinit var handler: Handler
    lateinit var runnable: Runnable
    private var timer: CountDownTimer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_message)

        setSupportActionBar(toolbar)

        initProgressDialog()
        initPickerDialog()

        intent.extras?.let {
            roomId = it.getString("room_id", "0")
            roomName = it.getString("room_name", "")
            roomPic = it.getString("room_pic", "")

            setRoomData()
        }

        profile = AppUtil.getProfile(this)!!

        adapter = ChatMessageAdapter(profile.id)
        adapter.listener = this

        val layoutManager = LinearLayoutManager(this)
        layoutManager.setStackFromEnd(false)
        rvChats.layoutManager = layoutManager
        rvChats.adapter = adapter

        loadData()

        refreshLayout.setOnRefreshListener {
            refreshLayout.isRefreshing = false
        }

        ivBack.setOnClickListener {
            onBackPressed()
        }

        ivSend.setOnClickListener {
            sendChat()
        }

        ivAdd.setOnClickListener {
            dialogSelectImage.show()
        }

        ivPicture.setOnClickListener {
            showChatDetail()
        }

        tvRoomName.setOnClickListener {
            showChatDetail()
        }

        ivRecord.setOnTouchListener(object : View.OnTouchListener {

            override fun onTouch(v: View, event: MotionEvent): Boolean {
                if (event.action == MotionEvent.ACTION_DOWN) {
                    recordingTime = System.currentTimeMillis()

                    etMessage.setText("00:00")

                    handler = Handler()
                    runnable = object : Runnable {
                        override fun run() {
                            startTimer()
                        }
                    }

                    handler.postDelayed(runnable, 700)

                    val beep: MediaPlayer = MediaPlayer.create(this@ChatMessageActivity, R.raw.beep)
                    beep.setVolume(100f, 100f)
                    beep.start()
                    beep.setOnCompletionListener {
                        beep.release()
                        startRecording()
                    }

                } else if (event.action == MotionEvent.ACTION_UP) {
                    if (recordingTime + 3000 > System.currentTimeMillis()) {
                        clearRecorder()
                        stopTimer()
                        Toasty.info(this@ChatMessageActivity,
                            "Tahan mic untuk merekam audio minimal 3 detik", Toasty.LENGTH_LONG)
                            .show()
                    } else {
                        clearRecorder()
                        stopTimer()
                        sendRecording()
                    }

                    val beep: MediaPlayer = MediaPlayer.create(this@ChatMessageActivity, R.raw.beep)
                    beep.setVolume(100f, 100f)
                    beep.start()
                    beep.setOnCompletionListener {
                        beep.release()
                    }

                }

                return true
            }
        })

        rvChats.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            var userScrolled = false
            var scrollOutItems = 0

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition()

                if (userScrolled && scrollOutItems == 0 && itemList.size > 10) {
                    userScrolled = false

                    refreshLayout.isRefreshing = true

                    db.reference.child("chats").child(roomId).orderByKey()
                        .endAt(itemList.first().id).limitToLast(10)
                        .addListenerForSingleValueEvent(object : ValueEventListener {

                            override fun onDataChange(dataSnapshot: DataSnapshot) {
                                val chats = ArrayList<ChatMessage>()
                                for (ds in dataSnapshot.children) {
                                    val item = ds.getValue(ChatMessage::class.java)
                                    item?.id = ds.key ?: ""
                                    item?.let { chats.add(it) }
                                }

                                if (chats.size > 0) {
                                    chats.removeAt(chats.lastIndex)
                                }

                                chats.addAll(itemList)
                                itemList.clear()
                                itemList.addAll(chats)

                                adapter.updateData(itemList)
                                refreshLayout.isRefreshing = false
                            }
                            override fun onCancelled(error: DatabaseError) {
                                refreshLayout.isRefreshing = false
                            }
                        })
                }
            }
        })

        etMessage.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                val message = s.toString()

                if (TextUtils.isEmpty(message) || recordingTime > 0) {
                    ivRecord.visibility = View.VISIBLE
                    ivSend.visibility = View.GONE
                } else {
                    ivRecord.visibility = View.GONE
                    ivSend.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun initProgressDialog() {
        dialog = ProgressDialog(this);
        dialog.setMessage("Please wait..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
    }

    private fun initPickerDialog() {
        val dialogView = layoutInflater.inflate(R.layout.dialog_select_image, null)

        dialogSelectImage = BottomSheetDialog(this);
        dialogSelectImage.setContentView(dialogView);
        dialogSelectImage.setCanceledOnTouchOutside(true);

        dialogView.llPickerCamera.setOnClickListener {
            dialogSelectImage.dismiss()
            EasyImage.openCamera(this, 0)
        }

        dialogView.llPickerGallery.setOnClickListener {
            dialogSelectImage.dismiss()
            EasyImage.openGallery(this, 0)
        }
    }

    private fun setRoomData() {
        tvRoomName.text = roomName

        if (!TextUtils.isEmpty(roomPic)) {
            GlideApp.with(this)
                .load(roomPic)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .circleCrop()
                .into(ivPicture)
        } else {
            ivPicture.setImageResource(R.color.package1)
        }
    }

    override fun onStart() {
        super.onStart()
        PermissionUtil.askStorageAndRecordPermissions(this)

//        if (memberList.isEmpty()) loadChatRoomMember()
//        else loadChats()
    }

    override fun onStop() {
        super.onStop()

//        itemList.clear()
//        chatQuery.removeEventListener(chatListener)
    }

    private fun loadData() {
        if (memberList.isEmpty()) loadChatRoomMember()
        loadChatRoomData()
    }

    private fun loadChatRoomMember() {
        val disposable = ServerManager.getInstance()
            .service.loadChatRoomMembers(roomId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { refreshLayout.isRefreshing = true }
            .doOnTerminate { refreshLayout.isRefreshing = false }
            .subscribe(
                {
                    memberList.clear()
                    memberList.addAll(it)

                    adapter.updateMemberList(memberList)
                    loadChats()
                },
                {
                    Log.d(TAG, "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    private fun loadChatRoomData() {
        chatRoomQuery = db.reference.child("chat_rooms").child(roomId)

        chatRoomListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val item = dataSnapshot.getValue(ChatRoom::class.java)
                item?.id = dataSnapshot.key ?: ""

                item?.let {
                    roomName = it.name
                    roomPic = it.pic

                    setRoomData()
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                refreshLayout.isRefreshing = false
                Log.e(TAG, databaseError.message)
            }
        }

        chatRoomQuery.addValueEventListener(chatRoomListener)
    }

    private fun isChatShouldBeDisplayed(item: ChatMessage): Boolean {
        val isMyMessage = item.user_id.equals(profile.id)
        return (!isMyMessage && item.status.equals("1")) || isMyMessage
    }

    private fun loadChats() {
        refreshLayout.isRefreshing = true

        chatQuery = db.reference.child("chats").child(roomId)

        chatListener = object : ChildEventListener {

            override fun onChildAdded(dataSnapshot: DataSnapshot, s: String?) {
                val item = dataSnapshot.getValue(ChatMessage::class.java)
                item?.id = dataSnapshot.key ?: ""

                item?.let {
                    if (isChatShouldBeDisplayed(it)) {
                        itemList.add(it)
                        adapter.updateData(itemList)
                        rvChats.scrollToPosition(itemList.lastIndex)
                    }
                }

                refreshLayout.isRefreshing = false
            }

            override fun onChildChanged(dataSnapshot: DataSnapshot, s: String?) {
                val item = dataSnapshot.getValue(ChatMessage::class.java)
                item?.id = dataSnapshot.key ?: ""

                item?.let {
                    if (isChatShouldBeDisplayed(it)) {
                        for (i in itemList.lastIndex downTo 0) {
                            val chatId = itemList.get(i).id

                            if (chatId.equals(it.id)) {
                                itemList.removeAt(i)
                                itemList.add(i, it)
                                break
                            }
                        }
                        adapter.updateData(itemList)
                    }
                }

                refreshLayout.isRefreshing = false
            }

            override fun onChildMoved(dataSnapshot: DataSnapshot, s: String?) {
                refreshLayout.isRefreshing = false
            }

            override fun onChildRemoved(dataSnapshot: DataSnapshot) {
                val item = dataSnapshot.getValue(ChatMessage::class.java)
                item?.id = dataSnapshot.key ?: ""

                item?.let {
                    for (i in itemList.lastIndex downTo 0) {
                        val chatId = itemList.get(i).id

                        if (chatId.equals(it.id)) {
                            itemList.removeAt(i)
                        }
                    }
                    adapter.updateData(itemList)
                }

                refreshLayout.isRefreshing = false
            }

            override fun onCancelled(error: DatabaseError) {
                refreshLayout.isRefreshing = false
                Log.e(TAG, error.message)
            }
        }

        chatQuery.limitToLast(20).addChildEventListener(chatListener)
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
        chatQuery.removeEventListener(chatListener)
        chatRoomQuery.removeEventListener(chatRoomListener)
    }

    private fun sendChat() {
        val message = etMessage.text.toString().trim()
        if (TextUtils.isEmpty(message)) return

        val timestamp = DateUtil.getFormattedCalendar(Calendar.getInstance(),
            DateUtil.MYSQL_DATE_TIME_FORMAT)

        val ref = db.reference.child("chats").child(roomId).push()
        val chat = hashMapOf<String, Any>(
            "message" to message,
            "user_id" to profile.id,
            "file_url" to "",
            "status" to "1",
            "timestamp" to timestamp,
            "type" to "text"
        )

        ref.updateChildren(chat, object : DatabaseReference.CompletionListener {
            override fun onComplete(error: DatabaseError?, reference: DatabaseReference) {
                etMessage.setText("")
                updateChatRoom(message, timestamp)
                sendChatNotif("mengirim chat baru")
            }
        })
    }

    private fun sendImage(uri: Uri) {
        val timestamp = DateUtil.getFormattedCalendar(Calendar.getInstance(),
            DateUtil.MYSQL_DATE_TIME_FORMAT)

        val ref = db.reference.child("chats").child(roomId).push()
        val pushId = ref.key

        val chat = hashMapOf<String, Any>(
            "message" to "",
            "user_id" to profile.id,
            "file_url" to uri.path!!,
            "status" to "2",
            "timestamp" to timestamp,
            "type" to "image"
        )

        ref.updateChildren(chat, object : DatabaseReference.CompletionListener {
            override fun onComplete(error: DatabaseError?, reference: DatabaseReference) {
                uploadImage(uri, pushId)
            }
        })
    }

    private fun updateChatRoom(message: String, timestamp: String) {
        val ref = db.reference.child("chat_rooms").child(roomId)
        val chatRoom = hashMapOf<String, Any>(
            "latest_message" to message,
            "timestamp" to timestamp
        )

        ref.updateChildren(chatRoom, object : DatabaseReference.CompletionListener {
            override fun onComplete(error: DatabaseError?, reference: DatabaseReference) {
                etMessage.setText("")
            }
        })
    }

    private fun updateUploadImageSuccess(uri: Uri, chatId: String?) {
        chatId?.let {
            val ref = db.reference.child("chats").child(roomId)
                .child(it)

            val timestamp = DateUtil.getFormattedCalendar(
                Calendar.getInstance(), DateUtil.MYSQL_DATE_TIME_FORMAT
            )
            val chat = hashMapOf<String, Any>(
                "file_url" to uri.toString(),
                "timestamp" to timestamp,
                "status" to "1"
            )

            ref.updateChildren(chat, object : DatabaseReference.CompletionListener {
                override fun onComplete(error: DatabaseError?, reference: DatabaseReference) {
                    updateChatRoom("Mengirim gambar", timestamp)
                    sendChatNotif("mengirim chat gambar")
                }
            })
        }
    }

    private fun updateUploadImageFailed(chatId: String?) {
        chatId?.let {
            val ref = db.reference.child("chats").child(roomId)
                .child(it)

            val timestamp = DateUtil.getFormattedCalendar(
                Calendar.getInstance(), DateUtil.MYSQL_DATE_TIME_FORMAT
            )
            val chat = hashMapOf<String, Any>(
                "timestamp" to timestamp,
                "status" to "3"
            )

            ref.updateChildren(chat)
        }
    }

    private fun uploadImage(uri: Uri, chatId: String?) {
        val fileName = "${chatId}.jpg"
        val ref = storage.reference.child("chat_images").child(fileName)

        ref.putFile(uri)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    ref.downloadUrl
                        .addOnSuccessListener { uri ->
                            updateUploadImageSuccess(uri, chatId)
                        }
                        .addOnFailureListener {
                            updateUploadImageFailed(chatId)
                        }
                } else {
                    updateUploadImageFailed(chatId)
                }
            }
            .addOnFailureListener {
                updateUploadImageFailed(chatId)
            }
    }

    override fun onItemClicked(type: String, item: ChatMessage) {
        when (type) {
            "image" -> {
                val filePath =
                    if (item.status.equals("1")) "${FileUtil.DOWNLOAD_FOLDER_PATH}/${item.id}.jpg"
                    else item.file_url

                val path = File(filePath)

                if (path.exists()) {
                    openFile(filePath)
                } else {
                    downloadFile(item.file_url, item.id)
                }
            }
            "audio" -> {
                val filePath =
                    if (item.status.equals("1"))"${FileUtil.AUDIO_FOLDER_PATH}/${item.id}.mp3"
                    else item.file_url

                val path = File(filePath)

                if (path.exists()) {
                    openAudio(filePath)
                } else {
                    downloadAudio(item)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        EasyImage.handleActivityResult(requestCode, resultCode, data, this,
            object : DefaultCallback() {

                override fun onImagesPicked(
                    imageFiles: MutableList<File>, source: EasyImage.ImageSource?, type: Int
                ) {
                    if (imageFiles.size > 0) {

                        val options = UCrop.Options()
                        options.setCompressionFormat(Bitmap.CompressFormat.JPEG)
                        options.setCompressionQuality(80)
                        options.setShowCropGrid(true)
                        options.setFreeStyleCropEnabled(true)

                        UCrop.of(Uri.fromFile(imageFiles[0]), Uri.fromFile(File(cacheDir, "chat_image.jpg")))
                            .withOptions(options)
                            .withMaxResultSize(1024, 1024)
                            .start(this@ChatMessageActivity)
                    }
                }

                override fun onCanceled(source: EasyImage.ImageSource?, type: Int) {
                    super.onCanceled(source, type)

                    if (source == EasyImage.ImageSource.CAMERA) {
                        val photoFile = EasyImage.lastlyTakenButCanceledPhoto(this@ChatMessageActivity)
                        photoFile?.delete()
                    }
                }

                override fun onImagePickerError(
                    e: Exception?, source: EasyImage.ImageSource?, type: Int
                ) {
                    super.onImagePickerError(e, source, type)

                    Toasty.error(this@ChatMessageActivity, "Error dalam mengambil gambar",
                        Toasty.LENGTH_LONG).show()
                    e!!.printStackTrace()
                }
        })

        if (requestCode == UCrop.REQUEST_CROP) {
            if (data == null) {
                Toasty.error(this, "Error cropping image", Toasty.LENGTH_LONG).show()
                return;
            }

            if (resultCode == RESULT_OK) {
                val imageUri = UCrop.getOutput(data);
                imageUri?.let {
                    sendImage(it)
                }

            } else if (resultCode == UCrop.RESULT_ERROR) {
                Toasty.error(this, "Error cropping image", Toasty.LENGTH_LONG).show()
                Log.e("Error", "Crop error:" + UCrop.getError(data)!!.message);
            }
        }

    }

    private fun showChatDetail() {
        val intent = Intent(this, ChatDetailActivity::class.java)
        intent.putExtra("room_id", roomId)
        intent.putExtra("room_name", roomName)
        intent.putExtra("room_pic", roomPic)
        startActivity(intent)
    }

    private fun openAudio(path: String) {
        val fragment = PlayAudioFragment()
        val args = Bundle()
        args.putString("path", path)
        fragment.arguments = args

        val ft = supportFragmentManager.beginTransaction()
        ft.addToBackStack(null)
        ft.replace(R.id.rootLayout, fragment).commit()
    }

    private fun downloadAudio(chat: ChatMessage) {
        dialog.show()

        val fileDir = "${FileUtil.AUDIO_FOLDER_PATH}/"
        val fileName = "${chat.id}.mp3"

        PRDownloader.download(chat.file_url, fileDir, fileName)
            .build()
            .start(object : OnDownloadListener {

                override fun onDownloadComplete() {
                    dialog.dismiss()
                    openAudio(fileDir + fileName)
                }

                override fun onError(error: Error?) {
                    dialog.dismiss()
                    Toasty.error(this@ChatMessageActivity, "Download audio gagal",
                        Toasty.LENGTH_LONG).show()
                }
            })
    }

    private fun startTimer() {
        timer = object : CountDownTimer(30000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val time = 30000 - millisUntilFinished
                val min = (time / 1000).toInt() / 60
                val sec = (time / 1000).toInt() % 60

                val timeLeftFormatted =
                    String.format(Locale.getDefault(), "%02d:%02d", min, sec)

                etMessage.setText(timeLeftFormatted)
            }

            override fun onFinish() {
                sendRecording()
                etMessage.setText("")
            }
        }

        timer?.start()
    }

    private fun stopTimer() {
        recordingTime = 0
        etMessage.setText("")
        handler.removeCallbacks(runnable)
        timer?.cancel()
    }

    private fun clearRecorder() {
        recorder?.apply {
            stop()
            reset()
            release()
        }
    }

    private fun startRecording() {
        audioFile = File(cacheDir, "chat_audio.mp3").absolutePath

        recorder = MediaRecorder()
        recorder?.apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
            setOutputFile(audioFile)
            setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)

            try {
                prepare()
                start()
            } catch (e: IOException) {
                Log.e(TAG, e.message!!)
            } catch (e: IllegalStateException) {
                Log.e(TAG, e.message!!)
            }
        }
    }

    private fun sendRecording() {
        val timestamp = DateUtil.getFormattedCalendar(Calendar.getInstance(),
            DateUtil.MYSQL_DATE_TIME_FORMAT)

        val ref = db.reference.child("chats").child(roomId).push()
        val pushId = ref.key

        val chat = hashMapOf<String, Any>(
            "message" to "",
            "user_id" to profile.id,
            "file_url" to audioFile!!,
            "status" to "2",
            "timestamp" to timestamp,
            "type" to "audio"
        )

        ref.updateChildren(chat, object : DatabaseReference.CompletionListener {
            override fun onComplete(error: DatabaseError?, reference: DatabaseReference) {
                uploadAudio(Uri.fromFile(File(audioFile)), pushId)
            }
        })
    }

    private fun uploadAudio(uri: Uri, chatId: String?) {
        val fileName = "${chatId}.mp3"
        val ref = storage.reference.child("chat_audios").child(fileName)

        ref.putFile(uri)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    ref.downloadUrl
                        .addOnSuccessListener { uri ->
                            updateUploadAudioSuccess(uri, chatId)
                        }
                        .addOnFailureListener {
                            updateUploadImageFailed(chatId)
                        }
                } else {
                    updateUploadImageFailed(chatId)
                }
            }
            .addOnFailureListener {
                updateUploadImageFailed(chatId)
            }
    }

    private fun updateUploadAudioSuccess(uri: Uri, chatId: String?) {
        chatId?.let {
            val ref = db.reference.child("chats").child(roomId)
                .child(it)

            val timestamp = DateUtil.getFormattedCalendar(
                Calendar.getInstance(), DateUtil.MYSQL_DATE_TIME_FORMAT
            )
            val chat = hashMapOf<String, Any>(
                "file_url" to uri.toString(),
                "timestamp" to timestamp,
                "status" to "1"
            )

            ref.updateChildren(chat, object : DatabaseReference.CompletionListener {
                override fun onComplete(error: DatabaseError?, reference: DatabaseReference) {
                    updateChatRoom("Mengirim audio", timestamp)
                    sendChatNotif("mengirim chat audio")
                }
            })
        }
    }

    private fun downloadFile(fileUrl: String, chatId: String) {
        dialog.show()

        val fileDir = "${FileUtil.DOWNLOAD_FOLDER_PATH}/"
//        val fileName = fileUrl
//            .split("/").last()
//            .split("?").first()

        val fileName = "${chatId}.jpg"

        PRDownloader.download(fileUrl, fileDir, fileName)
            .build()
            .start(object : OnDownloadListener {

                override fun onDownloadComplete() {
                    dialog.dismiss()
                    openFile(fileDir + fileName)
                }

                override fun onError(error: Error?) {
                    dialog.dismiss()
                    Toasty.error(this@ChatMessageActivity, "Download file gagal",
                        Toasty.LENGTH_LONG).show()
                }
            })
    }

    private fun openFile(path: String) {
        val uri = FileProvider.getUriForFile(this,
            BuildConfig.APPLICATION_ID + ".fileprovider", File(path));

        val intent = Intent(Intent.ACTION_VIEW)
        intent.setDataAndType(uri, getMimeType(path))
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

        try {
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Toasty.error(this, "No handler for this type of file.", Toasty.LENGTH_LONG).show()
        }
    }

    private fun getMimeType(url: String): String {
        if (url.contains("jpg") || url.contains("JPG"))
            return "image/jpeg"
        else if (url.contains("pdf") || url.contains("PDF"))
            return "application/pdf";
        else if (url.contains("doc") || url.contains("DOC"))
            return "application/msword";
        else if (url.contains("docx") || url.contains("DOCX"))
            return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        else if (url.contains("xls") || url.contains("XLS"))
            return "application/vnd.ms-excel";
        else if (url.contains("xlsx") || url.contains("XLSX"))
            return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        return "";
    }

    private fun sendChatNotif(message: String) {
        var tokens = ""
        var idx = 0
        for (member in memberList) {
            if (!TextUtils.isEmpty(member.fcm_token)) {
                tokens += member.fcm_token
            }
            if (idx < memberList.lastIndex) {
                tokens += "|||"
            }
            idx++
        }

        val request = hashMapOf(
            "user_id" to profile.id,
            "user_name" to profile.fullname,
            "room_id" to roomId,
            "room_name" to roomName,
            "room_pic" to roomPic,
            "message" to message,
            "fcm_tokens" to tokens
        )

        val disposable = ServerManager.getInstance()
            .service.sendChatNotif(request)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .subscribe(
                {}, {}
            )

        compositeDisposable.add(disposable)

    }
}