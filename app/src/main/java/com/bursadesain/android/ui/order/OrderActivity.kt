package com.bursadesain.android.ui.order

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bursadesain.android.R
import com.bursadesain.android.app.Constants
import com.bursadesain.android.model.Profile
import com.bursadesain.android.model.UploadedFile
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.ui.adapter.UploadedFileAdapter
import com.bursadesain.android.ui.payment.PaymentActivity
import com.bursadesain.android.util.*
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import com.yalantis.ucrop.UCrop
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_order.*
import kotlinx.android.synthetic.main.dialog_select_picker.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.util.concurrent.TimeUnit

class OrderActivity : AppCompatActivity(), UploadedFileAdapter.Listener {

    val TAG = OrderActivity::class.java.name

    private val PICK_IMAGE = 100
    private val PICK_FILE = 200

    private val compositeDisposable = CompositeDisposable()

    lateinit var dialogPicker: BottomSheetDialog
    lateinit var dialog: ProgressDialog

    lateinit var packageId: String
    lateinit var packageTitle: String
    lateinit var packageDesc: String
    var packagePrice: Int = 0

    lateinit var profile: Profile

    private var selectedFiles = ArrayList<File>()
    private var selectedFilePaths = ArrayList<UploadedFile>()

    lateinit var uploadedFileAdapter: UploadedFileAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order)

        packageId = intent.getStringExtra("package_id") ?: ""
        packageTitle = intent.getStringExtra("package_name") ?: ""
        packageDesc = intent.getStringExtra("package_desc") ?: ""
        packagePrice = intent.getIntExtra("package_price", 0)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initProgressDialog()
        initPickerDialog()

        val profileJson = PrefUtil.read(this, Constants.PREF_PROFILE, "")
        profile = Gson().fromJson(profileJson, Profile::class.java)

        tvTitle.text = packageTitle
        tvDesc.text = packageDesc
        tvPrice.text = "Rp. ${AppUtil.convertToMoney(packagePrice)}"

        uploadedFileAdapter = UploadedFileAdapter(selectedFilePaths)
        uploadedFileAdapter.listener = this

        rvFiles.adapter = uploadedFileAdapter

        btAddFile.setOnClickListener {
            if (selectedFilePaths.size < 10) {
                dialogPicker.show()
            } else {
                Toasty.warning(this, "Tidak boleh melebihi 10 files", Toasty.LENGTH_LONG).show()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        PermissionUtil.askStoragePermissions(this)
    }

    private fun initProgressDialog() {
        dialog = ProgressDialog(this)
        dialog.setMessage("Please wait..")
        dialog.setIndeterminate(true)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)
    }

    private fun initPickerDialog() {
        val dialogView = layoutInflater.inflate(R.layout.dialog_select_picker, null)
        dialogPicker = BottomSheetDialog(this);
        dialogPicker.setContentView(dialogView);
        dialogPicker.setCanceledOnTouchOutside(true);

        dialogView.llPickerPhoto.setOnClickListener {
            dialogPicker.dismiss()
            selectImage()
        }

        dialogView.llPickerDoc.setOnClickListener {
            dialogPicker.dismiss()
            selectFile()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    fun order(view: View) {
        var payment = ""

        if (rbTransfer.isChecked) payment = "transfer"

        val notes = etNotes.text.toString();

        if (TextUtils.isEmpty(notes)) {
            Toasty.info(this, "Catatan harus diisi", Toasty.LENGTH_LONG).show()
        } else if (selectedFilePaths.size == 0) {
            Toasty.info(this, "Tambah gambar / file terlebih dahulu", Toasty.LENGTH_LONG).show()
        } else {

            if (!isFileSizeOk()) return

            val fileRequestBodies = getFileRequestBodies()

            // submit data
            AppUtil.hideKeyboard(this)

            val order = hashMapOf(
                "user_id" to RequestUtil.getBody(profile.id),
                "package_id" to RequestUtil.getBody(packageId),
                "notes" to RequestUtil.getBody(notes),
                "payment_type" to RequestUtil.getBody(payment)
            )

            val disposable = ServerManager.getInstance()
                .service.createOrder(order, fileRequestBodies)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .debounce(400, TimeUnit.MILLISECONDS)
                .doOnSubscribe { dialog.show() }
                .doOnTerminate { dialog.dismiss() }
                .subscribe(
                    {
                        if (it.equals("Gagal Upload") || it.equals("No Image")) {
                            Toasty.error(this, "Upload File gagal", Toasty.LENGTH_LONG).show()
                        } else if (it.equals("Gagal")) {
                            Toasty.error(this, "Order Anda gagal", Toasty.LENGTH_LONG).show()
                        } else {
                            // success will return order id
                            val orderId = it

                            Toasty.success(this, "Order Anda berhasil", Toasty.LENGTH_LONG).show()

                            val intent = Intent(this, PaymentActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                            intent.putExtra("after_order", true)
                            intent.putExtra("order_id", orderId)
                            intent.putExtra("package_name", packageTitle)
                            intent.putExtra("package_price", packagePrice)
                            startActivity(intent)
                        }
                    },
                    {
                        Toasty.error(this, "Order Anda gagal", Toasty.LENGTH_LONG).show()
                    }
                )

            compositeDisposable.add(disposable)
        }
    }

    private fun isFileSizeOk(): Boolean {
        for (i in selectedFiles.indices) {
            val fileSize = selectedFiles[i].length() / 1024
            val maxAllowedSize: Int = 2048

            if (fileSize > maxAllowedSize) {
                Toasty.warning(this,
                    "File dokumen / gambar tidak boleh melebihi 2MB", Toasty.LENGTH_LONG).show()
                return false
            }
        }
        return true
    }

    private fun getFileRequestBodies(): Array<MultipartBody.Part?> {
        val fileRequestBodies = arrayOfNulls<MultipartBody.Part>(selectedFilePaths.size)

        // prepare file first
        for (i in selectedFilePaths.indices) {
            val newFile = FileUtil.getInstance()
                .prepareDocumentFile(selectedFiles[i], selectedFilePaths[i].imagePath)

            val uri = Uri.fromFile(newFile)
            val mimeType =
                FileUtil.getInstance().getMimeType(FilePickerUtil.getPath(this, uri))

            val requestFile = RequestBody.create(MediaType.parse(mimeType), newFile)
            val fileRequestBody =
                MultipartBody.Part.createFormData("file[]", newFile.name, requestFile)

            fileRequestBodies[i] = fileRequestBody
        }

        return fileRequestBodies
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    fun selectImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Pilih gambar profil"), PICK_IMAGE)
    }
    
    fun selectFile() {
        val intent: Intent
        intent =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                Intent(Intent.ACTION_OPEN_DOCUMENT)
            else
                Intent(Intent.ACTION_GET_CONTENT)

        intent.type = "application/*"

        val mimetypes = arrayOf(
            "application/msword",  // doc
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",  // docx
            "application/pdf" // pdf
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes)

        startActivityForResult(Intent.createChooser(intent, "Choose Document"), PICK_FILE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            if (data == null) {
                Toasty.error(this, "Error picking image", Toasty.LENGTH_LONG).show()
                return;
            }

            val imageUri = data.getData()

            val cropFile = FileUtil.getInstance()
                .createTempFile(this, "imagefile", ".jpg")

            val options = UCrop.Options()
            options.setCompressionFormat(Bitmap.CompressFormat.JPEG)
            options.setCompressionQuality(80)
            options.setShowCropGrid(true)
            options.setFreeStyleCropEnabled(true)

            UCrop.of(imageUri!!, Uri.fromFile(cropFile))
                .withOptions(options)
                .withMaxResultSize(2048, 2048)
                .start(this)
        }
        else if (requestCode == UCrop.REQUEST_CROP) {
            if (data == null) {
                Toasty.error(this, "Error cropping image", Toasty.LENGTH_LONG).show()
                return;
            }

            if (resultCode == RESULT_OK) {
                val imageUri = UCrop.getOutput(data);

                imageUri?.path?.let {
                    val selectedFile = File(it)
                    val fileSize = selectedFile.length().toInt() / 1024
                    
                    val uploadedFile = UploadedFile(it, selectedFile.name, "image", fileSize)
                    
                    selectedFilePaths.add(uploadedFile)
                    selectedFiles.add(selectedFile)

                    uploadedFileAdapter.notifyItemInserted(selectedFilePaths.size - 1)
                }
                
            } else if (resultCode == UCrop.RESULT_ERROR) {
                Toasty.error(this, "Error cropping image", Toasty.LENGTH_LONG).show()
                Log.e("Error", "Crop error:" + UCrop.getError(data)!!.message);
            }
        }

        // pick document result
        if (requestCode == PICK_FILE) {
            if (data == null) {
                Toasty.error(this, "Error picking file", Toasty.LENGTH_LONG).show()
                return;
            }

            if (resultCode == RESULT_OK) {
                val uri = data.getData()
                
                val filePath = FilePickerUtil.getPath(this, uri)
                val selectedFile = File(filePath)
                val fileSize = selectedFile.length().toInt() / 1024
                
                val uploadedFile = UploadedFile(filePath, selectedFile.name, "doc", fileSize)
                
                selectedFilePaths.add(uploadedFile)
                selectedFiles.add(selectedFile)
                
                uploadedFileAdapter.notifyItemInserted(selectedFilePaths.size - 1)
            }
        }
    }

    override fun onFileRemoved(position: Int) {
        selectedFiles.removeAt(position)
        selectedFilePaths.removeAt(position)
        uploadedFileAdapter.notifyItemRemoved(position)
    }
}