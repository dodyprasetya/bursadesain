package com.bursadesain.android.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bursadesain.android.R
import com.bursadesain.android.app.Constants
import com.bursadesain.android.model.Order
import com.bursadesain.android.util.DateUtil
import kotlinx.android.synthetic.main.item_order.view.*

class OrderAdapter : RecyclerView.Adapter<OrderAdapter.ViewHolder>(){

    private lateinit var itemList: List<Order>
    lateinit var context: Context
    var listener: Listener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_order, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if (::itemList.isInitialized) itemList.size else 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    fun updateData(list: ArrayList<Order>) {
        itemList = list;
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind() {
            val item = itemList.get(adapterPosition)

            itemView.tvOrderId.text = "ORDER-${item.id}"
            itemView.tvTitle.text = item.package_name
            itemView.tvDesc.text = item.notes

            val date = DateUtil.getDateWithShortMonthName(context, item.created_date,
                DateUtil.MYSQL_DATE_TIME_FORMAT)
            itemView.tvDate.text = date

            itemView.btPayment.visibility = View.GONE

            when (item.status) {
                Constants.ORDER_STATUS_PENDING -> {
                    if (item.payment_proof_count > 0) {
                        itemView.tvStatus.text = "Menunggu Konfirmasi"
                    } else {
                        itemView.tvStatus.text = "Belum Dibayar"
                        itemView.btPayment.visibility = View.VISIBLE
                    }
                }
                Constants.ORDER_STATUS_PAID -> {
                    itemView.tvStatus.text = "Pembayaran Valid"
                }
                Constants.ORDER_STATUS_PAYMENT_INVALID -> {
                    itemView.tvStatus.text = "Pembayaran Tidak Valid"
                }
                Constants.ORDER_STATUS_ONPROGRESS -> {
                    itemView.tvStatus.text = "On Progress"
                }
                Constants.ORDER_STATUS_COMPLETED -> {
                    itemView.tvStatus.text = "Selesai"
                }
                Constants.ORDER_STATUS_CANCELLED -> {
                    itemView.tvStatus.text = "Dibatalkan"
                }
            }

            itemView.container.setOnClickListener {
                listener?.onItemSelected(item)
            }
            itemView.btPayment.setOnClickListener {
                listener?.onConfirmPayment(item)
            }
        }
    }

    interface Listener {
        fun onItemSelected(item: Order)
        fun onConfirmPayment(item: Order)
    }
}