package com.bursadesain.android.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.bursadesain.android.R
import com.bursadesain.android.ui.adminorder.AdminOrderHistoryActivity
import com.bursadesain.android.ui.adminorder.AdminOrderListFragment
import kotlinx.android.synthetic.main.fragment_admin_order.*

class AdminOrderFragment : Fragment() {

    val TAG = AdminOrderFragment::class.java.name

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_admin_order, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.inflateMenu(R.menu.menu_history)
        toolbar.menu.findItem(R.id.menu_history).setOnMenuItemClickListener {
            startActivity(Intent(context, AdminOrderHistoryActivity::class.java))
            true
        }

        tvPending.setOnClickListener {
            loadFragment("pending")
            tvPending.setTextColor(ContextCompat.getColor(activity!!, R.color.colorPrimary))
            tvPaid.setTextColor(ContextCompat.getColor(activity!!, R.color.black))
            tvProgress.setTextColor(ContextCompat.getColor(activity!!, R.color.black))
        }

        tvPaid.setOnClickListener {
            loadFragment("paid")
            tvPending.setTextColor(ContextCompat.getColor(activity!!, R.color.black))
            tvPaid.setTextColor(ContextCompat.getColor(activity!!, R.color.colorPrimary))
            tvProgress.setTextColor(ContextCompat.getColor(activity!!, R.color.black))
        }

        tvProgress.setOnClickListener {
            loadFragment("onprogress")
            tvPending.setTextColor(ContextCompat.getColor(activity!!, R.color.black))
            tvPaid.setTextColor(ContextCompat.getColor(activity!!, R.color.black))
            tvProgress.setTextColor(ContextCompat.getColor(activity!!, R.color.colorPrimary))
        }

        loadFragment("pending")
    }

    private fun loadFragment(status: String) {
        val fragment = AdminOrderListFragment()
        val args = Bundle()
        args.putString("status", status)
        fragment.arguments = args

        val transaction = childFragmentManager.beginTransaction()
        transaction.replace(R.id.frameLayout, fragment)
        transaction.commit()
    }
}