package com.bursadesain.android.ui.order

import android.app.Activity
import android.app.DownloadManager
import android.app.ProgressDialog
import android.app.TimePickerDialog
import android.content.*
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.text.TextUtils
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.TimePicker
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bursadesain.android.R
import com.bursadesain.android.app.Constants
import com.bursadesain.android.app.GlideApp
import com.bursadesain.android.helper.DatePickerHelper
import com.bursadesain.android.model.ChatRoom
import com.bursadesain.android.model.Order
import com.bursadesain.android.model.OrderFile
import com.bursadesain.android.model.Profile
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.ui.adapter.OrderFileAdapter
import com.bursadesain.android.ui.chat.ChatMessageActivity
import com.bursadesain.android.ui.designer.DesignerListActivity
import com.bursadesain.android.ui.payment.ConfirmPaymentActivity
import com.bursadesain.android.ui.payment.PaymentProofActivity
import com.bursadesain.android.ui.profile.ViewProfileActivity
import com.bursadesain.android.util.AppUtil
import com.bursadesain.android.util.DateUtil
import com.bursadesain.android.util.PrefUtil
import com.google.firebase.database.*
import com.google.gson.Gson
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_order_detail.*
import java.util.*
import java.util.concurrent.TimeUnit

class OrderDetailActivity : AppCompatActivity(), OrderFileAdapter.Listener {

    val TAG = OrderDetailActivity::class.java.name

    val PAYMENT_VALIDATION_REQ_CODE = 1000
    val SELECT_DESIGNER_REQ_CODE = 2000

    val db = FirebaseDatabase.getInstance()
    private val compositeDisposable = CompositeDisposable()

    lateinit var dialog: ProgressDialog
    lateinit var cancelDialog: AlertDialog

    lateinit var downloadManager: DownloadManager
    private var downloadRefId: Long = 0

    val fileList = ArrayList<OrderFile>()
    lateinit var adapter: OrderFileAdapter

    private var orderId: String = "0"
    private var designerId: String = "0"
    private var orderStatus: Int = 0

    private var orderData: Order? = null

    lateinit var profile: Profile

    lateinit var datePicker: DatePickerHelper
    lateinit var timePicker: TimePickerDialog
    lateinit var timePickerEdit: TimePickerDialog

    private var deadlineCal = Calendar.getInstance()
    private var deadlineDateTime: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_detail)

        intent.extras?.let {
            orderId = it.getString("order_id", "0")
        }

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initProgressDialog()
        initCancelOrderDialog()
        initDateTimeDialog()

        val profileJson = PrefUtil.read(this, Constants.PREF_PROFILE, "");
        profile = Gson().fromJson(profileJson, Profile::class.java)

        if (profile.type == 0 || profile.type == 2) tvChat.text = "Chat Client"
        else tvChat.text = "Chat Admin"

        adapter = OrderFileAdapter()
        adapter.listener = this
        rvFiles.adapter = adapter

        refreshLayout.setOnRefreshListener {
            loadData()
        }

        loadData()

        btConfirmPayment.setOnClickListener {
            val intent = Intent(this, ConfirmPaymentActivity::class.java)
            intent.putExtra("order_id", orderId)
            intent.putExtra("package_name", tvTitle.text.toString())
            startActivity(intent)
        }

        btViewPayment.setOnClickListener {
            val intent = Intent(this, PaymentProofActivity::class.java)
            intent.putExtra("order_id", orderId)
            intent.putExtra("package_name", tvTitle.text.toString())
            startActivityForResult(intent, PAYMENT_VALIDATION_REQ_CODE)
        }

        llDesigners.setOnClickListener {
            when (orderStatus) {
                Constants.ORDER_STATUS_PENDING,
                Constants.ORDER_STATUS_PAYMENT_INVALID-> {
                    Toasty.info(this, "Validasikan pembayaran terlebih dahulu",
                        Toasty.LENGTH_LONG).show()
                }
                Constants.ORDER_STATUS_PAID,
                Constants.ORDER_STATUS_ONPROGRESS -> {
                    val intent = Intent(this, DesignerListActivity::class.java)
                    intent.putExtra("select_mode", true)
                    startActivityForResult(intent, SELECT_DESIGNER_REQ_CODE)
                }
                Constants.ORDER_STATUS_COMPLETED -> {
                    Toasty.info(this, "Pengerjaan sudah selesai",
                        Toasty.LENGTH_LONG).show()
                }
                Constants.ORDER_STATUS_CANCELLED -> {
                    Toasty.info(this, "Order sudah dibatalkan",
                        Toasty.LENGTH_LONG).show()
                }
            }
        }

        btStartOrder.setOnClickListener {
            startOrder()
        }

        btUpdateDeadline.setOnClickListener {
            updateDeadline()
        }

        btCancelOrder.setOnClickListener {
            cancelDialog.show()
        }

        btCompleteOrder.setOnClickListener {
            completeOrder()
        }

        llChat.setOnClickListener {
            goToChat()
        }

        tvDeadline.setOnClickListener {
            val cal = Calendar.getInstance()

            val d = cal.get(Calendar.DAY_OF_MONTH)
            val m = cal.get(Calendar.MONTH)
            val y = cal.get(Calendar.YEAR)

            datePicker.showDialog(d, m, y, object : DatePickerHelper.Callback {

                override fun onDateSelected(dayofMonth: Int, month: Int, year: Int) {
                    deadlineCal.set(Calendar.DAY_OF_MONTH, dayofMonth)
                    deadlineCal.set(Calendar.MONTH, month)
                    deadlineCal.set(Calendar.YEAR, year)

                    timePicker.show()
                }
            })
        }

        tvEditDeadline.setOnClickListener {
            orderData?.let {
                val cal = DateUtil.getCalendarFromString(it.date_start,
                    DateUtil.MYSQL_DATE_TIME_FORMAT)!!

                val d = cal.get(Calendar.DAY_OF_MONTH)
                val m = cal.get(Calendar.MONTH)
                val y = cal.get(Calendar.YEAR)

                datePicker.showDialog(d, m, y, object : DatePickerHelper.Callback {

                    override fun onDateSelected(dayofMonth: Int, month: Int, year: Int) {
                        deadlineCal.set(Calendar.DAY_OF_MONTH, dayofMonth)
                        deadlineCal.set(Calendar.MONTH, month)
                        deadlineCal.set(Calendar.YEAR, year)

                        timePickerEdit.show()
                    }
                })
            }
        }

        llClient.setOnClickListener {
            orderData?.let {
                val intent = Intent(this, ViewProfileActivity::class.java)
                intent.putExtra("id", it.user_id)
                intent.putExtra("fullname", it.fullname)
                intent.putExtra("email", it.email)
                intent.putExtra("address", it.address)
                intent.putExtra("phone", it.phone)
                intent.putExtra("profile_pic", it.profile_pic)
                intent.putExtra("type", it.type)
                startActivity(intent)
            }
        }

        btEditDeadline.setOnClickListener {
            if (llEditDeadline.visibility == View.VISIBLE) {
                llEditDeadline.visibility = View.GONE
                btEditDeadline.text = "Edit Deadline"
            } else {
                llEditDeadline.visibility = View.VISIBLE
                btEditDeadline.text = "Batal Edit"
            }
        }

        btSubmitRating.setOnClickListener {
            submitRating()
        }

        hideButtons()

        registerDownloadReceiver()
    }

    private fun initProgressDialog() {
        dialog = ProgressDialog(this);
        dialog.setMessage("Please wait..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
    }

    private fun initCancelOrderDialog() {
        cancelDialog = AlertDialog.Builder(this)
            .setTitle("Batalkan Order")
            .setMessage("Apakah Anda yakin ingin membatalkan order ini?")
            .setCancelable(true)
            .setPositiveButton("Ya", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, position: Int) {
                    cancelDialog.dismiss()
                    cancelOrder()
                }
            })
            .setNegativeButton("Tidak", null)
            .create()
    }

    private fun initDateTimeDialog() {
        datePicker = DatePickerHelper(this)

        val cal = Calendar.getInstance()
        val hour = cal.get(Calendar.HOUR_OF_DAY)
        val minute = cal.get(Calendar.MINUTE)

        timePicker = TimePickerDialog(this, object : TimePickerDialog.OnTimeSetListener {

            override fun onTimeSet(dialog: TimePicker?, hourOfDay: Int, minute: Int) {
                deadlineCal.set(Calendar.HOUR_OF_DAY, hourOfDay)
                deadlineCal.set(Calendar.MINUTE, minute)
                deadlineCal.set(Calendar.SECOND, 0)

                deadlineDateTime = DateUtil.getFormattedCalendar(deadlineCal, DateUtil.MYSQL_DATE_TIME_FORMAT)
                tvDeadline.text = DateUtil.getFormattedCalendar(deadlineCal, DateUtil.LOCAL_DATE_TIME_FORMAT)
                tvDuration.text = getDeadlineDuration(Calendar.getInstance(), deadlineCal)
            }
        }, hour, minute, true)

        timePickerEdit = TimePickerDialog(this, object : TimePickerDialog.OnTimeSetListener {

            override fun onTimeSet(dialog: TimePicker?, hourOfDay: Int, minute: Int) {
                deadlineCal.set(Calendar.HOUR_OF_DAY, hourOfDay)
                deadlineCal.set(Calendar.MINUTE, minute)
                deadlineCal.set(Calendar.SECOND, 0)

                deadlineDateTime = DateUtil.getFormattedCalendar(deadlineCal, DateUtil.MYSQL_DATE_TIME_FORMAT)
                tvEditDeadline.text = DateUtil.getFormattedCalendar(deadlineCal, DateUtil.LOCAL_DATE_TIME_FORMAT)
                tvEditDuration.text = getDeadlineDuration(Calendar.getInstance(), deadlineCal)
            }
        }, hour, minute, true)
    }

    private fun getDeadlineDuration(startCal: Calendar, endCal: Calendar): String {
        val start = startCal.timeInMillis
        val end = endCal.timeInMillis
        val durationHours = (end - start) / (3600 * 1000)

        if (durationHours < 24) {

            val durationMinute = (end - start) / (60 * 1000) % 60

            if (durationMinute.toInt() > 0) {
                return "Durasi ${durationHours.toInt()} jam ${durationMinute.toInt()} menit"
            } else {
                return "Durasi ${durationHours.toInt()} jam"
            }
        } else {
            val day = (durationHours / 24).toInt()
            val hour = (durationHours % 24).toInt()
            return "Durasi ${day} hari ${hour} jam"
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PAYMENT_VALIDATION_REQ_CODE) {
                loadData()
            } else if (requestCode == SELECT_DESIGNER_REQ_CODE) {
                data?.let {
                    val designerId = it.getStringExtra("designer_id") ?: "0"
                    assignDesigner(designerId)
                }
            }
        }
    }

    private fun hideButtons() {
        btConfirmPayment.visibility = View.GONE
        btViewPayment.visibility = View.GONE
        btCancelOrder.visibility = View.GONE
        btCompleteOrder.visibility = View.GONE
    }

    private fun loadData() {
        val disposable = ServerManager.getInstance()
            .service.loadOrderDetail(orderId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { refreshLayout.isRefreshing = true }
            .doOnTerminate { refreshLayout.isRefreshing = false }
            .subscribe(
                {
                    fileList.clear()
                    fileList.addAll(it.files)
                    adapter.updateData(fileList)

                    orderData = it

                    tvOrderId.text = "ORDER-${it.id}"

                    tvDate.text = DateUtil.getDateWithShortMonthName(this,
                        it.created_date, DateUtil.MYSQL_DATE_TIME_FORMAT)

                    tvTitle.text = it.package_name
                    tvDesc.text = it.description
                    tvPrice.text = "Rp. ${AppUtil.convertToMoney(it.price)}"
                    tvNotes.text = it.notes

                    hideButtons()

                    orderStatus = it.status
                    when (it.status) {
                        Constants.ORDER_STATUS_PENDING -> {
                            if (it.payment_proof_count > 0) {

                                tvStatus.text = "Menunggu Konfirmasi"
                                if (profile.type == 0) btViewPayment.visibility = View.VISIBLE

                            } else {
                                tvStatus.text = "Belum Dibayar"
                                if (profile.type == 1) btConfirmPayment.visibility = View.VISIBLE
                            }

                            if (profile.type != 2)
                                btCancelOrder.visibility = View.VISIBLE
                        }
                        Constants.ORDER_STATUS_PAID -> {
                            tvStatus.text = "Pembayaran Valid"

                            if (profile.type != 2)
                                btCancelOrder.visibility = View.VISIBLE
                        }
                        Constants.ORDER_STATUS_PAYMENT_INVALID -> {
                            tvStatus.text = "Pembayaran Tidak Valid"

                            if (profile.type == 1) {
                                btConfirmPayment.visibility = View.VISIBLE
                                btConfirmPayment.text = "Konfirmasi Ulang"
                            }

                            if (profile.type != 2)
                                btCancelOrder.visibility = View.VISIBLE
                        }
                        Constants.ORDER_STATUS_ONPROGRESS -> {
                            tvStatus.text = "On Progress"

                            showDeadlineInfo(it)

                            if (profile.type == 0) btCompleteOrder.visibility = View.VISIBLE

                            if (profile.type != 2)
                                btCancelOrder.visibility = View.VISIBLE
                        }
                        Constants.ORDER_STATUS_COMPLETED -> {
                            tvStatus.text = "Selesai"
                            btCancelOrder.visibility = View.GONE
                            llDeadline.visibility = View.GONE
                            llEditDeadline.visibility = View.GONE
                            llRating.visibility = View.VISIBLE

                            if (it.rating == 0f && profile.type == 1) { // client
                                ratingBar.isSaveEnabled = true
                                ratingBar.setIsIndicator(false)
                                btSubmitRating.visibility = View.VISIBLE
                            } else {
                                ratingBar.isSaveEnabled = false
                                ratingBar.rating = it.rating
                                ratingBar.setIsIndicator(true)
                                btSubmitRating.visibility = View.GONE
                            }
                        }
                        Constants.ORDER_STATUS_CANCELLED -> {
                            tvStatus.text = "Dibatalkan"
                            btCancelOrder.visibility = View.GONE
                            llDeadline.visibility = View.GONE
                            llEditDeadline.visibility = View.GONE
                            llRating.visibility = View.GONE
                        }
                    }

                    // if admin
                    if (profile.type == 0) {
                        llClient.visibility = View.VISIBLE
                        llDesigners.visibility = View.VISIBLE

                        if (it.status == Constants.ORDER_STATUS_PAID) {
                            llStartOrder.visibility = View.VISIBLE
                        } else {
                            llStartOrder.visibility = View.GONE
                        }

                        tvClient.text = it.fullname
                        GlideApp.with(this)
                            .load(it.profile_pic)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .circleCrop()
                            .into(ivClient)

                        if (!it.designer_id.equals("0")) {
                            designerId = it.designer_id

                            tvDesigner.text = it.designer_name
                            GlideApp.with(this)
                                .load(it.designer_profile_pic)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .circleCrop()
                                .into(ivDesigner)
                        } else {
                            tvDesigner.text = "Semua Designer"
                            ivDesigner.setImageDrawable(ContextCompat.getDrawable(this,
                                R.drawable.image_default_user))
                        }
                    }

                    // if designer
                    else if (profile.type == 2) {
                        llClient.visibility = View.VISIBLE

                        tvClient.text = it.fullname
                        GlideApp.with(this)
                            .load(it.profile_pic)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .circleCrop()
                            .into(ivClient)
                    }

                },
                {
                    Log.d(TAG, "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    private fun showDeadlineInfo(order: Order) {
        llDeadline.visibility = View.VISIBLE

        deadlineCal = DateUtil.getCalendarFromString(order.date_end,
            DateUtil.MYSQL_DATE_TIME_FORMAT)

        if (deadlineCal.timeInMillis > Calendar.getInstance().timeInMillis) {
            tvLabelDeadline.text = "Waktu deadline dalam:"
            tvDeadlineRemaining.text = getDeadlineDuration(Calendar.getInstance(), deadlineCal)
            llDeadline.setBackgroundColor(ContextCompat.getColor(this, R.color.package5))
        } else {
            tvLabelDeadline.text = "Waktu deadline sudah"
            tvDeadlineRemaining.text = "Over Deadline"
            llDeadline.setBackgroundColor(ContextCompat.getColor(this, R.color.package4))
        }

        if (profile.type == 0) {
            btEditDeadline.visibility = View.VISIBLE
        } else {
            btEditDeadline.visibility = View.GONE
        }
    }

    private fun assignDesigner(designerId: String) {
        val disposable = ServerManager.getInstance()
            .service.assignDesigner(orderId, designerId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { dialog.show() }
            .doOnTerminate { dialog.dismiss() }
            .subscribe(
                {
                    if (it.equals("Berhasil")) {
                        Toasty.success(this, "Memilih designer berhasil", Toasty.LENGTH_LONG).show()
                        loadData()
                    } else {
                        Toasty.error(this, "Memilih designer gagal", Toasty.LENGTH_LONG).show()
                    }
                },
                {
                    Toasty.error(this, "Memilih designer gagal", Toasty.LENGTH_LONG).show()
                }
            )

        compositeDisposable.add(disposable)
    }

    private fun startOrder() {
        if (TextUtils.isEmpty(deadlineDateTime)) {
            Toasty.info(this, "Pilih tanggal deadline terlebih dahulu",
                Toasty.LENGTH_LONG).show()
            return
        }

        val disposable = ServerManager.getInstance()
            .service.startOrder(orderId, deadlineDateTime)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { dialog.show() }
            .doOnTerminate { dialog.dismiss() }
            .subscribe(
                {
                    if (it.equals("Berhasil")) {
                        deadlineDateTime = ""
                        Toasty.success(this, "Pengerjaan dimulai", Toasty.LENGTH_LONG).show()
                        loadData()
                    } else {
                        Toasty.error(this, "Request gagal", Toasty.LENGTH_LONG).show()
                    }
                },
                {
                    Toasty.error(this, "Request gagal", Toasty.LENGTH_LONG).show()
                }
            )

        compositeDisposable.add(disposable)
    }

    private fun cancelOrder() {
        val disposable = ServerManager.getInstance()
            .service.cancelOrder(orderId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { dialog.show() }
            .doOnTerminate { dialog.dismiss() }
            .subscribe(
                {
                    if (it.equals("Berhasil")) {
                        Toasty.success(this, "Order dibatalkan", Toasty.LENGTH_LONG).show()
                        loadData()
                    } else {
                        Toasty.error(this, "Request gagal", Toasty.LENGTH_LONG).show()
                    }
                },
                {
                    Toasty.error(this, "Request gagal", Toasty.LENGTH_LONG).show()
                }
            )

        compositeDisposable.add(disposable)
    }

    private fun completeOrder() {
        val disposable = ServerManager.getInstance()
            .service.completeOrder(orderId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { dialog.show() }
            .doOnTerminate { dialog.dismiss() }
            .subscribe(
                {
                    if (it.equals("Berhasil")) {
                        Toasty.success(this, "Pengerjaan selesai", Toasty.LENGTH_LONG).show()
                        loadData()
                    } else {
                        Toasty.error(this, "Request gagal", Toasty.LENGTH_LONG).show()
                    }
                },
                {
                    Toasty.error(this, "Request gagal", Toasty.LENGTH_LONG).show()
                }
            )

        compositeDisposable.add(disposable)
    }

    private fun submitRating() {
        val rating = ratingBar.rating

        if (rating > 0) {
            val disposable = ServerManager.getInstance()
                .service.submitRating(orderId, rating)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .debounce(400, TimeUnit.MILLISECONDS)
                .doOnSubscribe { dialog.show() }
                .doOnTerminate { dialog.dismiss() }
                .subscribe(
                    {
                        if (it.equals("Berhasil")) {
                            Toasty.success(this, "Submit rating sukses", Toasty.LENGTH_LONG).show()

                            ratingBar.isSaveEnabled = false
                            ratingBar.setIsIndicator(true)
                            btSubmitRating.visibility = View.GONE

                        } else {
                            Toasty.error(this, "Submit rating gagal", Toasty.LENGTH_LONG).show()
                        }
                    },
                    {
                        Toasty.error(this, "Submit rating gagal", Toasty.LENGTH_LONG).show()
                    }
                )

            compositeDisposable.add(disposable)
        }
    }


    private fun updateDeadline() {
        if (TextUtils.isEmpty(deadlineDateTime)) {
            Toasty.info(this, "Pilih tanggal deadline terlebih dahulu",
                Toasty.LENGTH_LONG).show()
            return
        }

        val disposable = ServerManager.getInstance()
            .service.updateDeadline(orderId, deadlineDateTime)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { dialog.show() }
            .doOnTerminate { dialog.dismiss() }
            .subscribe(
                {
                    if (it.equals("Berhasil")) {
                        deadlineDateTime = ""
                        llEditDeadline.visibility = View.GONE
                        btEditDeadline.text = "Edit Deadline"

                        Toasty.success(this, "Deadline berhasil diupdate", Toasty.LENGTH_LONG).show()
                        loadData()
                    } else {
                        Toasty.error(this, "Request gagal", Toasty.LENGTH_LONG).show()
                    }
                },
                {
                    Toasty.error(this, "Request gagal", Toasty.LENGTH_LONG).show()
                }
            )

        compositeDisposable.add(disposable)
    }

    private fun goToChat() {
        val query = db.reference.child("chat_rooms").child(orderId)

        val listener = object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    createChatRoom()
                } else {
                    val item = dataSnapshot.getValue(ChatRoom::class.java)
                    item?.id = dataSnapshot.key ?: ""

                    item?.let {
                        goToChatDetail(it.id, it.name, it.pic)
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Toasty.error(this@OrderDetailActivity, "Gagal koneksi ke chat",
                    Toasty.LENGTH_LONG).show()
            }
        }

        query.addListenerForSingleValueEvent(listener)
    }

    private fun createChatRoom() {
        val timestamp = DateUtil.getFormattedCalendar(Calendar.getInstance(),
            DateUtil.MYSQL_DATE_TIME_FORMAT)

        val ref = db.reference.child("chat_rooms").child(orderId)

        orderData?.let {
            val chatRoom = hashMapOf<String, Any>(
                "name" to it.package_name,
                "pic" to "",
                "status" to "1",
                "timestamp" to timestamp,
                "latest_message" to "",
                "designer_id" to it.designer_id,
                "member_id" to it.user_id,
                "typing" to ""
            )

            ref.updateChildren(chatRoom, object : DatabaseReference.CompletionListener {
                override fun onComplete(error: DatabaseError?, reference: DatabaseReference) {
                    goToChatDetail(orderId, it.package_name, "")
                }
            })
        }
    }

    private fun goToChatDetail(id: String, name: String, pic: String) {
        val intent = Intent(this, ChatMessageActivity::class.java)
        intent.putExtra("room_id", id)
        intent.putExtra("room_name", name)
        intent.putExtra("room_pic", pic)
        startActivity(intent)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onItemDownload(item: OrderFile) {
        downloadRefId = downloadFile(item.file)
    }

    private fun downloadFile(url: String): Long {
        val uri = Uri.parse(url)

        downloadManager = getSystemService(DOWNLOAD_SERVICE) as DownloadManager
        val request = DownloadManager.Request(uri);

        val fileName = url.split("/").last()

        request.setTitle(fileName);
        request.setDescription(fileName);
        request.setMimeType(getMimeType(fileName));
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);

        return downloadManager.enqueue(request)
    }

    private fun getMimeType(url: String): String {
        if (url.contains("jpg") || url.contains("JPG"))
            return "image/jpeg"
        else if (url.contains("pdf") || url.contains("PDF"))
            return "application/pdf";
        else if (url.contains("doc") || url.contains("DOC"))
            return "application/msword";
        else if (url.contains("docx") || url.contains("DOCX"))
            return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        else if (url.contains("xls") || url.contains("XLS"))
            return "application/vnd.ms-excel";
        else if (url.contains("xlsx") || url.contains("XLSX"))
            return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        return "";
    }

    private val downloadReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            val refId = intent?.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

            if (refId == downloadRefId) {
                Toasty.success(this@OrderDetailActivity,
                    "Download selesai, silahkan cek di folder Downloads Anda",
                    Toasty.LENGTH_LONG)
                    .show()
            }
        }

    }
    private fun registerDownloadReceiver() {
        val filter = IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
        registerReceiver(downloadReceiver, filter);
    }
}