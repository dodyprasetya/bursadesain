package com.bursadesain.android.ui.member

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.bursadesain.android.R
import com.bursadesain.android.model.Profile
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.ui.adapter.MemberAdapter
import com.bursadesain.android.ui.profile.ViewProfileActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_member.*
import java.util.concurrent.TimeUnit

class MemberActivity : AppCompatActivity(), MemberAdapter.Listener {

    val TAG = MemberActivity::class.java.name

    private val compositeDisposable = CompositeDisposable()

    val itemList = ArrayList<Profile>()
    lateinit var adapter: MemberAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_member)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        adapter = MemberAdapter()
        adapter.listener = this
        rvMembers.adapter = adapter

        refreshLayout.setOnRefreshListener {
            loadData()
        }

        loadData()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun loadData() {
        val disposable = ServerManager.getInstance()
            .service.loadMembers()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { refreshLayout.isRefreshing = true }
            .doOnTerminate { refreshLayout.isRefreshing = false }
            .subscribe(
                {
                    itemList.clear()
                    itemList.addAll(it)
                    adapter.updateData(itemList)
                },
                {
                    Log.d(TAG, "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onItemSelected(item: Profile) {
        val intent = Intent(this, ViewProfileActivity::class.java)
        intent.putExtra("id", item.id)
        intent.putExtra("fullname", item.fullname)
        intent.putExtra("email", item.email)
        intent.putExtra("address", item.address)
        intent.putExtra("phone", item.phone)
        intent.putExtra("profile_pic", item.profile_pic)
        intent.putExtra("type", item.type)
        startActivity(intent)
    }

    override fun onCall(item: Profile) {
        val phone = item.phone
        val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
        startActivity(intent)
    }

    override fun onSendEmail(item: Profile) {
        val email = item.email
        val intent = Intent(Intent.ACTION_SENDTO,
            Uri.fromParts("mailto", email, null))

        intent.putExtra(Intent.EXTRA_SUBJECT, "")
        intent.putExtra(Intent.EXTRA_TEXT, "")
        startActivity(Intent.createChooser(intent, "Kirim Email..."))
    }

    override fun onChatWhatsapp(item: Profile) {
        val url = "https://api.whatsapp.com/send?phone=${item.phone}"
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }
}