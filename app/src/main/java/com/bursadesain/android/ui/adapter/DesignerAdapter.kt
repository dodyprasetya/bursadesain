package com.bursadesain.android.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bursadesain.android.R
import com.bursadesain.android.app.GlideApp
import com.bursadesain.android.model.Profile
import kotlinx.android.synthetic.main.item_member.view.*

class DesignerAdapter : RecyclerView.Adapter<DesignerAdapter.ViewHolder>(){

    private lateinit var itemList: List<Profile>
    private var context: Context? = null
    var listener: Listener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_designer, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if (::itemList.isInitialized) itemList.size else 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    fun updateData(list: ArrayList<Profile>) {
        itemList = list;
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind() {
            val item = itemList.get(adapterPosition)

            itemView.tvName.text = item.fullname

            GlideApp.with(context!!)
                .load(item.profile_pic)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .circleCrop()
                .into(itemView.ivProfile)

            itemView.container.setOnClickListener {
                listener?.onItemSelected(item)
            }

            itemView.container.setOnLongClickListener {
                listener?.onItemLongClicked(item)
                true
            }
        }
    }

    interface Listener {
        fun onItemSelected(item: Profile)
        fun onItemLongClicked(item: Profile)
    }
}