package com.bursadesain.android.ui.payment

import android.app.Activity
import android.app.ProgressDialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bursadesain.android.BuildConfig
import com.bursadesain.android.R
import com.bursadesain.android.app.GlideApp
import com.bursadesain.android.model.PaymentProof
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.util.AppUtil
import com.bursadesain.android.util.DateUtil
import com.bursadesain.android.util.FileUtil
import com.bursadesain.android.util.PermissionUtil
import com.downloader.Error
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_payment_proof.*
import java.io.File
import java.util.concurrent.TimeUnit

class PaymentProofActivity : AppCompatActivity() {

    val TAG = PaymentProofActivity::class.java.name

    lateinit var dialog: ProgressDialog

    private val compositeDisposable = CompositeDisposable()

    private var proofId: String = "0"
    private var orderId: String = "0"
    private var packageTitle: String = ""
    private var fileUrl: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_proof)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initProgressDialog()

        intent.extras?.let {
            orderId = it.getString("order_id", "0")
            packageTitle = it.getString("package_name", "")

            tvOrderId.text = "ORDER-${orderId}"
            tvTitle.text = packageTitle
        }

        refreshLayout.setOnRefreshListener {
            loadData()
        }

        loadData()

        btValid.setOnClickListener {
            validatePayment(true)
        }

        btNotValid.setOnClickListener {
            validatePayment(false)
        }

        tvDownload.setOnClickListener {
            val fileName = fileUrl.split("/").last()

            val filePath = "${FileUtil.DOWNLOAD_FOLDER_PATH}/${fileName}"
            val path = File(filePath)

            if (path.exists()) {
                openFile(filePath)
            } else {
                downloadFile()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onStart() {
        super.onStart()
        PermissionUtil.askStoragePermissions(this)
    }

    private fun initProgressDialog() {
        dialog = ProgressDialog(this);
        dialog.setMessage("Please wait..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
    }

    private fun loadData() {
        val disposable = ServerManager.getInstance()
            .service.loadPaymentProofs(orderId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { refreshLayout.isRefreshing = true }
            .doOnTerminate { refreshLayout.isRefreshing = false }
            .subscribe(
                {
                    if (it.size > 0) setData(it.first())
                },
                {
                    Log.d(TAG, "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    private fun setData(it: PaymentProof) {
        proofId = it.id
        fileUrl = it.file

        etFullname.setText(it.account_name)
        etBankName.setText(it.bank_name)
        etBankAccNumber.setText(it.account_number)
        etTotalPaid.setText("${AppUtil.convertToMoney(it.payment)}")
        etDate.setText(DateUtil.getDateWithShortMonthName(this,
            it.payment_date, DateUtil.MYSQL_DATE_FORMAT))

        GlideApp.with(this)
            .load(it.file)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .into(ivPaymentProof)
    }

    private fun validatePayment(isValid: Boolean) {
        val disposable = ServerManager.getInstance()
            .service.validatePayment(orderId, proofId, isValid)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { dialog.show() }
            .doOnTerminate { dialog.dismiss() }
            .subscribe(
                {
                    if (it.equals("Berhasil")) {
                        Toasty.success(this, "Validasi berhasil", Toasty.LENGTH_LONG).show()

                        setResult(Activity.RESULT_OK)
                        finish()

                    } else {
                        Toasty.error(this, "Validasi gagal", Toasty.LENGTH_LONG).show()
                    }
                },
                {
                    Toasty.error(this, "Validasi gagal", Toasty.LENGTH_LONG).show()
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    private fun downloadFile() {
        dialog.show()

        val fileDir = "${FileUtil.DOWNLOAD_FOLDER_PATH}/"
        val fileName = fileUrl.split("/").last()

        PRDownloader.download(fileUrl, fileDir, fileName)
            .build()
            .start(object : OnDownloadListener {

                override fun onDownloadComplete() {
                    dialog.dismiss()
                    openFile(fileDir + fileName)
                }

                override fun onError(error: Error?) {
                    dialog.dismiss()
                    Toasty.error(this@PaymentProofActivity, "Download file gagal",
                        Toasty.LENGTH_LONG).show()
                }
            })
    }

    private fun openFile(path: String) {
        val uri = FileProvider.getUriForFile(this,
            BuildConfig.APPLICATION_ID + ".fileprovider", File(path));

        val intent = Intent(Intent.ACTION_VIEW)
        intent.setDataAndType(uri, getMimeType(path))
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

        try {
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Toasty.error(this, "No handler for this type of file.", Toasty.LENGTH_LONG).show()
        }
    }

    private fun getMimeType(url: String): String {
        if (url.contains("jpg") || url.contains("JPG"))
            return "image/jpeg"
        else if (url.contains("pdf") || url.contains("PDF"))
            return "application/pdf";
        else if (url.contains("doc") || url.contains("DOC"))
            return "application/msword";
        else if (url.contains("docx") || url.contains("DOCX"))
            return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        else if (url.contains("xls") || url.contains("XLS"))
            return "application/vnd.ms-excel";
        else if (url.contains("xlsx") || url.contains("XLSX"))
            return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        return "";
    }
}