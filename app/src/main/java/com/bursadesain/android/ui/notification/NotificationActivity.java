package com.bursadesain.android.ui.notification;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.bursadesain.android.R;
import com.bursadesain.android.model.Notification;
import com.bursadesain.android.ui.adapter.NotificationAdapter;
import com.bursadesain.android.ui.chat.ChatMessageActivity;
import com.bursadesain.android.ui.order.OrderDetailActivity;
import com.bursadesain.android.util.TinyDB;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NotificationActivity extends AppCompatActivity implements NotificationAdapter.Listener {

    private NotificationAdapter mAdapter;
    private List<Notification> itemList;

    private RecyclerView recyclerView;

    DatabaseReference rootRef;

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = findViewById(R.id.rvNotifications);

        TinyDB tinyDB = new TinyDB(this);
        ArrayList<Object> list = tinyDB.getListObject("notifications", Notification.class);
        Collections.reverse(list);

        itemList = new ArrayList<>();

        for (Object obj: list) {
            Notification notification = (Notification) obj;
            itemList.add(notification);
        }

        mAdapter = new NotificationAdapter(itemList, this);
        mAdapter.setListener(this);

        recyclerView.setAdapter(mAdapter);

        tinyDB.putInt("new_notification_count", 0);
    }

    @Override
    public void onItemClicked(final Notification item) {
        if (item.getType().equals("ORDER")) {
            Intent intent = new Intent(this, OrderDetailActivity.class);
            intent.putExtra("order_id", item.getId());
            startActivity(intent);
        } else if (item.getType().equals("CHAT")) {
            Intent intent = new Intent(this, ChatMessageActivity.class);
            intent.putExtra("room_id", item.getId());
            intent.putExtra("room_name", item.getRoom_name());
            intent.putExtra("room_pic", item.getRoom_pic());
            startActivity(intent);
        }
    }
}
