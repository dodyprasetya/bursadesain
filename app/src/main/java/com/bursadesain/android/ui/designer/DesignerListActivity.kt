package com.bursadesain.android.ui.designer

import android.app.Activity
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bursadesain.android.R
import com.bursadesain.android.model.Profile
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.ui.adapter.DesignerAdapter
import com.bursadesain.android.ui.profile.ViewProfileActivity
import com.google.android.material.bottomsheet.BottomSheetDialog
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_designer_list.*
import kotlinx.android.synthetic.main.dialog_package_action.view.*
import java.util.concurrent.TimeUnit

class DesignerListActivity : AppCompatActivity(), DesignerAdapter.Listener {

    val TAG = DesignerListActivity::class.java.name

    private val ADD_DESIGNER_REQ_CODE = 1000

    private val compositeDisposable = CompositeDisposable()

    val itemList = ArrayList<Profile>()
    lateinit var adapter: DesignerAdapter

    private var isSelectMode: Boolean = false

    lateinit var dialog: ProgressDialog
    lateinit var dialogAction: BottomSheetDialog
    lateinit var deleteDialog: AlertDialog

    private var selectedDesignerId: String = "0"
    lateinit var selectedDesigner: Profile

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_designer_list)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initProgressDialog()
        initActionDialog()
        initDeleteDialog()

        intent.extras?.let {
            isSelectMode = it.getBoolean("select_mode", false)
        }

        if (isSelectMode) {
            toolbar.title = "Pilih Designer"
            tvNotice.visibility = View.GONE
        } else {
            tvNotice.visibility = View.VISIBLE
        }

        adapter = DesignerAdapter()
        adapter.listener = this
        rvDesigners.adapter = adapter

        refreshLayout.setOnRefreshListener {
            loadData()
        }

        loadData()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (!isSelectMode) {
            menuInflater.inflate(R.menu.menu_add, menu)
            return true
        } else {
            return false
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
            R.id.menu_add -> {
                startActivityForResult(Intent(this, AddDesignerActivity::class.java),
                    ADD_DESIGNER_REQ_CODE)
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun initProgressDialog() {
        dialog = ProgressDialog(this);
        dialog.setMessage("Please wait..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
    }

    private fun initActionDialog() {
        val dialogView = layoutInflater.inflate(R.layout.dialog_package_action, null)
        dialogAction = BottomSheetDialog(this);
        dialogAction.setContentView(dialogView);
        dialogAction.setCanceledOnTouchOutside(true);

        dialogView.llEdit.visibility = View.VISIBLE
        dialogView.llEdit.setOnClickListener {
            dialogAction.dismiss()

            val intent = Intent(this, EditDesignerActivity::class.java)
            intent.putExtra("designer_id", selectedDesigner.id)
            intent.putExtra("fullname", selectedDesigner.fullname)
            intent.putExtra("email", selectedDesigner.email)
            intent.putExtra("address", selectedDesigner.address)
            intent.putExtra("phone", selectedDesigner.phone)
            intent.putExtra("profile_pic", selectedDesigner.profile_pic)
            intent.putExtra("can_post_package", selectedDesigner.can_post_package)
            startActivityForResult(intent, ADD_DESIGNER_REQ_CODE)
        }

        dialogView.llDelete.visibility = View.VISIBLE

        dialogView.llDelete.setOnClickListener {
            dialogAction.dismiss()
            deleteDialog.show()
        }
    }

    private fun initDeleteDialog() {
        deleteDialog = AlertDialog.Builder(this)
            .setTitle("Hapus Data")
            .setMessage("Apakah Anda yakin ingin menghapus data ini?")
            .setCancelable(true)
            .setPositiveButton("Ya", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, position: Int) {
                    deleteDialog.dismiss()
                    deleteData()
                }
            })
            .setNegativeButton("Tidak", null)
            .create()
    }

    private fun loadData() {
        val disposable = ServerManager.getInstance()
            .service.loadDesigners()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { refreshLayout.isRefreshing = true }
            .doOnTerminate { refreshLayout.isRefreshing = false }
            .subscribe(
                {
                    itemList.clear()

                    if (isSelectMode) {
                        val allUser = Profile()
                        allUser.id = "0"
                        allUser.fullname = "Semua Designer"

                        itemList.add(allUser)
                    }

                    itemList.addAll(it)
                    adapter.updateData(itemList)
                },
                {
                    Log.d(TAG, "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onItemSelected(item: Profile) {
        if (isSelectMode) {
            val result = Intent()
            result.putExtra("designer_id", item.id)
            setResult(Activity.RESULT_OK, result)
            finish()
        } else {
            val intent = Intent(this, ViewProfileActivity::class.java)
            intent.putExtra("id", item.id)
            intent.putExtra("fullname", item.fullname)
            intent.putExtra("email", item.email)
            intent.putExtra("address", item.address)
            intent.putExtra("phone", item.phone)
            intent.putExtra("profile_pic", item.profile_pic)
            intent.putExtra("type", item.type)
            startActivity(intent)
        }
    }

    override fun onItemLongClicked(item: Profile) {
        if (!isSelectMode) {
            selectedDesigner = item
            selectedDesignerId = item.id
            dialogAction.show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == ADD_DESIGNER_REQ_CODE && resultCode == Activity.RESULT_OK) {
            loadData()
        }
    }

    private fun deleteData() {
        val disposable = ServerManager.getInstance()
            .service.deleteDesigner(selectedDesignerId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { dialog.show() }
            .doOnTerminate { dialog.dismiss() }
            .subscribe(
                {
                    if (it.equals("Berhasil")) {
                        Toasty.success(this, "Hapus designer berhasil", Toasty.LENGTH_LONG).show()
                        loadData()
                    } else {
                        Toasty.error(this, "Hapus designer gagal", Toasty.LENGTH_LONG).show()
                    }
                },
                {
                    Toasty.error(this, "Hapus designer gagal", Toasty.LENGTH_LONG).show()
                }
            )

        compositeDisposable.add(disposable)
    }
}