package com.bursadesain.android.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bursadesain.android.R
import com.bursadesain.android.model.PackageCategory
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.ui.adapter.PackageCategoryAdapter
import com.bursadesain.android.ui.login.LoginActivity
import com.bursadesain.android.ui.notification.NotificationActivity
import com.bursadesain.android.ui.packages.PackagesActivity
import com.bursadesain.android.ui.packages.SearchPackagesActivity
import com.bursadesain.android.util.AppUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.concurrent.TimeUnit

class HomeFragment : Fragment(), PackageCategoryAdapter.Listener {

    val TAG = HomeFragment::class.java.name

    private val compositeDisposable = CompositeDisposable()

    val packageList = ArrayList<PackageCategory>()
    lateinit var adapter: PackageCategoryAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.inflateMenu(R.menu.menu_notification)
        toolbar.menu.findItem(R.id.menu_notification).setOnMenuItemClickListener {

            AppUtil.isLogin(context!!).also {
                if (it) startActivity(Intent(context, NotificationActivity::class.java))
                else startActivity(Intent(context, LoginActivity::class.java))
            }

            true
        }

        adapter = PackageCategoryAdapter()
        adapter.listener = this

        rvPackages.adapter = adapter

        refreshLayout.setOnRefreshListener {
            loadData()
        }

        loadData()

        tvSearch.setOnClickListener {
            startActivity(Intent(context, SearchPackagesActivity::class.java))
        }
    }

    private fun loadData() {
        val disposable = ServerManager.getInstance()
            .service.loadPackageCategories()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { refreshLayout.isRefreshing = true }
            .doOnTerminate { refreshLayout.isRefreshing = false }
            .subscribe(
                {
                    packageList.clear()
                    packageList.addAll(it)
                    adapter.updateData(packageList)
                },
                {
                    Log.d(TAG, "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onItemSelected(item: PackageCategory) {
        val intent = Intent(activity, PackagesActivity::class.java)
        intent.putExtra("category_id", item.id)
        intent.putExtra("package_name", item.name)
        startActivity(intent)
    }
}