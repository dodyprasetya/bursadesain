package com.bursadesain.android.ui.payment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bursadesain.android.R
import com.bursadesain.android.model.Profile
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.ui.adapter.MemberAdapter
import com.bursadesain.android.ui.main.MainActivity
import com.bursadesain.android.util.AppUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_payment.*
import java.util.concurrent.TimeUnit

class PaymentActivity : AppCompatActivity() {

    val TAG = PaymentActivity::class.java.name

    private val compositeDisposable = CompositeDisposable()

    private var orderId: String = "0"
    private var packageTitle: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)

        setSupportActionBar(toolbar)

        intent.extras?.let {
            val isAfterOrder = it.getBoolean("after_order")

            if (isAfterOrder) {
                ivClose.visibility = View.VISIBLE
            } else {
                supportActionBar?.setDisplayHomeAsUpEnabled(true)
            }

            orderId = it.getString("order_id", "0")
            packageTitle = it.getString("package_name", "")
            val packagePrice = it.getInt("package_price")

            tvOrderId.text = "ORDER-${orderId}"
            tvTitle.text = packageTitle
            tvPrice.text = "Rp. ${AppUtil.convertToMoney(packagePrice)}"
        }

        refreshLayout.setOnRefreshListener {
            loadData()
        }

        loadData()

        ivClose.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            intent.putExtra("selected_tab", 1)
            startActivity(intent)
        }

        btConfirmPayment.setOnClickListener {
            val intent = Intent(this, ConfirmPaymentActivity::class.java)
            intent.putExtra("order_id", orderId)
            intent.putExtra("package_name", packageTitle)
            startActivity(intent)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun loadData() {
//        val disposable = ServerManager.getInstance()
//            .service.loadDesigners()
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .debounce(400, TimeUnit.MILLISECONDS)
//            .doOnSubscribe { refreshLayout.isRefreshing = true }
//            .doOnTerminate { refreshLayout.isRefreshing = false }
//            .subscribe(
//                {
//                    itemList.clear()
//                    itemList.addAll(it)
//                    adapter.updateData(itemList)
//                },
//                {
//                    Log.d(TAG, "Error getting datas.", it)
//                }
//            )
//
//        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}