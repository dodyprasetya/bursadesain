package com.bursadesain.android.ui.packages

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bursadesain.android.R
import com.bursadesain.android.app.GlideApp
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.ui.adapter.SelectColorAdapter
import com.bursadesain.android.util.AppUtil
import com.bursadesain.android.util.RequestUtil
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.yalantis.ucrop.UCrop
import es.dmoral.toasty.Toasty
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_input_package.*
import kotlinx.android.synthetic.main.dialog_select_color.view.*
import okhttp3.RequestBody
import java.io.File
import java.util.concurrent.TimeUnit

class InputPackageActivity : AppCompatActivity(), SelectColorAdapter.Listener {

    private val PICK_IMAGE = 100

    lateinit var dialog: ProgressDialog

    private val compositeDisposable = CompositeDisposable()

    lateinit var colorPicker: BottomSheetDialog

    private var isEditing: Boolean = false
    private var categoryId: String = "0"
    private var packageId: String = "0"
    private var name: String = ""
    private var desc: String = ""
    private var price: Int = 0
    private var color: String = "1"
    private var isUsingIcon: Boolean = false
    private var isIcon: String = "0"
    private var userId: String = "0"

    private var imageUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_input_package)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initProgressDialog()
        initColorPicker()

        intent.extras?.let {
            isEditing = it.getBoolean("is_editing", false)
            categoryId = it.getString("category_id", "0")
            packageId = it.getString("package_id", "0")
            name = it.getString("name", "")
            desc = it.getString("desc", "")
            color = it.getString("color", "1")
            isIcon = it.getString("is_icon", "0")
            isUsingIcon = it.getBoolean("is_using_icon", false)
            price = it.getInt("price", 0)

            if (isEditing) toolbar.title = "Edit Paket"

            initData()
        }

        AppUtil.getProfile(this)?.also {
            // if the user is designer
            if (it.type == 2) userId = it.id
        }

        tvSelectColor.setOnClickListener {
            colorPicker.show()
        }

        tvSelectImage.setOnClickListener {
            selectImage()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_submit, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
            R.id.menu_submit -> {
                submit()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun initProgressDialog() {
        dialog = ProgressDialog(this);
        dialog.setMessage("Please wait..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
    }

    private fun initColorPicker() {
        val dialogView = layoutInflater.inflate(R.layout.dialog_select_color, null)
        colorPicker = BottomSheetDialog(this);
        colorPicker.setContentView(dialogView);
        colorPicker.setCanceledOnTouchOutside(true);

        val adapter = SelectColorAdapter()
        adapter.listener = this
        dialogView.rvColors.adapter = adapter
    }

    private fun initData() {
        etName.setText(name)
        etDesc.setText(desc)
        etPrice.setText(price.toString())

        if (isIcon.equals("0")) {
            isUsingIcon = false

            val col: Int
            if (color.equals("1")) col = R.color.package1
            else if (color.equals("2")) col = R.color.package2
            else if (color.equals("3")) col = R.color.package3
            else if (color.equals("4")) col = R.color.package4
            else if (color.equals("5")) col = R.color.package5
            else if (color.equals("6")) col = R.color.package6
            else if (color.equals("7")) col = R.color.package7
            else if (color.equals("8")) col = R.color.package8
            else if (color.equals("9")) col = R.color.package9
            else col = R.color.package10

            ivColor.setImageResource(col)

        } else {
            isUsingIcon = true

            GlideApp.with(this)
                .load(color)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .centerCrop()
                .into(ivColor)

            ivColor.isDisableCircularTransformation = true
        }
    }

    private fun submit() {
        val name = etName.text.toString();
        val desc = etDesc.text.toString();
        val price = etPrice.text.toString();

        if (
            !TextUtils.isEmpty(name) &&
            !TextUtils.isEmpty(desc) &&
            !TextUtils.isEmpty(price)
        ) {
            submitData()
        } else {
            Toasty.info(this, "Isi semua data", Toasty.LENGTH_LONG).show()
        }
    }

    private fun getRequestIfUsingColor(): HashMap<String, RequestBody> {
        val name = etName.text.toString();
        val desc = etDesc.text.toString();
        val price = etPrice.text.toString();

        val request = if (!isEditing) hashMapOf(
            "category_id" to RequestUtil.getBody(categoryId),
            "name" to RequestUtil.getBody(name),
            "price" to RequestUtil.getBody(price),
            "description" to RequestUtil.getBody(desc),
            "color" to RequestUtil.getBody(color),
            "user_id" to RequestUtil.getBody(userId)
        )
        else hashMapOf(
            "package_id" to RequestUtil.getBody(packageId),
            "name" to RequestUtil.getBody(name),
            "price" to RequestUtil.getBody(price),
            "description" to RequestUtil.getBody(desc),
            "color" to RequestUtil.getBody(color),
            "is_icon" to RequestUtil.getBody(isIcon)
        )

        return request
    }

    private fun getRequestIfUsingImage(): HashMap<String, RequestBody> {
        val name = etName.text.toString();
        val desc = etDesc.text.toString();
        val price = etPrice.text.toString();

        val request = if (!isEditing) hashMapOf(
            "category_id" to RequestUtil.getBody(categoryId),
            "name" to RequestUtil.getBody(name),
            "price" to RequestUtil.getBody(price),
            "description" to RequestUtil.getBody(desc),
            "user_id" to RequestUtil.getBody(userId)
        )
        else hashMapOf(
            "package_id" to RequestUtil.getBody(packageId),
            "name" to RequestUtil.getBody(name),
            "price" to RequestUtil.getBody(price),
            "description" to RequestUtil.getBody(desc)
        )

        return request
    }

    private fun submitData() {
        val request: HashMap<String, RequestBody>
        val observable: Observable<String>

        if (isUsingIcon) {
            request = getRequestIfUsingImage()
            val fileRequestBody = RequestUtil.getFileBody(imageUri!!)

            observable =
                if (!isEditing) ServerManager.getInstance().service
                    .addPackageWithIcon(request, fileRequestBody)
                else ServerManager.getInstance().service
                    .editPackageWithIcon(request, fileRequestBody)
        } else {
            request = getRequestIfUsingColor()

            observable =
                if (!isEditing) ServerManager.getInstance().service.addPackage(request)
                else ServerManager.getInstance().service.editPackage(request)
        }

        val disposable = observable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { dialog.show() }
            .doOnTerminate { dialog.dismiss() }
            .subscribe(
                {
                    if (it.equals("Berhasil")) {
                        if (!isEditing)
                            Toasty.success(this, "Tambah paket berhasil", Toasty.LENGTH_LONG).show()
                        else
                            Toasty.success(this, "Edit paket berhasil", Toasty.LENGTH_LONG).show()

                        setResult(Activity.RESULT_OK)
                        finish()

                    } else {
                        if (!isEditing)
                            Toasty.error(this, "Tambah paket gagal", Toasty.LENGTH_LONG).show()
                        else
                            Toasty.error(this, "Edit paket gagal", Toasty.LENGTH_LONG).show()
                    }
                },
                {
                    if (!isEditing)
                        Toasty.error(this, "Tambah paket gagal", Toasty.LENGTH_LONG).show()
                    else
                        Toasty.error(this, "Edit paket gagal", Toasty.LENGTH_LONG).show()
                }
            )

        compositeDisposable.add(disposable)

    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            if (data == null) {
                Toasty.error(this, "Error picking image", Toasty.LENGTH_LONG).show()
                return;
            }

            imageUri = data.getData()

            val options = UCrop.Options()
            options.setCompressionFormat(Bitmap.CompressFormat.PNG)
            options.setShowCropGrid(true)

            UCrop.of(imageUri!!, Uri.fromFile(File(cacheDir, "package_icon.png")))
                .withOptions(options)
                .withMaxResultSize(512, 512)
                .withAspectRatio(1f, 1f)
                .start(this)
        }
        else if (requestCode == UCrop.REQUEST_CROP) {
            if (data == null) {
                Toasty.error(this, "Error cropping image", Toasty.LENGTH_LONG).show()
                return;
            }

            if (resultCode == RESULT_OK) {
                imageUri = UCrop.getOutput(data);
                ivColor.setImageURI(imageUri);
                ivColor.isDisableCircularTransformation = true
                isUsingIcon = true
                isIcon = "1"

            } else if (resultCode == UCrop.RESULT_ERROR) {
                Toasty.error(this, "Error cropping image", Toasty.LENGTH_LONG).show()
                Log.e("Error", "Crop error:" + UCrop.getError(data)!!.message);
            }
        }
    }

    override fun onColorSelected(item: Int, idx: Int) {
        color = idx.toString()
        ivColor.setImageResource(item)
        ivColor.isDisableCircularTransformation = false
        colorPicker.dismiss()
        isUsingIcon = false
        isIcon = "0"
    }

    fun selectImage() {
        val intent = Intent()
        intent.type = "image/png"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Pilih Icon (PNG)"), PICK_IMAGE)
    }
}