package com.bursadesain.android.ui.chat

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bursadesain.android.R
import kotlinx.android.synthetic.main.fragment_play_audio.*
import nl.changer.audiowife.AudioWife

class PlayAudioFragment : Fragment() {

    lateinit var audioWife: AudioWife

    private var path: String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_play_audio, container, false)

        audioWife = AudioWife.getInstance()

        arguments?.let {
            path = it.getString("path", "")

            audioWife.init(context, Uri.parse(path))
                .setPlayView(view.findViewById(R.id.btPlay))
                .setPauseView(view.findViewById(R.id.btPause))
                .setSeekBar(view.findViewById(R.id.seekBar))
                .setRuntimeView(view.findViewById(R.id.tvProgressDuration))
                .setTotalTimeView(view.findViewById(R.id.tvDuration))

            audioWife.play()
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ivClose.setOnClickListener {
            activity?.onBackPressed()
        }
    }
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_play_audio)
//
//        audioWife = AudioWife.getInstance()
//
//        intent.extras?.let {
//            path = it.getString("path", "")
//
//            audioWife.init(this, Uri.parse(path))
//                .setPlayView(findViewById(R.id.btPlay))
//                .setPauseView(findViewById(R.id.btPause))
//                .setSeekBar(findViewById(R.id.seekBar))
//                .setRuntimeView(findViewById(R.id.tvProgressDuration))
//                .setTotalTimeView(findViewById(R.id.tvDuration))
//
//            audioWife.play()
//        }
//
//        ivClose.setOnClickListener {
//            onBackPressed()
//        }
//    }

    override fun onDestroy() {
        super.onDestroy()
        audioWife.pause()
        audioWife.release()
    }
}
