package com.bursadesain.android.ui.packages

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.bursadesain.android.R
import com.bursadesain.android.model.Package
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.ui.adapter.PackageAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_packages.*
import kotlinx.android.synthetic.main.fragment_home.rvPackages
import java.util.concurrent.TimeUnit

class PackagesActivity : AppCompatActivity(), PackageAdapter.Listener {

    val TAG = PackagesActivity::class.java.name

    private val compositeDisposable = CompositeDisposable()

    val packageList = ArrayList<Package>()
    lateinit var adapter: PackageAdapter

    lateinit var categoryId: String
    lateinit var packageTitle: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_packages)

        categoryId = intent.getStringExtra("category_id") ?: ""
        packageTitle = intent.getStringExtra("package_name") ?: ""

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.title = packageTitle

        adapter = PackageAdapter()
        adapter.listener = this
        rvPackages.adapter = adapter

        refreshLayout.setOnRefreshListener {
            loadData()
        }

        loadData()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun loadData() {
        val disposable = ServerManager.getInstance()
            .service.loadPackages(categoryId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { refreshLayout.isRefreshing = true }
            .doOnTerminate { refreshLayout.isRefreshing = false }
            .subscribe(
                {
                    packageList.clear()
                    packageList.addAll(it)
                    adapter.updateData(packageList)
                },
                {
                    Log.d(TAG, "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onItemSelected(item: Package) {
        val intent = Intent(this, PackageDetailActivity::class.java)
        intent.putExtra("package_id", item.id)
        intent.putExtra("package_name", item.name)
        intent.putExtra("package_desc", item.description)
        intent.putExtra("package_price", item.price)
        startActivity(intent)
    }
}