package com.bursadesain.android.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bursadesain.android.R
import kotlinx.android.synthetic.main.item_select_color.view.*

class SelectColorAdapter : RecyclerView.Adapter<SelectColorAdapter.ViewHolder>(){

    private var context: Context? = null
    var listener: Listener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_select_color, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 10
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind() {

            val colorIdx = adapterPosition + 1
            val color: Int

            if (colorIdx == 1) color = R.color.package1
            else if (colorIdx == 2) color = R.color.package2
            else if (colorIdx == 3) color = R.color.package3
            else if (colorIdx == 4) color = R.color.package4
            else if (colorIdx == 5) color = R.color.package5
            else if (colorIdx == 6) color = R.color.package6
            else if (colorIdx == 7) color = R.color.package7
            else if (colorIdx == 8) color = R.color.package8
            else if (colorIdx == 9) color = R.color.package9
            else color = R.color.package10

            itemView.ivColor.setImageResource(color)

            itemView.ivColor.setOnClickListener {
                listener?.onColorSelected(color, colorIdx)
            }
        }
    }

    interface Listener {
        fun onColorSelected(item: Int, idx: Int)
    }
}