package com.bursadesain.android.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bursadesain.android.R
import com.bursadesain.android.app.GlideApp
import com.bursadesain.android.model.OrderFile
import com.bursadesain.android.util.AppUtil
import kotlinx.android.synthetic.main.item_upload_file.view.*

class OrderFileAdapter : RecyclerView.Adapter<OrderFileAdapter.ViewHolder>(){

    private lateinit var itemList: List<OrderFile>
    lateinit var context: Context
    var listener: Listener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_upload_file, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if (::itemList.isInitialized) itemList.size else 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    fun updateData(list: ArrayList<OrderFile>) {
        itemList = list;
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind() {
            val item = itemList.get(adapterPosition)

            val filePath = item.file

            val urlParts = filePath.split("/")
            val fileName = urlParts.last()

            itemView.tvFileName.text = fileName

            if (filePath.toLowerCase().contains(".jpg")
                || filePath.toLowerCase().contains(".jpeg")
                || filePath.toLowerCase().contains(".png")
            ) {
                GlideApp.with(context)
                    .load(filePath)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .into(itemView.ivFile)
            } else {
                AppUtil.setDocumentIcon(context, itemView.ivFile, fileName)
            }

            itemView.ivDelete.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_download))

            itemView.ivDelete.setOnClickListener {
                listener?.onItemDownload(item)
            }
        }
    }

    interface Listener {
        fun onItemDownload(item: OrderFile)
    }
}