package com.bursadesain.android.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bursadesain.android.R
import com.bursadesain.android.app.GlideApp
import com.bursadesain.android.model.Portfolio
import kotlinx.android.synthetic.main.item_portfolio.view.*
import kotlinx.android.synthetic.main.item_portfolio_add.view.*

class PortfolioAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    private val TYPE_ADD = 1
    private val TYPE_ITEM = 2

    private lateinit var itemList: ArrayList<Portfolio>
    lateinit var context: Context
    var listener: Listener? = null

    var isOwner: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context

        when(viewType) {
            TYPE_ITEM -> {
                val view =
                    LayoutInflater.from(context).inflate(R.layout.item_portfolio, parent, false)
                return PortfolioViewHolder(view)
            }
            else -> {
                val view =
                    LayoutInflater.from(context).inflate(R.layout.item_portfolio_add, parent, false)
                return AddViewHolder(view)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (isOwner) {
            when (position) {
                0 -> return TYPE_ADD
                else -> return TYPE_ITEM
            }
        } else {
            return TYPE_ITEM
        }
    }

    override fun getItemCount(): Int {
        if (isOwner) {
            return if (::itemList.isInitialized) itemList.size else 1
        }
        return if (::itemList.isInitialized) itemList.size else 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (isOwner) {
            when (position) {
                0 -> (holder as AddViewHolder).bind()
                else -> (holder as PortfolioViewHolder).bind()
            }
        } else {
            (holder as PortfolioViewHolder).bind()
        }
    }

    fun updateData(list: ArrayList<Portfolio>) {
        itemList = list;
        if (isOwner) itemList.add(0, Portfolio())

        notifyDataSetChanged()
    }

    inner class PortfolioViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind() {
            val item = itemList.get(adapterPosition)

            GlideApp.with(context)
                .load(item.file)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(itemView.ivPortfolio)

            if (isOwner) {
                itemView.llDelete.visibility = View.VISIBLE

                itemView.llDelete.setOnClickListener {
                    listener?.onDeleteItem(item)
                }
            } else {
                itemView.llDelete.visibility = View.GONE
            }

            itemView.ivPortfolio.setOnClickListener {
                listener?.onItemSelected(if (isOwner) adapterPosition - 1 else adapterPosition)
            }
        }
    }

    inner class AddViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind() {
            itemView.llAdd.setOnClickListener {
                listener?.onAddItem()
            }
        }
    }

    interface Listener {
        fun onAddItem()
        fun onItemSelected(position: Int)
        fun onDeleteItem(item: Portfolio)
    }
}