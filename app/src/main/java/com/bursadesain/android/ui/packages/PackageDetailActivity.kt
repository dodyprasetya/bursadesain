package com.bursadesain.android.ui.packages

import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bursadesain.android.R
import com.bursadesain.android.model.Portfolio
import com.bursadesain.android.model.Profile
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.ui.adapter.PortfolioAdapter
import com.bursadesain.android.ui.login.LoginActivity
import com.bursadesain.android.ui.order.OrderActivity
import com.bursadesain.android.ui.portfolio.PortfolioSliderFragment
import com.bursadesain.android.util.AppUtil
import com.bursadesain.android.util.RequestUtil
import com.yalantis.ucrop.UCrop
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_package_detail.*
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import java.io.File
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class PackageDetailActivity : AppCompatActivity(), PortfolioAdapter.Listener {

    val TAG = PackageDetailActivity::class.java.name

    private val compositeDisposable = CompositeDisposable()

    val portfolioList = ArrayList<Portfolio>()
    lateinit var adapter: PortfolioAdapter

    lateinit var packageId: String
    lateinit var packageTitle: String
    lateinit var packageDesc: String
    var packagePrice: Int = 0

    lateinit var dialog: ProgressDialog
    lateinit var deleteDialog: AlertDialog
    private  var selectedPortfolioId: String = "0"

    lateinit var profile: Profile

    private var isLogin: Boolean = false
    private var isOwner: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_package_detail)

        initProgressDialog()
        initDeleteDialog()

        packageId = intent.getStringExtra("package_id") ?: ""
        packageTitle = intent.getStringExtra("package_name") ?: ""
        packageDesc = intent.getStringExtra("package_desc") ?: ""
        packagePrice = intent.getIntExtra("package_price", 0)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        tvTitle.text = packageTitle
        tvDesc.text = packageDesc
        tvPrice.text = "Rp. ${AppUtil.convertToMoney(packagePrice)}"

        AppUtil.isLogin(this).also {
            if (it) profile = AppUtil.getProfile(this)!!
            isLogin = it
        }

        adapter = PortfolioAdapter()
        adapter.listener = this
        rvPortofolios.adapter = adapter

        refreshLayout.setOnRefreshListener {
            loadData()
        }

        loadData()

        if (isLogin) {
            if (profile.type == 1) {
                ivLike.visibility = View.GONE
                btOrder.visibility = View.VISIBLE
            } else {
                ivLike.visibility = View.GONE
                btOrder.visibility = View.GONE
            }
        } else {
            ivLike.visibility = View.GONE
            btOrder.visibility = View.VISIBLE
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun initProgressDialog() {
        dialog = ProgressDialog(this);
        dialog.setMessage("Please wait..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
    }

    private fun initDeleteDialog() {
        deleteDialog = AlertDialog.Builder(this)
            .setTitle("Hapus Data")
            .setMessage("Apakah Anda yakin ingin menghapus portfolio ini?")
            .setCancelable(true)
            .setPositiveButton("Ya", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, position: Int) {
                    deleteDialog.dismiss()
                    deleteData()
                }
            })
            .setNegativeButton("Tidak", null)
            .create()
    }

    private fun loadData() {
        val disposable = ServerManager.getInstance()
            .service.loadPackageDetail(packageId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { refreshLayout.isRefreshing = true }
            .doOnTerminate { refreshLayout.isRefreshing = false }
            .subscribe(
                {
                    portfolioList.clear()
                    portfolioList.addAll(it.portfolios)

                    if (isLogin) {
                        isOwner = (profile.type == 0) || (profile.type == 2 && it.user_id == profile.id)
                        adapter.isOwner = isOwner
                    }

                    adapter.updateData(portfolioList)

                    tvTitle.text = it.name
                    tvDesc.text = it.description
                    tvPrice.text = "Rp. ${AppUtil.convertToMoney(it.price)}"

                },
                {
                    Log.d(TAG, "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    fun orderNow(view: View) {
        if (isLogin) {
            val intent = Intent(this, OrderActivity::class.java)
            intent.putExtra("package_id", packageId)
            intent.putExtra("package_name", packageTitle)
            intent.putExtra("package_desc", packageDesc)
            intent.putExtra("package_price", packagePrice)
            startActivity(intent)
        } else {
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }

    override fun onAddItem() {
        EasyImage.openGallery(this, 0)
    }

    override fun onItemSelected(position: Int) {
        val imageList = ArrayList<String>()
        for (item in portfolioList) {
            imageList.add(item.file)
        }

        if (isLogin) {
            // if owner

            if (isOwner) {
                imageList.removeAt(0)
            }
        }

        val args = Bundle()
        args.putStringArrayList("images", imageList)
        args.putInt("position", position)
        args.putInt("total_images", imageList.size)

        val ft = getSupportFragmentManager().beginTransaction()
        val fragment = PortfolioSliderFragment()
        fragment.setArguments(args)
        fragment.show(ft, "slideshow")
    }

    override fun onDeleteItem(item: Portfolio) {
        selectedPortfolioId = item.id
        deleteDialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        EasyImage.handleActivityResult(requestCode, resultCode, data, this,
            object : DefaultCallback() {

                override fun onImagesPicked(
                    imageFiles: MutableList<File>, source: EasyImage.ImageSource?, type: Int
                ) {
                    if (imageFiles.size > 0) {

                        val options = UCrop.Options()
                        options.setCompressionFormat(Bitmap.CompressFormat.JPEG)
                        options.setCompressionQuality(80)
                        options.setShowCropGrid(true)
                        options.setFreeStyleCropEnabled(true)

                        UCrop.of(Uri.fromFile(imageFiles[0]), Uri.fromFile(File(cacheDir, "porfolio_image.jpg")))
                            .withOptions(options)
                            .withMaxResultSize(1024, 1024)
                            .start(this@PackageDetailActivity)
                    }
                }

                override fun onCanceled(source: EasyImage.ImageSource?, type: Int) {
                    super.onCanceled(source, type)

                    if (source == EasyImage.ImageSource.CAMERA) {
                        val photoFile = EasyImage.lastlyTakenButCanceledPhoto(this@PackageDetailActivity)
                        photoFile?.delete()
                    }
                }

                override fun onImagePickerError(
                    e: Exception?, source: EasyImage.ImageSource?, type: Int
                ) {
                    super.onImagePickerError(e, source, type)

                    Toasty.error(this@PackageDetailActivity, "Error dalam mengambil gambar",
                        Toasty.LENGTH_LONG).show()
                    e!!.printStackTrace()
                }
            })

        if (requestCode == UCrop.REQUEST_CROP) {
            if (data == null) {
                Toasty.error(this, "Error cropping image", Toasty.LENGTH_LONG).show()
                return;
            }

            if (resultCode == RESULT_OK) {
                val imageUri = UCrop.getOutput(data);
                imageUri?.let {
                    addPortfolio(it)
                }

            } else if (resultCode == UCrop.RESULT_ERROR) {
                Toasty.error(this, "Error cropping image", Toasty.LENGTH_LONG).show()
                Log.e("Error", "Crop error:" + UCrop.getError(data)!!.message);
            }
        }
    }

    private fun addPortfolio(imageUri: Uri) {
        val user = hashMapOf(
            "package_id" to RequestUtil.getBody(packageId)
        )

        val fileRequestBody = RequestUtil.getFileBody(imageUri)

        val disposable = ServerManager.getInstance()
            .service.addPortfolio(user, fileRequestBody)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { dialog.show() }
            .doOnTerminate { dialog.dismiss() }
            .subscribe(
                {
                    if (it.equals("Gagal Upload") || it.equals("No Image")) {
                        Toasty.error(this, "Upload gambar gagal", Toasty.LENGTH_LONG).show()
                    } else if (it.equals("Gagal")) {
                        Toasty.error(this, "Tambah portfolio gagal", Toasty.LENGTH_LONG).show()
                    } else {
                        Toasty.success(this, "Tambah portfolio berhasil", Toasty.LENGTH_LONG).show()
                        loadData()
                    }
                },
                {
                    Toasty.error(this, "Tambah portfolio gagal", Toasty.LENGTH_LONG).show()
                }
            )

        compositeDisposable.add(disposable)
    }

    private fun deleteData() {
        val disposable = ServerManager.getInstance()
            .service.deletePortfolio(selectedPortfolioId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { dialog.show() }
            .doOnTerminate { dialog.dismiss() }
            .subscribe(
                {
                    if (it.equals("Berhasil")) {
                        Toasty.success(this, "Hapus portfolio berhasil", Toasty.LENGTH_LONG).show()
                        loadData()
                    } else {
                        Toasty.error(this, "Hapus portfolio gagal", Toasty.LENGTH_LONG).show()
                    }
                },
                {
                    Toasty.error(this, "Hapus portfolio gagal", Toasty.LENGTH_LONG).show()
                }
            )

        compositeDisposable.add(disposable)
    }
}