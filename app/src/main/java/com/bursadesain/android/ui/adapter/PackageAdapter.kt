package com.bursadesain.android.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bursadesain.android.R
import com.bursadesain.android.app.GlideApp
import com.bursadesain.android.model.Package
import com.bursadesain.android.util.AppUtil
import kotlinx.android.synthetic.main.item_package1.view.*

class PackageAdapter : RecyclerView.Adapter<PackageAdapter.ViewHolder>(){

    private lateinit var itemList: List<Package>
    private var context: Context? = null
    var listener: Listener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_package1, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if (::itemList.isInitialized) itemList.size else 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    fun updateData(list: ArrayList<Package>) {
        itemList = list;
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind() {
            val item = itemList.get(adapterPosition)

            val price = AppUtil.convertToMoney(item.price)
            itemView.tvTitle.text = item.name
            itemView.tvPrice.text = "Rp. ${price}"
            itemView.tvDesc.text = item.description

            itemView.container.setOnClickListener {
                listener?.onItemSelected(item)
            }
            itemView.btOrder.setOnClickListener {
                listener?.onItemSelected(item)
            }

            if (item.is_icon.equals("0")) {
                val color: Int
                if (item.color.equals("1")) color = R.color.package1
                else if (item.color.equals("2")) color = R.color.package2
                else if (item.color.equals("3")) color = R.color.package3
                else if (item.color.equals("4")) color = R.color.package4
                else if (item.color.equals("5")) color = R.color.package5
                else if (item.color.equals("6")) color = R.color.package6
                else if (item.color.equals("7")) color = R.color.package7
                else if (item.color.equals("8")) color = R.color.package8
                else if (item.color.equals("9")) color = R.color.package9
                else color = R.color.package10

                itemView.llColor.setBackgroundColor(ContextCompat.getColor(context!!, color))
                itemView.btOrder.setBackgroundColor(ContextCompat.getColor(context!!, color))

                itemView.ivColor.setImageResource(color)
                itemView.ivColor.isDisableCircularTransformation = false

            } else {
                GlideApp.with(context!!)
                    .load(item.color)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .into(itemView.ivColor)

                itemView.ivColor.isDisableCircularTransformation = true

                itemView.btOrder.setBackgroundColor(ContextCompat.getColor(context!!, R.color.package3))
            }

        }
    }

    interface Listener {
        fun onItemSelected(item: Package)
    }
}