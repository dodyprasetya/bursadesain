package com.bursadesain.android.ui.packages

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bursadesain.android.R
import com.bursadesain.android.model.PackageCategory
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.ui.adapter.ManageCategoryAdapter
import com.google.android.material.bottomsheet.BottomSheetDialog
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_packages.*
import kotlinx.android.synthetic.main.dialog_package_action.view.*
import kotlinx.android.synthetic.main.fragment_home.rvPackages
import java.util.concurrent.TimeUnit

class ManageCategoryActivity : AppCompatActivity(), ManageCategoryAdapter.Listener {

    val TAG = ManageCategoryActivity::class.java.name

    private val ADD_EDIT_CATEGORY_REQ_CODE = 1000

    private val compositeDisposable = CompositeDisposable()

    lateinit var dialogAction: BottomSheetDialog
    lateinit var deleteDialog: AlertDialog

    private var designerId: String = "0"

    val packageList = ArrayList<PackageCategory>()
    lateinit var adapter: ManageCategoryAdapter

    private var selectedItem: PackageCategory? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_category)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initActionDialog()
        initDeleteDialog()

        intent.extras?.let {
            designerId = it.getString("designer_id", "0")
        }

        val isAdmin = designerId.equals("0")
        adapter = ManageCategoryAdapter(isAdmin)
        adapter.listener = this
        rvPackages.adapter = adapter

        refreshLayout.setOnRefreshListener {
            loadData()
        }

        loadData()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_add, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
            R.id.menu_add -> {
                addData()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun initActionDialog() {
        val dialogView = layoutInflater.inflate(R.layout.dialog_package_action, null)
        dialogAction = BottomSheetDialog(this);
        dialogAction.setContentView(dialogView);
        dialogAction.setCanceledOnTouchOutside(true);

        dialogView.llEdit.setOnClickListener {
            dialogAction.dismiss()
            editData()
        }

        dialogView.llDelete.setOnClickListener {
            dialogAction.dismiss()
            deleteDialog.show()
        }
    }

    private fun initDeleteDialog() {
        deleteDialog = AlertDialog.Builder(this)
            .setTitle("Hapus Data")
            .setMessage("Apakah Anda yakin ingin menghapus data ini?")
            .setCancelable(true)
            .setPositiveButton("Ya", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, position: Int) {
                    deleteDialog.dismiss()
                    deleteData()
                }
            })
            .setNegativeButton("Tidak", null)
            .create()
    }

    private fun loadData() {
        val observable =
            if (designerId.equals("0")) ServerManager.getInstance().service.loadPackageCategories()
            else ServerManager.getInstance().service.loadPackageCategories(designerId)

        val disposable = observable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { refreshLayout.isRefreshing = true }
            .doOnTerminate { refreshLayout.isRefreshing = false }
            .subscribe(
                {
                    packageList.clear()
                    packageList.addAll(it)
                    adapter.updateData(packageList)
                },
                {
                    Log.d(TAG, "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    private fun addData() {
        Intent(this, InputCategoryActivity::class.java).also {
            it.putExtra("designer_id", designerId)
            startActivityForResult(it, ADD_EDIT_CATEGORY_REQ_CODE)
        }
    }

    private fun editData() {
        val intent = Intent(this, InputCategoryActivity::class.java)
        intent.putExtra("is_editing", true)
        intent.putExtra("category_id", selectedItem?.id)
        intent.putExtra("name", selectedItem?.name)
        intent.putExtra("price", selectedItem?.price)
        intent.putExtra("designer_id", designerId)

        startActivityForResult(intent, ADD_EDIT_CATEGORY_REQ_CODE)
    }

    private fun deleteData() {

    }

    override fun onItemSelected(item: PackageCategory) {
        val intent = Intent(this, ManagePackageActivity::class.java)
        intent.putExtra("category_id", item.id)
        intent.putExtra("package_name", item.name)
        intent.putExtra("designer_id", designerId)
        startActivity(intent)
    }

    override fun onItemLongClicked(item: PackageCategory) {
        selectedItem = item
        dialogAction.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == ADD_EDIT_CATEGORY_REQ_CODE && resultCode == Activity.RESULT_OK) {
            loadData()
        }
    }
}