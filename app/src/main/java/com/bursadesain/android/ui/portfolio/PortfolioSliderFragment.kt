package com.bursadesain.android.ui.portfolio

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bursadesain.android.R
import com.bursadesain.android.app.GlideApp
import com.bursadesain.android.helper.TouchImageView
import kotlinx.android.synthetic.main.fragment_portfolio_slider.*

class PortfolioSliderFragment : DialogFragment() {

    val TAG = PortfolioSliderFragment::class.java.name

    private val imageList = ArrayList<String>()
    private var selectedPosition = 0
    private var totalImages = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_portfolio_slider, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            imageList.addAll(it.getSerializable("images") as ArrayList<String>)
            selectedPosition = it.getInt("position", 0)
            totalImages = it.getInt("total_images", 0)
        }

        viewPager.setAdapter(PortfolioPagerAdapter())
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener)
        viewPager.setCurrentItem(selectedPosition)

        lblCount.text = "${selectedPosition + 1}/${totalImages}"
    }

    val viewPagerPageChangeListener = object : ViewPager.OnPageChangeListener {

        override fun onPageScrollStateChanged(state: Int) {
        }

        override fun onPageScrolled(
            position: Int, positionOffset: Float, positionOffsetPixels: Int
        ) {
        }

        override fun onPageSelected(position: Int) {
            lblCount.text = "${position + 1}/${totalImages}"
        }
    }

    inner class PortfolioPagerAdapter : PagerAdapter() {

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val layoutInflater = activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val view = layoutInflater.inflate(R.layout.image_fullscreen_preview, container, false)

            val ivImagePreview = view.findViewById<TouchImageView>(R.id.ivImagePreview)

            val image = imageList.get(position)

            GlideApp.with(context!!)
                .load(image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .fitCenter()
                .into(ivImagePreview)

            container.addView(view)

            return view
        }

        override fun getCount(): Int {
            return imageList.size
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view == `object` as View
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as View)
            super.destroyItem(container, position, `object`)
        }
    }
}