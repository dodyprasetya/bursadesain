package com.bursadesain.android.ui.chat

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bursadesain.android.R
import com.bursadesain.android.model.ChatRoom
import com.bursadesain.android.model.Profile
import com.bursadesain.android.ui.adapter.ChatRoomAdapter
import com.bursadesain.android.util.AppUtil
import com.google.firebase.database.*
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_chat.*
import java.util.*
import kotlin.collections.ArrayList

class ChatFragment : Fragment(), ChatRoomAdapter.Listener {

    val TAG = ChatFragment::class.java.name

    val db = FirebaseDatabase.getInstance()

    lateinit var chatRoomQuery: Query
    lateinit var chatRoomListener: ValueEventListener

    val itemList = ArrayList<ChatRoom>()
    lateinit var adapter: ChatRoomAdapter

    lateinit var profile: Profile

    private var userType: String = "user"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_chat, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            userType = it.getString("user_type", "user")
        }

        adapter = ChatRoomAdapter()
        adapter.listener = this

        rvChats.adapter = adapter

        refreshLayout.setOnRefreshListener {
            refreshLayout.isRefreshing = false
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        profile = AppUtil.getProfile(context)!!
    }

    override fun onStart() {
        super.onStart()
        loadData()
    }

    override fun onStop() {
        super.onStop()
        chatRoomQuery.removeEventListener(chatRoomListener)
    }

    private fun loadData() {
        refreshLayout.isRefreshing = true

        chatRoomQuery = db.reference.child("chat_rooms").orderByChild("timestamp")

        chatRoomListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                itemList.clear()

                for (ds in dataSnapshot.children) {

                    val item = ds.getValue(ChatRoom::class.java)
                    item?.id = ds.key ?: ""

                    item?.let {
                        itemList.add(it)
                    }
                }

                // filter to chat room that contain the user
                val chatRoomList = ArrayList<ChatRoom>()
                for (i in itemList.indices) {
                    val item = itemList.get(i)

                    if (userType.equals("user") && item.member_id.equals(profile.id)) {
                        chatRoomList.add(item)
                    } else if (userType.equals("designer") &&
                        (item.designer_id.equals(profile.id) || item.designer_id.equals("0"))) {
                        chatRoomList.add(item)
                    }
                }

                itemList.clear()
                itemList.addAll(chatRoomList)

                Collections.reverse(itemList)
                adapter.updateData(itemList)

                refreshLayout.isRefreshing = false
            }

            override fun onCancelled(databaseError: DatabaseError) {
                refreshLayout.isRefreshing = false
                Log.e(TAG, databaseError.message)
            }
        }

        chatRoomQuery.addValueEventListener(chatRoomListener)
    }

    override fun onItemSelected(item: ChatRoom) {
        val intent = Intent(context, ChatMessageActivity::class.java)
        intent.putExtra("room_id", item.id)
        intent.putExtra("room_name", item.name)
        intent.putExtra("room_pic", item.pic)
        startActivity(intent)
    }
}