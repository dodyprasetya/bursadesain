package com.bursadesain.android.ui.profile

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bursadesain.android.R
import com.bursadesain.android.app.GlideApp
import kotlinx.android.synthetic.main.activity_view_profile.*

class ViewProfileActivity : AppCompatActivity() {

    val TAG = ViewProfileActivity::class.java.name

    private var id: String = "0"
    private var fullname: String = ""
    private var email: String = ""
    private var phone: String = ""
    private var address: String = ""
    private var profilePic: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_profile)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        intent.extras?.let {
            id = it.getString("id", "0")
            fullname = it.getString("fullname", "")
            email = it.getString("email", "")
            phone = it.getString("phone", "")
            address = it.getString("address", "")
            profilePic = it.getString("profile_pic", "")

            initData()
        }

        llCall.setOnClickListener {
            call()
        }

        llSendEmail.setOnClickListener {
            sendEmail()
        }

        llOpenWhatsapp.setOnClickListener {
            openWhatsapp()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun initData() {
        etFullname.setText(fullname)
        etEmail.setText(email)
        etAddress.setText(address)
        etPhone.setText(phone)

        GlideApp.with(this)
            .load(profilePic)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .circleCrop()
            .into(ivProfile)
    }

    fun call() {
        val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
        startActivity(intent)
    }

    fun sendEmail() {
        val intent = Intent(
            Intent.ACTION_SENDTO,
            Uri.fromParts("mailto", email, null))

        intent.putExtra(Intent.EXTRA_SUBJECT, "")
        intent.putExtra(Intent.EXTRA_TEXT, "")
        startActivity(Intent.createChooser(intent, "Kirim Email..."))
    }

    fun openWhatsapp() {
        val url = "https://api.whatsapp.com/send?phone=${phone}"
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }
}