package com.bursadesain.android.ui.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bursadesain.android.R
import com.bursadesain.android.ui.login.LoginActivity
import kotlinx.android.synthetic.main.fragment_unauthorize.*

class UnauthorizeFragment : Fragment() {

    val TAG = UnauthorizeFragment::class.java.name

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_unauthorize, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btLogin.setOnClickListener {
            startActivity(Intent(context, LoginActivity::class.java))
        }
    }
}