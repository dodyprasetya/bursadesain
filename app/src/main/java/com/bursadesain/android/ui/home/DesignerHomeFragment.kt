package com.bursadesain.android.ui.home

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.bursadesain.android.R
import com.bursadesain.android.model.Profile
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.ui.designer.DesignerOrderListFragment
import com.bursadesain.android.ui.notification.NotificationActivity
import com.bursadesain.android.ui.packages.ManageCategoryActivity
import com.bursadesain.android.util.AppUtil
import com.google.firebase.firestore.FirebaseFirestore
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_designer_home.*
import java.util.concurrent.TimeUnit

class DesignerHomeFragment : Fragment() {

    val TAG = DesignerHomeFragment::class.java.name

    val db = FirebaseFirestore.getInstance()

    private val compositeDisposable = CompositeDisposable()

    lateinit var profile: Profile

    private var selectedFragment = "designer_active"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_designer_home, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.inflateMenu(R.menu.menu_notification)
        toolbar.menu.findItem(R.id.menu_notification).setOnMenuItemClickListener {
            startActivity(Intent(context, NotificationActivity::class.java))
            true
        }

        if (profile.can_post_package.equals("1")) {
            vPackage.visibility = View.VISIBLE
            llPackage.visibility = View.VISIBLE
        } else {
            vPackage.visibility = View.GONE
            llPackage.visibility = View.GONE
        }

        tvActiveOrder.setOnClickListener {
            selectedFragment = "designer_active"
            loadFragment(selectedFragment)
            tvActiveOrder.setTextColor(ContextCompat.getColor(activity!!, R.color.colorPrimary))
            tvCompleted.setTextColor(ContextCompat.getColor(activity!!, R.color.black))
            tvCancelled.setTextColor(ContextCompat.getColor(activity!!, R.color.black))
        }

        tvCompleted.setOnClickListener {
            selectedFragment = "designer_completed"
            loadFragment(selectedFragment)
            tvActiveOrder.setTextColor(ContextCompat.getColor(activity!!, R.color.black))
            tvCompleted.setTextColor(ContextCompat.getColor(activity!!, R.color.colorPrimary))
            tvCancelled.setTextColor(ContextCompat.getColor(activity!!, R.color.black))
        }

        tvCancelled.setOnClickListener {
            selectedFragment = "designer_cancelled"
            loadFragment(selectedFragment)
            tvActiveOrder.setTextColor(ContextCompat.getColor(activity!!, R.color.black))
            tvCompleted.setTextColor(ContextCompat.getColor(activity!!, R.color.black))
            tvCancelled.setTextColor(ContextCompat.getColor(activity!!, R.color.colorPrimary))
        }

        llPackage.setOnClickListener {
            AppUtil.getProfile(context!!)?.also { profile ->
                Intent(context!!, ManageCategoryActivity::class.java).also {
                    it.putExtra("designer_id", profile.id)
                    startActivity(it)
                }
            }
        }

        loadFragment(selectedFragment)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        profile = AppUtil.getProfile(context)!!
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    private fun loadData() {
        val disposable = ServerManager.getInstance()
            .service.loadSummaryOrderDesigner(profile.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .subscribe(
                {
                    tvSummaryOnProgress.text = "${it.on_progress}"
                    tvSummaryCompleted.text = "${it.completed}"
                },
                {
                    Log.d(TAG, "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    private fun loadFragment(status: String) {
        val fragment = DesignerOrderListFragment()
        val args = Bundle()
        args.putString("status", status)
        fragment.arguments = args

        val transaction = childFragmentManager.beginTransaction()
        transaction.replace(R.id.frameLayout, fragment)
        transaction.commit()
    }
}