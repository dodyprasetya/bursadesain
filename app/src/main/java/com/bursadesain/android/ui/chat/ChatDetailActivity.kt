package com.bursadesain.android.ui.chat

import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.text.InputType
import android.text.TextUtils
import android.util.Log
import android.util.TypedValue
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.marginLeft
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bursadesain.android.R
import com.bursadesain.android.app.GlideApp
import com.bursadesain.android.model.Profile
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.ui.adapter.DesignerAdapter
import com.bursadesain.android.ui.profile.ViewProfileActivity
import com.bursadesain.android.util.AppUtil
import com.bursadesain.android.util.PermissionUtil
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.yalantis.ucrop.UCrop
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_chat_detail.*
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import java.io.File
import java.util.concurrent.TimeUnit

class ChatDetailActivity : AppCompatActivity(), DesignerAdapter.Listener {

    val TAG = ChatDetailActivity::class.java.name

    lateinit var dialog: ProgressDialog
    lateinit var nameDialog: AlertDialog

    private val compositeDisposable = CompositeDisposable()

    val db = FirebaseDatabase.getInstance()
    val storage = FirebaseStorage.getInstance()

    lateinit var profile: Profile

    private var roomId: String = "0"
    private var roomName: String = ""
    private var roomPic: String = ""

    lateinit var etRoomName: EditText

    val memberList = ArrayList<Profile>()
    lateinit var adapter: DesignerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_detail)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initProgressDialog()
        initNameDialog()

        intent.extras?.let {
            roomId = it.getString("room_id", "0")
            roomName = it.getString("room_name", "")
            roomPic = it.getString("room_pic", "")

            initData()
        }

        profile = AppUtil.getProfile(this)!!

        if (profile.type == 0) {
            llUpdate.visibility = View.VISIBLE
        } else {
            llUpdate.visibility = View.GONE
        }

        adapter = DesignerAdapter()
        adapter.listener = this
        rvMembers.adapter = adapter

        loadChatRoomMember()

        tvChangePic.setOnClickListener {
            EasyImage.openGallery(this, 0)
        }

        tvChangeName.setOnClickListener {
            etRoomName.setText(roomName)
            nameDialog.show()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onStart() {
        super.onStart()
        PermissionUtil.askStoragePermissions(this)
    }

    private fun initProgressDialog() {
        dialog = ProgressDialog(this);
        dialog.setMessage("Please wait..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
    }

    private fun initNameDialog() {
        val view = layoutInflater.inflate(R.layout.dialog_update_name, null)
        etRoomName = view.findViewById(R.id.etName)

        nameDialog = AlertDialog.Builder(this)
            .setTitle("Hapus Data")
            .setMessage("Apakah Anda yakin ingin menghapus data ini?")
            .setView(view)
            .setCancelable(true)
            .setPositiveButton("Update", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, position: Int) {
                    nameDialog.dismiss()
                    updateChatRoomName()
                }
            })
            .setNegativeButton("Batal", null)
            .create()
    }

    private fun initData() {
        tvName.text = roomName

        if (!TextUtils.isEmpty(roomPic)) {
            GlideApp.with(this)
                .load(roomPic)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .circleCrop()
                .into(ivPicture)
        } else {
            ivPicture.setImageResource(R.color.package1)
        }
    }

    private fun loadChatRoomMember() {
        val disposable = ServerManager.getInstance()
            .service.loadChatRoomMembers(roomId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .subscribe(
                {
                    memberList.clear()
                    memberList.addAll(it)

                    adapter.updateData(memberList)
                },
                {
                    Log.d(TAG, "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    private fun uploadPic(uri: Uri) {
        dialog.show()

        val fileName = "${roomId}-${System.currentTimeMillis()}.jpg"
        val ref = storage.reference.child("chat_images").child(fileName)

        ref.putFile(uri)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    ref.downloadUrl
                        .addOnSuccessListener { uri ->
                            updateChatRoomImage(uri.toString())
                        }
                        .addOnFailureListener {
                            dialog.show()
                            Toasty.error(this@ChatDetailActivity, "Upload gagal",
                                Toasty.LENGTH_LONG).show()
                        }
                } else {
                    dialog.show()
                    Toasty.error(this@ChatDetailActivity, "Upload gagal",
                        Toasty.LENGTH_LONG).show()
                }
            }
            .addOnFailureListener {
                dialog.show()
                Toasty.error(this@ChatDetailActivity, "Upload gagal",
                    Toasty.LENGTH_LONG).show()
            }
    }

    private fun updateChatRoomImage(imageUrl: String) {
        val ref = db.reference.child("chat_rooms").child(roomId)
        val chatRoom = hashMapOf<String, Any>(
            "pic" to imageUrl
        )

        ref.updateChildren(chatRoom, object : DatabaseReference.CompletionListener {
            override fun onComplete(error: DatabaseError?, reference: DatabaseReference) {
                dialog.dismiss()
            }
        })
    }

    private fun updateChatRoomName() {
        val name = etRoomName.text.toString().trim()
        if (TextUtils.isEmpty(name)) return;

        dialog.show()

        val ref = db.reference.child("chat_rooms").child(roomId)
        val chatRoom = hashMapOf<String, Any>(
            "name" to name
        )

        ref.updateChildren(chatRoom, object : DatabaseReference.CompletionListener {
            override fun onComplete(error: DatabaseError?, reference: DatabaseReference) {
                dialog.dismiss()
                tvName.text = name
                roomName = name
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        EasyImage.handleActivityResult(requestCode, resultCode, data, this,
            object : DefaultCallback() {

                override fun onImagesPicked(
                    imageFiles: MutableList<File>, source: EasyImage.ImageSource?, type: Int
                ) {
                    if (imageFiles.size > 0) {

                        val options = UCrop.Options()
                        options.setCompressionFormat(Bitmap.CompressFormat.JPEG)
                        options.setCompressionQuality(80)
                        options.setShowCropGrid(true)

                        UCrop.of(Uri.fromFile(imageFiles[0]), Uri.fromFile(File(cacheDir, "chat_room_pic.jpg")))
                            .withOptions(options)
                            .withAspectRatio(1f, 1f)
                            .withMaxResultSize(1024, 1024)
                            .start(this@ChatDetailActivity)
                    }
                }

                override fun onCanceled(source: EasyImage.ImageSource?, type: Int) {
                    super.onCanceled(source, type)

                    if (source == EasyImage.ImageSource.CAMERA) {
                        val photoFile = EasyImage.lastlyTakenButCanceledPhoto(this@ChatDetailActivity)
                        photoFile?.delete()
                    }
                }

                override fun onImagePickerError(
                    e: Exception?, source: EasyImage.ImageSource?, type: Int
                ) {
                    super.onImagePickerError(e, source, type)

                    Toasty.error(this@ChatDetailActivity, "Error dalam mengambil gambar",
                        Toasty.LENGTH_LONG).show()
                    e!!.printStackTrace()
                }
            })

        if (requestCode == UCrop.REQUEST_CROP) {
            if (data == null) {
                Toasty.error(this, "Error cropping image", Toasty.LENGTH_LONG).show()
                return;
            }

            if (resultCode == RESULT_OK) {
                val imageUri = UCrop.getOutput(data);

                imageUri?.let {
                    ivPicture.setImageURI(it)
                    uploadPic(it)
                }

            } else if (resultCode == UCrop.RESULT_ERROR) {
                Toasty.error(this, "Error cropping image", Toasty.LENGTH_LONG).show()
                Log.e("Error", "Crop error:" + UCrop.getError(data)!!.message);
            }
        }
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onItemSelected(item: Profile) {
        val intent = Intent(this, ViewProfileActivity::class.java)
        intent.putExtra("id", item.id)
        intent.putExtra("fullname", item.fullname)
        intent.putExtra("email", item.email)
        intent.putExtra("address", item.address)
        intent.putExtra("phone", item.phone)
        intent.putExtra("profile_pic", item.profile_pic)
        intent.putExtra("type", item.type)
        startActivity(intent)
    }

    override fun onItemLongClicked(item: Profile) {

    }

}