package com.bursadesain.android.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bursadesain.android.R
import com.bursadesain.android.app.GlideApp
import com.bursadesain.android.model.UploadedFile
import com.bursadesain.android.util.AppUtil
import kotlinx.android.synthetic.main.item_upload_file.view.*

class UploadedFileAdapter(val itemList: ArrayList<UploadedFile>) : RecyclerView.Adapter<UploadedFileAdapter.ViewHolder>(){

    lateinit var context: Context
    var listener: Listener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_upload_file, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind() {
            val item = itemList.get(adapterPosition)

            if (item.type.equals("image", true)) {
                GlideApp.with(context)
                    .load(item.imagePath)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(itemView.ivFile)
                itemView.tvFileName.setText("${item.fileName} (${item.fileSize}KB)")
            } else if (item.type.equals("doc", true)) {
                AppUtil.setDocumentIcon(context, itemView.ivFile, item.fileName)
                itemView.tvFileName.setText("${item.fileName} (${item.fileSize}KB)")
            }

            itemView.ivDelete.setOnClickListener {
                listener?.onFileRemoved(adapterPosition)
            }
        }
    }

    interface Listener {
        fun onFileRemoved(position: Int)
    }
}