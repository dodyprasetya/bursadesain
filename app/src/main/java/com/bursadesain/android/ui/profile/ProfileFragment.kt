package com.bursadesain.android.ui.profile

import android.app.ProgressDialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bursadesain.android.BuildConfig
import com.bursadesain.android.R
import com.bursadesain.android.app.Constants
import com.bursadesain.android.app.GlideApp
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.ui.login.LoginActivity
import com.bursadesain.android.ui.main.MainActivity
import com.bursadesain.android.util.AppUtil
import com.bursadesain.android.util.PrefUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_profile.*
import java.util.concurrent.TimeUnit

class ProfileFragment : Fragment() {

    val TAG = ProfileFragment::class.java.name

    private val compositeDisposable = CompositeDisposable()

    lateinit var dialog: ProgressDialog

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_profile, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initProgressDialog()

        btLogout.setOnClickListener {
            logout()
        }

        tvEditProfile.setOnClickListener {
            startActivity(Intent(context, EditProfileActivity::class.java))
        }

        tvShare.setOnClickListener {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Bursa Desain App")
            var shareMessage = "\nAplikasi keren untuk pemesanan desain\n\n"
            shareMessage = shareMessage +
                    "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n"
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
            startActivity(Intent.createChooser(shareIntent, "Share app ke.."))
        }

        tvRate.setOnClickListener {
            try {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=${BuildConfig.APPLICATION_ID}")
                    )
                )
            } catch (e: ActivityNotFoundException) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}")
                    )
                )
            }
        }

        loadData()
    }

    private fun initProgressDialog() {
        dialog = ProgressDialog(context!!)
        dialog.setMessage("Please wait..")
        dialog.setIndeterminate(true)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    private fun loadData() {
        val profile = AppUtil.getProfile(context!!)

        profile?.let {
            tvName.text = it.fullname

            if (!TextUtils.isEmpty(it.profile_pic)) {
                GlideApp.with(context!!)
                    .load(it.profile_pic)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .circleCrop()
                    .into(ivProfile)
            }
        }
    }

    fun logout() {
        val profile = AppUtil.getProfile(context!!)
        
        profile?.let {
            val disposable = ServerManager.getInstance()
                .service.updateFcmToken(it.id, "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .debounce(400, TimeUnit.MILLISECONDS)
                .doOnSubscribe { dialog.show() }
                .doOnTerminate { dialog.dismiss() }
                .subscribe(
                    {
                        PrefUtil.remove(activity!!, Constants.PREF_PROFILE)

                        val intent = Intent(activity!!, MainActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                    },
                    {

                    }
                )

            compositeDisposable.add(disposable)
        }
        
    }
}