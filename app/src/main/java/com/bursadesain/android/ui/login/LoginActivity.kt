package com.bursadesain.android.ui.login

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bursadesain.android.R
import com.bursadesain.android.app.Constants
import com.bursadesain.android.model.Profile
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.ui.main.AdminActivity
import com.bursadesain.android.ui.main.DesignerActivity
import com.bursadesain.android.ui.main.MainActivity
import com.bursadesain.android.ui.signup.SignUpActivity
import com.bursadesain.android.util.PrefUtil
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*
import java.util.concurrent.TimeUnit

class LoginActivity : AppCompatActivity() {

    val TAG = LoginActivity::class.java.name

    private val compositeDisposable = CompositeDisposable()

    private val db = FirebaseFirestore.getInstance()

    var profile: Profile? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val profileJson = PrefUtil.read(this, Constants.PREF_PROFILE, "");
        profile = Gson().fromJson(profileJson, Profile::class.java)
        profile?.let {

            val intent: Intent
            if (it.type == 0) {
                intent = Intent(this, AdminActivity::class.java)
            } else if (it.type == 1) {
                intent = Intent(this, MainActivity::class.java)
            } else {
                intent = Intent(this, DesignerActivity::class.java)
            }
            startActivity(intent)

            finish()
        }

        setContentView(R.layout.activity_login)
    }

    fun signIn(view: View) {
        val email = etEmail.text.toString();
        val password = etPassword.text.toString();

        if (
            !TextUtils.isEmpty(email) &&
            !TextUtils.isEmpty(password)
        ) {

            val disposable = ServerManager.getInstance()
                .service.login(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .debounce(400, TimeUnit.MILLISECONDS)
                .doOnSubscribe {
                    progressBar.visibility = View.VISIBLE
                    btSignIn.isEnabled = false
                }
                .doOnTerminate {
                    progressBar.visibility = View.INVISIBLE
                    btSignIn.isEnabled = true
                }
                .subscribe(
                    {
                        checkLoginData(it)
                    },
                    {
                        Toasty.error(this, "Login gagal", Toasty.LENGTH_LONG).show()
                    }
                )

            compositeDisposable.add(disposable)

        } else {
            Toasty.info(this, "Please fill all fields", Toasty.LENGTH_LONG).show()
        }
    }

    fun goToSignUp(view: View) {
        startActivity(Intent(this, SignUpActivity::class.java))
    }

    fun checkLoginData(profile: Profile) {
        val profileJson = Gson().toJson(profile)
        PrefUtil.write(this, Constants.PREF_PROFILE, profileJson)

        val intent: Intent
        if (profile.type == 0) {
            intent = Intent(this, AdminActivity::class.java)
        } else if (profile.type == 1) {
            intent = Intent(this, MainActivity::class.java)
        } else {
            intent = Intent(this, DesignerActivity::class.java)
        }

        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}