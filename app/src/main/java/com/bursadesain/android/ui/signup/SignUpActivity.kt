package com.bursadesain.android.ui.signup

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bursadesain.android.R
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.util.PermissionUtil
import com.bursadesain.android.util.RequestUtil
import com.yalantis.ucrop.UCrop
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_signup.*
import okhttp3.RequestBody
import java.io.File
import java.util.concurrent.TimeUnit

class SignUpActivity : AppCompatActivity() {

    private val PICK_IMAGE = 100

    lateinit var dialog: ProgressDialog
    private var imageUri: Uri? = null

    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        initProgressDialog()

        btSignUp.setOnClickListener {
            signUp()
        }
    }

    override fun onStart() {
        super.onStart()
        PermissionUtil.askStoragePermissions(this)
    }

    private fun initProgressDialog() {
        dialog = ProgressDialog(this);
        dialog.setMessage("Please wait..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
    }

    private fun signUp() {
        if (imageUri != null) {
            val fullname = etFullname.text.toString();
            val email = etEmail.text.toString();
            val address = etAddress.text.toString();
            val phone = etPhone.text.toString();
            val password = etPassword.text.toString();

            if (
                !TextUtils.isEmpty(fullname) &&
                !TextUtils.isEmpty(email) &&
                !TextUtils.isEmpty(address) &&
                !TextUtils.isEmpty(phone) &&
                !TextUtils.isEmpty(password)
            ) {
                postUser()
            } else {
                Toasty.info(this, "Isi semua data", Toasty.LENGTH_LONG).show()
            }
        } else {
            Toasty.info(this, "Pilih gambar profile", Toasty.LENGTH_LONG).show()
        }
    }

    private fun postUser() {
        val fullname = etFullname.text.toString();
        val email = etEmail.text.toString();
        val address = etAddress.text.toString();
        val phone = etPhone.text.toString();
        val password = etPassword.text.toString();

        val user = hashMapOf(
            "email" to RequestUtil.getBody(email),
            "fullname" to RequestUtil.getBody(fullname),
            "password" to RequestUtil.getBody(password),
            "address" to RequestUtil.getBody(address),
            "phone" to RequestUtil.getBody(phone)
        )

        val fileRequestBody = RequestUtil.getFileBody(imageUri!!)

        val disposable = ServerManager.getInstance()
            .service.signUp(user, fileRequestBody)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { dialog.show() }
            .doOnTerminate { dialog.dismiss() }
            .subscribe(
                {
                    if (it.equals("Berhasil")) {
                        Toasty.success(this, "Pendaftaran berhasil", Toasty.LENGTH_LONG).show()
                        finish()
                    } else if (it.equals("Gagal Upload") || it.equals("No Image")) {
                        Toasty.error(this, "Upload gambar gagal", Toasty.LENGTH_LONG).show()
                    } else {
                        Toasty.error(this, "Pendaftaran gagal", Toasty.LENGTH_LONG).show()
                    }
                },
                {
                    Toasty.error(this, "Pendaftaran gagal", Toasty.LENGTH_LONG).show()
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            if (data == null) {
                Toasty.error(this, "Error picking image", Toasty.LENGTH_LONG).show()
                return;
            }

            imageUri = data.getData()

            val options = UCrop.Options()
            options.setCompressionFormat(Bitmap.CompressFormat.JPEG)
            options.setCompressionQuality(80)
            options.setShowCropGrid(true)

            UCrop.of(imageUri!!, Uri.fromFile(File(cacheDir, "user_profile_pic.jpg")))
                .withAspectRatio(1f, 1f)
                .withOptions(options)
                .withMaxResultSize(1024, 1024)
                .start(this)
        }
        else if (requestCode == UCrop.REQUEST_CROP) {
            if (data == null) {
                Toasty.error(this, "Error cropping image", Toasty.LENGTH_LONG).show()
                return;
            }

            if (resultCode == RESULT_OK) {
                imageUri = UCrop.getOutput(data);
                ivProfile.setImageURI(imageUri);
            } else if (resultCode == UCrop.RESULT_ERROR) {
                Toasty.error(this, "Error cropping image", Toasty.LENGTH_LONG).show()
                Log.e("Error", "Crop error:" + UCrop.getError(data)!!.message);
            }
        }
    }

    fun selectProfilePic(view: View) {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Pilih gambar profil"), PICK_IMAGE)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}