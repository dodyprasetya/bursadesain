package com.bursadesain.android.ui.adapter

import android.content.Context
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bursadesain.android.R
import com.bursadesain.android.app.GlideApp
import com.bursadesain.android.model.ChatMessage
import com.bursadesain.android.model.Profile
import com.bursadesain.android.util.DateUtil

class ChatMessageAdapter(val myUserId: String) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    private val MY_CHAT_MESSAGE = 1
    private val MY_CHAT_IMAGE = 2
    private val MY_CHAT_AUDIO = 3
    private val OTHER_CHAT_MESSAGE = 4
    private val OTHER_CHAT_IMAGE = 5
    private val OTHER_CHAT_AUDIO = 6

    private lateinit var memberList: List<Profile>
    private lateinit var itemList: List<ChatMessage>
    lateinit var context: Context
    var listener: Listener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context

        when(viewType) {
            MY_CHAT_MESSAGE -> {
                val view = LayoutInflater.from(context).inflate(R.layout.item_chat_message_me, parent, false)
                return MyChatMessageViewHolder(view)
            }
            MY_CHAT_IMAGE -> {
                val view = LayoutInflater.from(context).inflate(R.layout.item_chat_image_me, parent, false)
                return MyChatImageViewHolder(view)
            }
            MY_CHAT_AUDIO -> {
                val view = LayoutInflater.from(context).inflate(R.layout.item_chat_audio_me, parent, false)
                return MyChatAudioViewHolder(view)
            }
            OTHER_CHAT_MESSAGE -> {
                val view = LayoutInflater.from(context).inflate(R.layout.item_chat_message_other, parent, false)
                return OtherChatMessageViewHolder(view)
            }
            OTHER_CHAT_IMAGE -> {
                val view = LayoutInflater.from(context).inflate(R.layout.item_chat_image_other, parent, false)
                return OtherChatImageViewHolder(view)
            }
            else -> {
                val view = LayoutInflater.from(context).inflate(R.layout.item_chat_audio_other, parent, false)
                return OtherChatAudioViewHolder(view)
            }
        }
    }

    override fun getItemCount(): Int {
        return if (::itemList.isInitialized) itemList.size else 0
    }

    override fun getItemViewType(position: Int): Int {
        val item = itemList.get(position)
        val isMyMessage = item.user_id.equals(myUserId)

        when (item.type) {
            "text" -> return if (isMyMessage) MY_CHAT_MESSAGE else OTHER_CHAT_MESSAGE
            "image" -> return if (isMyMessage) MY_CHAT_IMAGE else OTHER_CHAT_IMAGE
            "audio" -> return if (isMyMessage) MY_CHAT_AUDIO else OTHER_CHAT_AUDIO
        }

        return super.getItemViewType(position)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = itemList.get(position)
        val isMyMessage = item.user_id.equals(myUserId)

        when(item.type) {
            "text" -> {
                if (isMyMessage) (holder as MyChatMessageViewHolder).bind()
                else (holder as OtherChatMessageViewHolder).bind()
            }
            "image" -> {
                if (isMyMessage) (holder as MyChatImageViewHolder).bind()
                else (holder as OtherChatImageViewHolder).bind()
            }
            "audio" -> {
                if (isMyMessage) (holder as MyChatAudioViewHolder).bind()
                else (holder as OtherChatAudioViewHolder).bind()
            }
        }
    }

    fun updateData(list: ArrayList<ChatMessage>) {
        itemList = list;
        notifyDataSetChanged()
    }

    fun updateMemberList(list: ArrayList<Profile>) {
        memberList = list;
        notifyDataSetChanged()
    }

    inner class MyChatMessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tvMessage: TextView
        var tvDate: TextView
        var tvTime: TextView

        init {
            tvMessage = itemView.findViewById(R.id.tvMessage)
            tvDate = itemView.findViewById(R.id.tvDate)
            tvTime = itemView.findViewById(R.id.tvTime)
        }

        fun bind() {
            val item = itemList.get(adapterPosition)
            tvMessage.text = item.message
            tvTime.text = item.timestamp.substring(11, 16)
            setDate(adapterPosition, item.timestamp, tvDate)
        }
    }

    inner class OtherChatMessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tvMessage: TextView
        var tvDate: TextView
        var tvName: TextView
        var tvTime: TextView

        init {
            tvMessage = itemView.findViewById(R.id.tvMessage)
            tvDate = itemView.findViewById(R.id.tvDate)
            tvName = itemView.findViewById(R.id.tvName)
            tvTime = itemView.findViewById(R.id.tvTime)
        }

        fun bind() {
            val item = itemList.get(adapterPosition)
            tvName.text = getName(item.user_id)
            tvMessage.text = item.message
            tvTime.text = item.timestamp.substring(11, 16)
            setDate(adapterPosition, item.timestamp, tvDate)
        }
    }

    inner class MyChatImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var ivImage: ImageView
        var ivNotSent: ImageView
        var tvDate: TextView
        var tvTime: TextView
        var pbUpload: ProgressBar

        init {
            ivImage = itemView.findViewById(R.id.ivImage)
            ivNotSent = itemView.findViewById(R.id.ivNotSent)
            tvDate = itemView.findViewById(R.id.tvDate)
            tvTime = itemView.findViewById(R.id.tvTime)
            pbUpload = itemView.findViewById(R.id.pbUpload)
        }

        fun bind() {
            val item = itemList.get(adapterPosition)
            tvTime.text = item.timestamp.substring(11, 16)
            setDate(adapterPosition, item.timestamp, tvDate)

            if (item.status.equals("2")) {
                pbUpload.visibility = View.VISIBLE

                GlideApp.with(context)
                    .load(item.file_url)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .centerCrop()
                    .into(ivImage)
            } else {
                pbUpload.visibility = View.GONE

                GlideApp.with(context)
                    .load(item.file_url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .into(ivImage)
            }

            if (item.status.equals("3")) ivNotSent.visibility = View.VISIBLE
            else ivNotSent.visibility = View.GONE

            ivImage.setOnClickListener {
                listener?.onItemClicked("image", item)
            }
        }
    }

    inner class OtherChatImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var ivImage: ImageView
        var tvDate: TextView
        var tvTime: TextView
        var tvName: TextView

        init {
            ivImage = itemView.findViewById(R.id.ivImage)
            tvDate = itemView.findViewById(R.id.tvDate)
            tvTime = itemView.findViewById(R.id.tvTime)
            tvName = itemView.findViewById(R.id.tvName)
        }

        fun bind() {
            val item = itemList.get(adapterPosition)
            tvName.text = getName(item.user_id)
            tvTime.text = item.timestamp.substring(11, 16)
            setDate(adapterPosition, item.timestamp, tvDate)

            GlideApp.with(context)
                .load(item.file_url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(ivImage)

            ivImage.setOnClickListener {
                listener?.onItemClicked("image", item)
            }
        }
    }

    inner class MyChatAudioViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var ivNotSent: ImageView
        var tvDate: TextView
        var tvTime: TextView
        var pbUpload: ProgressBar
        var llAudio: LinearLayout

        init {
            ivNotSent = itemView.findViewById(R.id.ivNotSent)
            tvDate = itemView.findViewById(R.id.tvDate)
            tvTime = itemView.findViewById(R.id.tvTime)
            pbUpload = itemView.findViewById(R.id.pbUpload)
            llAudio = itemView.findViewById(R.id.llAudio)
        }

        fun bind() {
            val item = itemList.get(adapterPosition)
            tvTime.text = item.timestamp.substring(11, 16)
            setDate(adapterPosition, item.timestamp, tvDate)

            if (item.status.equals("2")) {
                pbUpload.visibility = View.VISIBLE
            } else {
                pbUpload.visibility = View.GONE
            }

            if (item.status.equals("3")) ivNotSent.visibility = View.VISIBLE
            else ivNotSent.visibility = View.GONE

            llAudio.setOnClickListener {
                listener?.onItemClicked("audio", item)
            }
        }
    }

    inner class OtherChatAudioViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tvDate: TextView
        var tvTime: TextView
        var tvName: TextView
        var llAudio: LinearLayout

        init {
            tvDate = itemView.findViewById(R.id.tvDate)
            tvTime = itemView.findViewById(R.id.tvTime)
            tvName = itemView.findViewById(R.id.tvName)
            llAudio = itemView.findViewById(R.id.llAudio)
        }

        fun bind() {
            val item = itemList.get(adapterPosition)
            tvName.text = getName(item.user_id)
            tvTime.text = item.timestamp.substring(11, 16)
            setDate(adapterPosition, item.timestamp, tvDate)

            llAudio.setOnClickListener {
                listener?.onItemClicked("audio", item)
            }
        }
    }

    interface Listener {
        fun onItemClicked(type: String, item: ChatMessage)
    }

    private fun getName(userId: String): String {
        for (member in memberList) {
            if (member.id.equals(userId)) {
                return member.fullname
            }
        }
        return userId
    }

    private fun setDate(adapterPosition: Int, timestamp: String, tvDate: TextView) {
        if (adapterPosition != 0) {
            val chat = itemList.get(adapterPosition - 1)
            if (chat.timestamp.substring(0, 10).equals(timestamp.substring(0, 10))) {
                tvDate.visibility = View.GONE
            } else {
                tvDate.visibility = View.VISIBLE
                tvDate.text = DateUtil.getDateWithShortMonthName(context, timestamp,
                    DateUtil.MYSQL_DATE_TIME_FORMAT)
            }
        } else {
            tvDate.visibility = View.VISIBLE
            tvDate.text = DateUtil.getDateWithShortMonthName(context, timestamp,
                DateUtil.MYSQL_DATE_TIME_FORMAT)
        }
    }

    fun getAudioDuration(uri: Uri): String {
        try {
            val media = MediaMetadataRetriever()
            media.setDataSource(context, uri)

            val durationStr = media.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)

            val duration = durationStr.toInt()
            val second = (duration / 1000 % 60).toLong()
            val minute = (duration / (1000 * 60) % 60).toLong()

            return String.format("%02d:%02d", minute, second)

        } catch (e: Exception) {
        }
        return ""
    }
}