package com.bursadesain.android.ui.adapter

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bursadesain.android.R
import com.bursadesain.android.app.GlideApp
import com.bursadesain.android.model.ChatRoom
import kotlinx.android.synthetic.main.item_chat_room.view.*

class ChatRoomAdapter : RecyclerView.Adapter<ChatRoomAdapter.ViewHolder>(){

    private lateinit var itemList: List<ChatRoom>
    lateinit var context: Context
    var listener: Listener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_chat_room, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if (::itemList.isInitialized) itemList.size else 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    fun updateData(list: ArrayList<ChatRoom>) {
        itemList = list;
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind() {
            val item = itemList.get(adapterPosition)

            itemView.tvOrderId.text = "ORDER-${item.id}"
            itemView.tvName.text = item.name
            itemView.tvLatestMessage.text = item.latest_message

            if (!TextUtils.isEmpty(item.pic)) {
                GlideApp.with(context)
                    .load(item.pic)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .circleCrop()
                    .into(itemView.ivPicture)
            } else {
                itemView.ivPicture.setImageResource(R.color.package1)
            }

            itemView.container.setOnClickListener {
                listener?.onItemSelected(item)
            }
        }
    }

    interface Listener {
        fun onItemSelected(item: ChatRoom)
    }
}