package com.bursadesain.android.ui.order

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bursadesain.android.R
import com.bursadesain.android.app.Constants
import com.bursadesain.android.model.Order
import com.bursadesain.android.model.Profile
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.ui.adapter.OrderAdapter
import com.bursadesain.android.ui.payment.ConfirmPaymentActivity
import com.bursadesain.android.util.PrefUtil
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_order.*
import java.util.concurrent.TimeUnit

class OrderFragment : Fragment(), OrderAdapter.Listener {

    val TAG = OrderFragment::class.java.name

    private val compositeDisposable = CompositeDisposable()

    val itemList = ArrayList<Order>()
    lateinit var adapter: OrderAdapter

    lateinit var profile: Profile

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_order, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.inflateMenu(R.menu.menu_history)
        toolbar.menu.findItem(R.id.menu_history).setOnMenuItemClickListener {
            startActivity(Intent(context, OrderHistoryActivity::class.java))
            true
        }

        val profileJson = PrefUtil.read(activity!!, Constants.PREF_PROFILE, "");
        profile = Gson().fromJson(profileJson, Profile::class.java)

        adapter = OrderAdapter()
        adapter.listener = this

        rvOrders.adapter = adapter

        refreshLayout.setOnRefreshListener {
            loadData()
        }

        loadData()
    }

    private fun loadData() {
        val disposable = ServerManager.getInstance()
            .service.loadUserOrders(profile.id, "user")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { refreshLayout.isRefreshing = true }
            .doOnTerminate { refreshLayout.isRefreshing = false }
            .subscribe(
                {
                    itemList.clear()
                    itemList.addAll(it)
                    adapter.updateData(itemList)
                },
                {
                    Log.d(TAG, "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onItemSelected(item: Order) {
        val intent = Intent(activity, OrderDetailActivity::class.java)
        intent.putExtra("order_id", item.id)
        startActivity(intent)
    }

    override fun onConfirmPayment(item: Order) {
        val intent = Intent(activity, ConfirmPaymentActivity::class.java)
        intent.putExtra("order_id", item.id)
        intent.putExtra("package_name", item.package_name)
        startActivity(intent)
    }
}