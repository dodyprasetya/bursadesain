package com.bursadesain.android.ui.designer

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bursadesain.android.R
import com.bursadesain.android.app.Constants
import com.bursadesain.android.model.Order
import com.bursadesain.android.model.Profile
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.ui.adapter.AdminOrderAdapter
import com.bursadesain.android.ui.order.OrderDetailActivity
import com.bursadesain.android.util.AppUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_order.*
import java.util.concurrent.TimeUnit

class DesignerOrderListFragment : Fragment(), AdminOrderAdapter.Listener {

    val TAG = DesignerOrderListFragment::class.java.name

    private val compositeDisposable = CompositeDisposable()

    val itemList = ArrayList<Order>()
    lateinit var adapter: AdminOrderAdapter

    lateinit var profile: Profile

    private var status: String = "designer_active"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_admin_order_list, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            status = it.getString("status", "designer_active")
        }

        adapter = AdminOrderAdapter()
        adapter.listener = this

        rvOrders.adapter = adapter

        refreshLayout.setOnRefreshListener {
            loadData()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        profile = AppUtil.getProfile(context)!!
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    private fun loadData() {
        val orderStatus =
            if (status.equals("designer_cancelled") || status.equals("designer_completed"))
                "designer_history"
            else
                status

        val disposable = ServerManager.getInstance()
            .service.loadDesignerOrders(orderStatus, profile.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .doOnSubscribe { refreshLayout.isRefreshing = true }
            .doOnTerminate { refreshLayout.isRefreshing = false }
            .subscribe(
                {
                    itemList.clear()

                    if (status.equals("designer_cancelled")) {
                        for (data in it) {
                            if (data.status == Constants.ORDER_STATUS_CANCELLED) {
                                itemList.add(data)
                            }
                        }
                    } else if (status.equals("designer_completed")) {
                        for (data in it) {
                            if (data.status == Constants.ORDER_STATUS_COMPLETED) {
                                itemList.add(data)
                            }
                        }
                    } else {
                        itemList.addAll(it)
                    }

                    adapter.updateData(itemList)
                },
                {
                    Log.d(TAG, "Error getting datas.", it)
                }
            )

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onItemSelected(item: Order) {
        val intent = Intent(activity, OrderDetailActivity::class.java)
        intent.putExtra("order_id", item.id)
        startActivity(intent)
    }
}