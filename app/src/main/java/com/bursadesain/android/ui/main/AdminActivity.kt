package com.bursadesain.android.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.bursadesain.android.R
import com.bursadesain.android.app.Constants
import com.bursadesain.android.server.ServerManager
import com.bursadesain.android.ui.chat.AdminChatFragment
import com.bursadesain.android.ui.home.AdminHomeFragment
import com.bursadesain.android.ui.home.AdminOrderFragment
import com.bursadesain.android.ui.profile.ProfileFragment
import com.bursadesain.android.util.AppUtil
import com.bursadesain.android.util.PrefUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class AdminActivity : AppCompatActivity() {

    private val compositeDisposable = CompositeDisposable()

    lateinit var home: Fragment
    lateinit var order: Fragment
    lateinit var chat: Fragment
    lateinit var profile: Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        home = AdminHomeFragment()
        order = AdminOrderFragment()
        chat = AdminChatFragment()
        profile = ProfileFragment()

        loadFragment(home)

        bottomNavView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_home -> {
                    loadFragment(home)
                    true
                }
                R.id.navigation_order -> {
                    loadFragment(order)
                    true
                }
                R.id.navigation_chat -> {
                    loadFragment(chat)
                    true
                }
                R.id.navigation_profile -> {
                    loadFragment(profile)
                    true
                }
                else -> false
            }
        }

        checkUser()
    }

    private fun loadFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frameLayout, fragment)
        transaction.commit()
    }

    private fun updateFcmToken() {
        val token = PrefUtil.read(this, Constants.PREF_FCM_TOKEN, "")

        val profile = AppUtil.getProfile(this)!!

        val disposable = ServerManager.getInstance()
            .service.updateFcmToken(profile.id, token!!)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .subscribe({}, {})

        compositeDisposable.add(disposable)
    }

    private fun checkUser() {
        val profile = AppUtil.getProfile(this)!!

        val disposable = ServerManager.getInstance()
            .service.checkUser(profile.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .subscribe({
                if (it.equals("Valid")) {
                    updateFcmToken()
                } else {
                    // force logout
                    PrefUtil.remove(this, Constants.PREF_PROFILE)

                    val intent = Intent(this, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                }
            }, {})

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}
