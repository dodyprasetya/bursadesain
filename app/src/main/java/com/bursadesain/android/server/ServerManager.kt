package com.bursadesain.android.server

import android.util.Log
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit

public class ServerManager {

    lateinit var serverApi: ServerApi
    private val enableLog = true

    init {
        createService()
    }

    val service: ServerApi get() = serverApi

    private fun createService() {
        val loggingInterceptor =
            HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message ->
                if (enableLog) {
                    Log.i("Retrofit", message)
                }
            })
        loggingInterceptor.level =
            if (enableLog) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

        // header interceptor
        val headerInterceptor = ServerInterceptor()

        val client = OkHttpClient.Builder()
            .readTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(120, TimeUnit.SECONDS)
            .addInterceptor(loggingInterceptor)
            .addInterceptor(headerInterceptor)
            .build()

        val gson = GsonBuilder()
            .setLenient()
            .create()

        val retrofit: Retrofit = Retrofit.Builder()
            .client(client)
            .baseUrl(ServerUrl.baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

        serverApi = retrofit.create<ServerApi>(ServerApi::class.java)
    }

    private inner class ServerInterceptor : Interceptor {
        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            val original = chain.request()
            val request = original.newBuilder()
                .header("Content-Type", "application/json")
                .method(original.method(), original.body())
                .build()
            if (enableLog) Log.i("Retrofit", "Headers:\n" + request.headers().toString())
            return chain.proceed(request)
        }
    }

    companion object {

        private var instance: ServerManager? = null

        fun getInstance(): ServerManager {
            return instance ?: synchronized(this) {
                ServerManager().also { instance = it }
            }
        }
    }
}