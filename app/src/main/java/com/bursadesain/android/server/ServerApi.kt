package com.bursadesain.android.server

import com.bursadesain.android.model.*
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface ServerApi {

    @FormUrlEncoded
    @POST("login.php")
    fun login(
        @Field("username") username: String,
        @Field("password") password: String
    ): Observable<Profile>

    @Multipart
    @POST("signUp.php")
    fun signUp(
        @PartMap map: HashMap<String, RequestBody>,
        @Part file: MultipartBody.Part
    ): Observable<String>

    @FormUrlEncoded
    @POST("updateFcmToken.php")
    fun updateFcmToken(
        @Field("user_id") userId: String, @Field("fcm_token") fcmToken: String
    ): Observable<String>

    @FormUrlEncoded
    @POST("checkUser.php")
    fun checkUser(@Field("user_id") userId: String): Observable<String>

    @POST("listMember.php")
    fun loadMembers(): Observable<List<Profile>>

    @POST("listDesigner.php")
    fun loadDesigners(): Observable<List<Profile>>

    @Multipart
    @POST("addDesigner.php")
    fun addDesigner(
        @PartMap map: HashMap<String, RequestBody>,
        @Part file: MultipartBody.Part
    ): Observable<String>

    @FormUrlEncoded
    @POST("editDesigner.php")
    fun editDesigner(@FieldMap map: HashMap<String, String>): Observable<String>

    @FormUrlEncoded
    @POST("deleteDesigner.php")
    fun deleteDesigner(@Field("designer_id") designerId: String): Observable<String>

    @POST("listPackageCategory.php")
    fun loadPackageCategories(): Observable<List<PackageCategory>>

    @FormUrlEncoded
    @POST("listPackageCategory.php")
    fun loadPackageCategories(
        @Field("designer_id") designerId: String
    ): Observable<List<PackageCategory>>

    @FormUrlEncoded
    @POST("addPackageCategory.php")
    fun addPackageCategory(
        @Field("name") name: String,
        @Field("price") price: String,
        @Field("designer_id") designerId: String
    ): Observable<String>

    @FormUrlEncoded
    @POST("editPackageCategory.php")
    fun editPackageCategory(
        @Field("name") name: String,
        @Field("price") price: String,
        @Field("package_category_id") categoryId: String
    ): Observable<String>

    @FormUrlEncoded
    @POST("listPackage.php")
    fun loadPackages(@Field("category_id") categoryId: String): Observable<List<Package>>

    @FormUrlEncoded
    @POST("listPackage.php")
    fun loadPackages(
        @Field("category_id") categoryId: String,
        @Field("designer_id") designerId: String
    ): Observable<List<Package>>

    @FormUrlEncoded
    @POST("searchPackage.php")
    fun searchPackages(@Field("keyword") keyword: String): Observable<List<Package>>

    @FormUrlEncoded
    @POST("packageDetail.php")
    fun loadPackageDetail(@Field("package_id") packageId: String): Observable<Package>

//    @FormUrlEncoded
//    @POST("addPackage.php")
//    fun addPackage(@FieldMap map: HashMap<String, String>): Observable<String>

    @Multipart
    @POST("addPackage.php")
    fun addPackage(@PartMap map: HashMap<String, RequestBody>): Observable<String>

    @Multipart
    @POST("addPackage.php")
    fun addPackageWithIcon(
        @PartMap map: HashMap<String, RequestBody>,
        @Part file: MultipartBody.Part
    ): Observable<String>

//    @FormUrlEncoded
//    @POST("editPackage.php")
//    fun editPackage(@FieldMap map: HashMap<String, String>): Observable<String>

    @Multipart
    @POST("editPackage.php")
    fun editPackage(@PartMap map: HashMap<String, RequestBody>): Observable<String>

    @Multipart
    @POST("editPackage.php")
    fun editPackageWithIcon(
        @PartMap map: HashMap<String, RequestBody>,
        @Part file: MultipartBody.Part
    ): Observable<String>

    @Multipart
    @POST("addPortfolios.php")
    fun addPortfolios(
        @PartMap map: HashMap<String, RequestBody>,
        @Part files: Array<MultipartBody.Part>
    ): Observable<String>

    @Multipart
    @POST("addPortfolio.php")
    fun addPortfolio(
        @PartMap map: HashMap<String, RequestBody>,
        @Part file: MultipartBody.Part
    ): Observable<String>

    @FormUrlEncoded
    @POST("deletePortfolio.php")
    fun deletePortfolio(@Field("porfolio_id") porfolioId: String): Observable<String>

    @Multipart
    @POST("createOrder.php")
    fun createOrder(
        @PartMap map: HashMap<String, RequestBody>,
        @Part files: Array<MultipartBody.Part?>
    ): Observable<String>

    @FormUrlEncoded
    @POST("listOrder.php")
    fun loadUserOrders(
        @Field("user_id") userId: String,
        @Field("order_status") orderStatus: String
    ): Observable<List<Order>>

    @FormUrlEncoded
    @POST("listOrder.php")
    fun loadAdminOrders(@Field("order_status") orderStatus: String): Observable<List<Order>>

    @FormUrlEncoded
    @POST("listOrder.php")
    fun loadDesignerOrders(
        @Field("order_status") orderStatus: String,
        @Field("designer_id") designerId: String
    ): Observable<List<Order>>

    @FormUrlEncoded
    @POST("orderDetail.php")
    fun loadOrderDetail(@Field("order_id") orderId: String): Observable<Order>

    @FormUrlEncoded
    @POST("listPaymentProof.php")
    fun loadPaymentProofs(@Field("order_id") orderId: String): Observable<List<PaymentProof>>

    @Multipart
    @POST("confirmPayment.php")
    fun confirmPayment(
        @PartMap map: HashMap<String, RequestBody>,
        @Part file: MultipartBody.Part
    ): Observable<String>

    @FormUrlEncoded
    @POST("validatePayment.php")
    fun validatePayment(
        @Field("order_id") orderId: String,
        @Field("proof_id") proofId: String,
        @Field("is_valid") isValid: Boolean
    ): Observable<String>

    @FormUrlEncoded
    @POST("assignDesigner.php")
    fun assignDesigner(
        @Field("order_id") orderId: String,
        @Field("designer_id") designer_id: String
    ): Observable<String>

    @FormUrlEncoded
    @POST("startOrder.php")
    fun startOrder(
        @Field("order_id") orderId: String, @Field("deadline") deadline: String
    ): Observable<String>

    @FormUrlEncoded
    @POST("updateDeadline.php")
    fun updateDeadline(
        @Field("order_id") orderId: String, @Field("deadline") deadline: String
    ): Observable<String>

    @FormUrlEncoded
    @POST("completeOrder.php")
    fun completeOrder(@Field("order_id") orderId: String): Observable<String>

    @FormUrlEncoded
    @POST("submitRating.php")
    fun submitRating(
        @Field("order_id") orderId: String,
        @Field("rating") rating: Float
    ): Observable<String>

    @FormUrlEncoded
    @POST("cancelOrder.php")
    fun cancelOrder(@Field("order_id") orderId: String): Observable<String>

    @FormUrlEncoded
    @POST("updateProfile.php")
    fun updateProfile(@FieldMap map: HashMap<String, String>): Observable<String>

    @Multipart
    @POST("updateProfilePic.php")
    fun updateProfilePic(
        @PartMap map: HashMap<String, RequestBody>,
        @Part file: MultipartBody.Part
    ): Observable<UrlData>

    @FormUrlEncoded
    @POST("listChatMember.php")
    fun loadChatRoomMembers(@Field("order_id") orderId: String): Observable<List<Profile>>

    @POST("summaryOrderAdmin.php")
    fun loadSummaryOrderAdmin(): Observable<OrderSummary>

    @FormUrlEncoded
    @POST("summaryOrderDesigner.php")
    fun loadSummaryOrderDesigner(@Field("designer_id") designerId: String): Observable<OrderSummary>

    @FormUrlEncoded
    @POST("sendChatNotif.php")
    fun sendChatNotif(@FieldMap map: HashMap<String, String>): Observable<String>
}