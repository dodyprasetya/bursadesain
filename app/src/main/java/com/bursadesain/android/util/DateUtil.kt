package com.bursadesain.android.util

import android.content.Context
import android.text.TextUtils
import com.bursadesain.android.R
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateUtil {

    const val MYSQL_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss"
    const val MYSQL_DATE_FORMAT = "yyyy-MM-dd"

    const val LOCAL_DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss"
    const val LOCAL_DATE_FORMAT = "dd/MM/yyyy"

    fun getShortMonthName(context: Context, value: Int): String {
        val monthLabels =
            context.resources.getStringArray(R.array.short_month_names)
        return monthLabels[value]
    }

    // output format: 1 Jan 1990
    fun getDateWithShortMonthName(
        context: Context, date: String? = null, inputFormat: String? = null
    ): String {

        var cal = Calendar.getInstance()

        // if there is date string param
        date?.let {
            inputFormat?.let {
                val dateFormat = SimpleDateFormat(inputFormat, Locale.US)
                dateFormat.parse(date)
                cal = dateFormat.calendar
            }
        }

        val day = cal.get(Calendar.DAY_OF_MONTH)
        val month = getShortMonthName(context, cal.get(Calendar.MONTH))
        val year = cal.get(Calendar.YEAR)
        return "${day} ${month} ${year}"
    }

    // get calendar with custom output format
    fun getFormattedCalendar(cal: Calendar, outputFormat: String): String {
        val date = cal.time
        val dateFormat: DateFormat = SimpleDateFormat(outputFormat, Locale.US)
        return dateFormat.format(date)
    }

    // get calendar from date string
    fun getCalendarFromString(date: String, inputFormat: String): Calendar? {
        if (!TextUtils.isEmpty(date)) {
            try {
                val dateFormat = SimpleDateFormat(inputFormat, Locale.US)
                dateFormat.parse(date)
                return dateFormat.calendar
            } catch (e: ParseException) {
                e.printStackTrace()
            }
        }
        return null
    }
}