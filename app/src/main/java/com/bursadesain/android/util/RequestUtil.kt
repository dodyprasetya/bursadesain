package com.bursadesain.android.util

import android.net.Uri
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

object RequestUtil {

    fun getBody(param: String): RequestBody {
        return RequestBody.create(MediaType.parse("text/plain"), param)
    }

    fun getFileBody(fileUri: Uri): MultipartBody.Part {
        val filePath = fileUri.path!!
        val mimeType = FileUtil.getInstance().getMimeType(filePath)
        val file = File(filePath)

        val requestFile = RequestBody.create(MediaType.parse(mimeType), file)
        val fileRequestBody = MultipartBody.Part.createFormData(
            "file", file.getName(), requestFile)

        return fileRequestBody
    }
}