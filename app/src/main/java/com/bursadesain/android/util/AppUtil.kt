package com.bursadesain.android.util

import android.app.Activity
import android.app.ActivityManager
import android.app.ActivityManager.RunningAppProcessInfo
import android.content.Context
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.bursadesain.android.R
import com.bursadesain.android.app.Constants
import com.bursadesain.android.model.Profile
import com.google.gson.Gson
import java.text.NumberFormat
import java.util.*

object AppUtil {

    fun convertToMoney(price: Long): String {
        return NumberFormat.getNumberInstance(Locale.US)
            .format(price)
            .replace(",", ".")
    }

    fun convertToMoney(price: Int): String {
        return NumberFormat.getNumberInstance(Locale.US)
            .format(price.toLong())
            .replace(",", ".")
    }

    fun convertToMoney(price: Float): String {
        var priceStr = String.format(Locale.US, "%,.2f", price)
        val endIndex = priceStr.lastIndexOf(".")
        if (endIndex != -1) priceStr = priceStr.substring(0, endIndex)
        return priceStr
    }

    fun convertToMoney(price: Double): String {
        var priceStr = String.format(Locale.US, "%,.2f", price)
        val endIndex = priceStr.lastIndexOf(".")
        if (endIndex != -1) priceStr = priceStr.substring(0, endIndex)
        // priceStr = priceStr.replace(",", ".");
        return priceStr
    }

    fun hideKeyboard(context: Activity) {
        val view = context.currentFocus
        view?.let {
            val imm = context
                .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun isAppIsInBackground(context: Context): Boolean {
        var isInBackground = true
        val am = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val runningProcesses = am.runningAppProcesses
        for (processInfo in runningProcesses) {
            if (processInfo.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                for (activeProcess in processInfo.pkgList) {
                    if (activeProcess == context.packageName) {
                        isInBackground = false
                    }
                }
            }
        }
        return isInBackground
    }

    fun setDocumentIcon(context: Context, imageView: ImageView, fileName: String) {
        var drawableId = 0
        if (fileName.toLowerCase().contains(".doc") ||
            fileName.toLowerCase().contains(".docx")) {
            drawableId = R.drawable.ic_doc
        } else if (fileName.toLowerCase().contains(".pdf")) {
            drawableId = R.drawable.ic_pdf
        }
        if (drawableId != 0)
            imageView.setImageDrawable(ContextCompat.getDrawable(context, drawableId))
    }

    fun getProfile(context: Context): Profile? {
        val profileJson = PrefUtil.read(context, Constants.PREF_PROFILE, "");
        val profile = Gson().fromJson(profileJson, Profile::class.java)
        return profile
    }

    fun saveProfile(context: Context, profile: Profile) {
        val profileJson = Gson().toJson(profile)
        PrefUtil.write(context, Constants.PREF_PROFILE, profileJson)
    }

    fun isLogin(context: Context): Boolean {
        val profileStr = PrefUtil.read(context, Constants.PREF_PROFILE, "")
        if (profileStr == "") return false

        val profile = Gson().fromJson(profileStr, Profile::class.java)
        return !profile.id.equals("")
    }
}