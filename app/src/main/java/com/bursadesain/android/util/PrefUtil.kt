package com.bursadesain.android.util

import android.content.Context

object PrefUtil {

    const val PREF_NAME = "preference"

    fun remove(context: Context, key: String?) {
        val pref =
            context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        pref.edit().remove(key).apply()
    }

    fun read(
        context: Context, key: String?, defaultString: String?
    ): String? {
        val pref =
            context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        return pref.getString(key, defaultString)
    }

    fun write(context: Context, key: String?, value: String?) {
        val pref =
            context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        pref.edit().putString(key, value).apply()
    }
}